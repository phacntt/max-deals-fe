import { Card } from 'antd';
import styled from 'styled-components';

export const CartBlock = styled(Card)`
  .ant-card-head {
    background-color: #ff5261;
    .ant-card-head-title {
      color: white;
      font-size: 18px;
      font-weight: 700;
    }
  }
`;

export const CartBlockName = styled(Card)`
  .ant-card-head {
    background-color: #ff5261;
    .ant-card-head-title {
      color: white;
      font-size: 18px;
      font-weight: 700;
    }
  }

  .ant-card-body {
    padding: 20px;
    .ant-card-bordered {
      border: none;
    }
  }
`;

export const TitleDiscountDetail = styled.div`
  position: absolute;
  z-index: 1;
  height: 35px;
  line-height: 35px;
  padding: 0 10px;
  margin: 0;
  font-size: 20px;
  color: #fff;
  text-transform: uppercase;
  font-weight: 700;
  min-width: 50px;
  background-color: rgba(231, 57, 72, 0.8);
  width: 60px;
  bottom: 70px;

  &::before {
    content: '';
    position: absolute;
    right: -30px;
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 35px 30px 0 0;
    border-color: rgba(231, 57, 72, 0.8) transparent transparent transparent;
  }
`;

export const TitleExpiryDateDetail = styled.div`
  position: absolute;
  z-index: 1;
  height: 40px;
  line-height: 40px;
  padding: 0 10px;
  margin: 0;
  font-weight: 700;

  font-size: 20px;
  color: #fff;
  width: 150px;
  background-color: rgba(0, 0, 0, 0.6);
  bottom: 30px;

  &::before {
    content: '';
    position: absolute;
    right: -30px;
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 40px 30px 0 0;
    border-color: rgba(0, 0, 0, 0.6) transparent transparent transparent;
  }
`;
