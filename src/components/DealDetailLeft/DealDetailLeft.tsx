import { Card, Carousel, Col, Image } from 'antd';
import Banner from 'components/Banner/Banner';
import DetailBanner from 'components/Banner/DetailBanner';
import DealBanner from 'components/DealBanner/DealBanner';
import {
  TitleDiscount,
  TitleExpiryDate,
} from 'components/DealCard/DealCard.style';
import DealDetailAddress from 'components/DealDetailAddress/DealDetailAddress';
import DealDetailCondition from 'components/DealDetailCondittion/DealDetailCondition';
import DealDetailImg from 'components/DealDetailImg/DealDetailImg';
import DealDetailOffer from 'components/DealDetailOffer/DealDetailOffer';
import { CardDetailDeal } from 'components/ListImageDeal/ListImageDeal.style';

import ListImagePreview from 'components/ListImageDeal/ListImagePreview';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import { fileService, storeService } from 'services';
import { Deal } from 'types/Deal';
import { File } from 'types/File';
import { Store } from 'types/Store';
import {
  CartBlock,
  CartBlockName,
  TitleDiscountDetail,
  TitleExpiryDateDetail,
} from './DealDetailLeft.style';

interface Props {
  deal: Deal;
}

const DealDetailLeft: FC<Props> = ({ deal }) => {
  const [images, setImages] = useState<File[]>([]);
  const [store, setStore] = useState<Store>();
  const [dayExpiry, setDayExpiry] = useState<number>();
  useEffect(() => {
    const fetchData = async () => {
      setImages(deal.images);
      const dealByStore = await storeService.getStoreById(deal.storeId);
      setStore(dealByStore);

      const leftDayOfDeal = moment(deal.expiryDate).diff(moment(), 'days');
      setDayExpiry(leftDayOfDeal);
    };
    fetchData();
  }, [deal]);

  return (
    <Col span={16}>
      <div
        style={{
          position: 'relative',
        }}
      >
        {deal && (
          <>
            <div>
              <TitleDiscountDetail>-{deal.discount}%</TitleDiscountDetail>
              <TitleExpiryDateDetail>
                Còn {dayExpiry} ngày
              </TitleExpiryDateDetail>
            </div>
            <Carousel autoplay>
              {images.map(image => (
                <div>
                  {/* <img
                  style={{
                    height: 450,
                    width: '100%',
                    backgroundSize: 'cover',
                  }}
                  alt="example"
                  src={image?.url}
                /> */}

                  <div
                    style={{
                      backgroundImage: `url('${image.url}')`,
                      height: 450,
                      width: '100%',
                      backgroundSize: 'cover',
                    }}
                  />
                </div>
              ))}
            </Carousel>
          </>
        )}
      </div>
      <CardDetailDeal>
        <ListImagePreview images={images} />
      </CardDetailDeal>
      <CartBlockName style={{ margin: '20px 0' }}>
        {deal && <DealDetailOffer deal={deal} />}
      </CartBlockName>
      <CartBlock title="Conditions apply">
        {deal && <DealDetailCondition deal={deal} />}
      </CartBlock>
      <CartBlock style={{ margin: '20px 0' }} title="Place of application">
        {store && <DealDetailAddress store={store} />}
      </CartBlock>
    </Col>
  );
};

export default DealDetailLeft;
