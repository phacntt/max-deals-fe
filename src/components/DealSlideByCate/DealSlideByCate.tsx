import { Carousel } from 'antd';
import { BannerItem } from 'components/Banner/Banner.style';
import DetailBanner from 'components/Banner/DetailBanner';
import React, { FC } from 'react';
import { Deal } from 'types/Deal';

interface Props {
  deals: Deal[];
}

const DealSlideByCate: FC<Props> = ({ deals }) => {
  console.log('123', deals.length);
  return (
    <Carousel autoplay dotPosition="right">
      {deals.length < 4
        ? ''
        : deals.slice(0, 4).map(deal => <DetailBanner deal={deal} />)}
    </Carousel>
  );
};

export default DealSlideByCate;
