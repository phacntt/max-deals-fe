import { Card, Carousel } from 'antd';
import { BannerItem } from 'components/Banner/Banner.style';
import React, { FC } from 'react';
import { Link } from 'react-router-dom';
import { File } from 'types/File';

interface Props {
  images: File[];
}

const DealDetailImg: FC<Props> = ({ images = [] }) => {
  return (
    <Carousel autoplay>
      <Card
        hoverable
        cover={
          <img
            style={{
              height: '400px',
            }}
            src="/ban-tiec.jpg"
            alt="ok"
            srcSet=""
          />
        }
      >
        <Link to="/" />
      </Card>
    </Carousel>
  );
};

export default DealDetailImg;
