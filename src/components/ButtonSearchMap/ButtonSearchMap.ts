import { Button } from 'antd';
import styled from 'styled-components';

export const ButtonSearchMap = styled(Button)`
  width: 150px;
  font-weight: bold;
  border-color: #4dc2ef;
  &:hover {
    background-color: #4dc2ef;
    color: white;
  }
`;
