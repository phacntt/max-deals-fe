import { EnvironmentFilled } from '@ant-design/icons/lib/icons';
import { Avatar, Card } from 'antd';
import { Meta } from 'antd/lib/list/Item';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';

import { Link } from 'react-router-dom';
import { dealService, fileService, userService } from 'services';
import { Deal } from 'types/Deal';
import { File } from 'types/File';
import { FileVariants } from 'types/FileVariants';
import {
  LogoBrand,
  TitleDiscount,
  TitleExpiryDate,
  TitleNameDeal,
} from './DealCard.style';

interface Props {
  deal: Deal;
}

const DealCard: FC<Props> = ({ deal }) => {
  const [imageDeal, setImageDeal] = useState<File>();
  const [imageStore, setImageStore] = useState<File>();
  const [imageDealResize, setImageDealResize] = useState<string>();
  const [dayExpiry, setDayExpiry] = useState<number>();

  useEffect(() => {
    if (!deal) return;
    const fetchData = async () => {
      const { id, storeId } = deal;
      const getImageStore = await fileService.getFileById(
        storeId,
        'StoreEntity',
      );
      setImageDealResize(deal.images[0].url);
      const leftDayOfDeal = moment(deal.expiryDate).diff(moment(), 'days');
      setDayExpiry(leftDayOfDeal);
      // getImageStore.map(image => setImageStore(image));
    };
    fetchData();
  }, [deal]);

  return (
    <div>
      {console.log('deals', deal.images[0].url)}
      <a href={`/store/${deal.store.slug} `}>
        <LogoBrand>
          <Avatar src={imageStore?.url} />
        </LogoBrand>
      </a>
      <Card
        style={{ marginBottom: 20 }}
        cover={
          <img
            style={{ objectFit: 'fill' }}
            alt="example"
            src={deal.images[0].url && deal.images[0].fileVariants[0].url}
          />
        }
      >
        <TitleDiscount>-{deal.discount}%</TitleDiscount>
        <TitleExpiryDate>Còn {dayExpiry} ngày</TitleExpiryDate>
        <Link to={`/deals/${deal.slug}`}>
          <TitleNameDeal title={deal.name} />
        </Link>
        <hr />
        <div
          style={{
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            fontSize: '13px',
            color: 'gray',
          }}
        >
          <EnvironmentFilled /> {deal.store.fullAddress}
        </div>
      </Card>
    </div>
  );
};

export default DealCard;
