import Meta from 'antd/lib/card/Meta';
import styled from 'styled-components';

export const LogoBrand = styled.div`
  text-align: center;
  border: 1px 1px 0px 1px solid #d4d4d4;
  background: white;
  padding: 10px 0;
`;

export const TitleNameDeal = styled(Meta)`
  .ant-card-meta-title {
    font-size: 14px;
  }
`;

export const TitleDiscount = styled.div`
  position: absolute;
  z-index: 0;
  height: 25px;
  line-height: 25px;
  padding: 0 10px;
  margin: 0;
  font-size: 14px;
  color: #fff;
  text-transform: uppercase;
  font-weight: 700;
  min-width: 50px;
  background-color: rgba(231, 57, 72, 0.8);
  width: 60px;
  bottom: 130px;
  left: 0;

  &::before {
    content: '';
    position: absolute;
    right: -10px;
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 25px 10px 0 0;
    border-color: rgba(231, 57, 72, 0.8) transparent transparent transparent;
  }
`;

export const TitleExpiryDate = styled.div`
  position: absolute;
  z-index: 0;
  height: 30px;
  line-height: 30px;
  padding: 0 10px;
  margin: 0;
  font-weight: 700;

  font-size: 14px;
  color: #fff;
  width: 150px;
  background-color: rgba(0, 0, 0, 0.6);
  bottom: 100px;
  left: 0;

  &::before {
    content: '';
    position: absolute;
    right: -10px;
    width: 0;
    height: 30px;
    border-style: solid;
    border-width: 30px 10px 0 0;
    border-color: rgba(0, 0, 0, 0.6) transparent transparent transparent;
  }
`;
