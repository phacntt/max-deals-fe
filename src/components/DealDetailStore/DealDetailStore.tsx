import { Divider } from 'antd';
import React, { FC, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { fileService } from 'services';
import { Deal } from 'types/Deal';
import { File } from 'types/File';
import { Store } from 'types/Store';
import {
  DescriptionStore,
  LinkToStore,
  LogoStore,
  NameStore,
} from './DealDetailStore.style';

interface Props {
  store: Store;
}

const DealDetailStore: FC<Props> = ({ store }) => {
  console.log('hear');
  const [image, setImage] = useState<File>();
  useEffect(() => {
    const fetchData = async () => {
      const imageStore = await fileService.getFileById(store.id, 'StoreEntity');
      setImage(imageStore[0] as File);
    };
    fetchData();
  }, []);

  return (
    <div>
      <div
        style={{
          textAlign: 'center',
          marginBottom: '10px',
        }}
      />
      <Divider orientation="center">
        <Link to={`/store/${store.slug}`}>
          <LogoStore src={image?.url} alt="" />
        </Link>
      </Divider>
      <NameStore>{store.name}</NameStore>
      <DescriptionStore
        dangerouslySetInnerHTML={{ __html: store.description }}
      />

      <LinkToStore href={`/store/${store.slug}`}>
        {store.name} tại ZamZa
      </LinkToStore>
    </div>
  );
};

export default DealDetailStore;
