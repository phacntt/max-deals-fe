import styled from 'styled-components';

export const LogoStore = styled.img`
  max-height: 40px;
  max-width: 100%;
`;

export const NameStore = styled.div`
  text-align: center;
  color: #e73948;
  font-weight: 700;
  margin-bottom: 15px;
`;

export const DescriptionStore = styled.div`
  display: -webkit-box;
  -webkit-line-clamp: 4;
  -webkit-box-orient: vertical;
  overflow: hidden;
  margin-bottom: 10px;
`;

export const LinkToStore = styled.a`
  padding-top: 15px;
  border-top: 1px solid #ededed;
  display: block;
  text-align: center;
  color: #e73948;
  text-decoration: underline;
  &:hover {
    color: #e73948;
    text-decoration: underline;
  }
`;
