import {
  LaptopOutlined,
  DashboardOutlined,
  HomeOutlined,
  TagOutlined,
  UserOutlined,
} from '@ant-design/icons';
import { Menu } from 'antd';
import Sider from 'antd/lib/layout/Sider';
import SubMenu from 'antd/lib/menu/SubMenu';
import React, { FC } from 'react';
import { Link, useParams } from 'react-router-dom';
import { useAppSelector } from 'redux/hooks';
import { selectCurrentUser } from 'redux/slices/current-user.slice';

const SidebarSAdmin: FC = () => {
  const currentUser = useAppSelector(selectCurrentUser);

  return (
    <Sider
      style={{
        flex: '1 auto',
        zIndex: 2,
        height: '100%',
        position: 'fixed',
      }}
      width={240}
      className="site-layout-background"
    >
      <Menu
        mode="inline"
        defaultSelectedKeys={['1']}
        defaultOpenKeys={['sub1']}
        style={{ height: '100%', borderRight: 0 }}
      >
        <SubMenu key="sub1" icon={<DashboardOutlined />} title="DashBoard">
          <Menu.Item key="1">
            <Link to="/superadmin">HomePage</Link>
          </Menu.Item>
        </SubMenu>
        <SubMenu key="sub2" icon={<LaptopOutlined />} title="Category Manage">
          <Menu.Item key="5">
            <Link to="/superadmin/categorymanage">List Categories</Link>
          </Menu.Item>
          <Menu.Item key="6">
            <Link to="/superadmin/categorymanage/add">Add New Category</Link>
          </Menu.Item>
        </SubMenu>
        <SubMenu key="sub3" icon={<TagOutlined />} title="Deal Manage">
          <Menu.Item key="7">
            <Link to="/superadmin/dealmanage">List Deals</Link>
          </Menu.Item>
          <Menu.Item key="8">
            <Link to="/superadmin/dealmanage/add">Add New Deal</Link>
          </Menu.Item>
        </SubMenu>
        <SubMenu key="sub4" icon={<HomeOutlined />} title="Store Manage">
          <Menu.Item key="9">
            <Link to="/superadmin/storemanage">List Stores</Link>
          </Menu.Item>
          <Menu.Item key="10">
            <Link to="/superadmin/storemanage/add">Add New Store</Link>
          </Menu.Item>
        </SubMenu>
        <SubMenu key="sub5" icon={<UserOutlined />} title="User Manage">
          <Menu.Item key="11">
            <Link to="/superadmin/usermanage">List Users</Link>
          </Menu.Item>
        </SubMenu>
      </Menu>
    </Sider>
  );
};

export default SidebarSAdmin;
