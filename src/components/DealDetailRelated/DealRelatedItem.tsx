import { Col, List, Row } from 'antd';
import React, { FC, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { fileService, storeService } from 'services';
import { Deal } from 'types/Deal';
import { File } from 'types/File';
import { Store } from 'types/Store';
import {
  DealRelated,
  ItemDealAndStore,
  ListItemDealAndStore,
  StoreRelated,
  TitleRelated,
} from './DealDetailRelated.style';

interface Props {
  deal: Deal;
}

const DealRelatedItem: FC<Props> = ({ deal }) => {
  const [image, setImage] = useState<File>();
  const [store, setStore] = useState<Store>();

  useEffect(() => {
    const fetchData = async () => {
      const imageDeal = await fileService.getFileById(deal.id, 'DealEntity');
      setImage(imageDeal[0] as File);
      const storeDeal = await storeService.getStoreById(deal.storeId);
      setStore(storeDeal);
    };
    fetchData();
  }, []);

  return (
    <ItemDealAndStore>
      <Row>
        <Col span={7}>
          <a href={`/deals/${deal.slug}`}>
            <img
              style={{
                maxHeight: '100%',
                maxWidth: '100%',
              }}
              src={image?.url}
              alt=""
            />
          </a>
        </Col>
        <Col span={17}>
          <Row>
            <Col span={24}>
              <StoreRelated href={`/store/${store?.slug}`}>
                <TitleRelated>{store?.name}</TitleRelated>
              </StoreRelated>
              <DealRelated href={`/deals/${deal?.slug}`}>
                <TitleRelated>{deal?.name}</TitleRelated>
              </DealRelated>
            </Col>
          </Row>
        </Col>
      </Row>
    </ItemDealAndStore>
  );
};

export default DealRelatedItem;
