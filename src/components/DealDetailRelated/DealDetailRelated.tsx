import { List } from 'antd';
import React, { FC } from 'react';

import { Deal } from 'types/Deal';
import { ListItemDealAndStore } from './DealDetailRelated.style';
import DealRelatedItem from './DealRelatedItem';

interface Props {
  deals: Deal[];
  idDealCurrent: number;
}

const DealDetailRelated: FC<Props> = ({ deals, idDealCurrent }) => {
  console.log('related', deals);
  const dealRelated = deals.filter(deal => deal.id !== idDealCurrent);
  console.log('related', dealRelated);
  return (
    // <List
    //   size="large"
    //   dataSource={dealRelated}
    //   renderItem={deal => <DealRelatedItem deal={deal} />}
    // />
    <ListItemDealAndStore>
      {dealRelated.map(deal => (
        <DealRelatedItem deal={deal} />
      ))}
    </ListItemDealAndStore>
  );
};

export default DealDetailRelated;
