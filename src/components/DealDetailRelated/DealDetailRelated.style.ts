import { List } from 'antd';
import Item from 'antd/lib/list/Item';
import styled from 'styled-components';

export const TitleRelated = styled.div`
  display: -webkit-box;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
  overflow: hidden;
  margin-left: 30px;
  line-height: 30px;
`;

export const DealRelated = styled.a`
  margin-top: 10px;
  color: black;
  font-size: 15px;
  &:hover {
    color: black;
  }
`;
export const StoreRelated = styled.a`
  color: black;
  width: 100%;
  font-weight: 700;
  font-size: 16px;
  &:hover {
    color: black;
  }
`;

export const ListItemDealAndStore = styled.ul`
  padding: 0;
`;

export const ItemDealAndStore = styled.li`
  list-style: none;
  padding: 16px 0;
`;
