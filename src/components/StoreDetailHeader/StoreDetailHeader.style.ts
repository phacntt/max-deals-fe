import styled from 'styled-components';

export const LogoStore = styled.div`
  height: 130px;
`;

export const ContentStore = styled.div`
  height: 100%;
  display: flex;
  align-items: flex-end;
  justify-content: center;
  padding: 10px 0;
`;

export const ContentStoreDeals = styled.span`
  border-radius: 5px;
  border: 2px solid #e73948;
  color: #e73948;
  padding: 0 10px;
  font-size: 18px;
  font-weight: 600;
  text-align: center;
  width: 100%;
`;

export const ContentStoreConnect = styled.span`
  color: gray;
`;
