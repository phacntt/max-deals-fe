import { EnvironmentFilled, PhoneFilled, TagFilled } from '@ant-design/icons';
import { Col, Row } from 'antd';
import React, { FC } from 'react';
import { Deal } from 'types/Deal';
import { File } from 'types/File';
import { Store } from 'types/Store';
import {
  ContentStore,
  ContentStoreConnect,
  ContentStoreDeals,
  LogoStore,
} from './StoreDetailHeader.style';

interface Props {
  deals: Deal[];
  image: File;
  store: Store;
}

const StoreDetailHeader: FC<Props> = ({ deals = [], image, store }) => {
  return (
    <>
      <Row gutter={16}>
        <Col span={4}>
          <LogoStore>
            <img
              style={{ height: 130, width: '100%' }}
              src={image.url}
              alt=""
              srcSet=""
            />
          </LogoStore>
        </Col>
        <Col span={4}>
          <ContentStore>
            <ContentStoreDeals>
              <TagFilled /> {deals.length} deals
            </ContentStoreDeals>
          </ContentStore>
        </Col>
        <Col span={16}>
          <ContentStore>
            <ContentStoreConnect>
              <EnvironmentFilled /> {store.fullAddress}
              <br />
              <PhoneFilled /> {store.phone}
            </ContentStoreConnect>
          </ContentStore>
        </Col>
      </Row>
      <br />
    </>
  );
};

export default StoreDetailHeader;
