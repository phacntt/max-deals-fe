import { Card } from 'antd';
import React, { FC } from 'react';
import { Deal } from 'types/Deal';

interface Props {
  deal: Deal;
}

const DealDetailOffer: FC<Props> = ({ deal }) => {
  return (
    <div
      style={{
        fontSize: 20,
        fontWeight: 700,
      }}
    >
      {deal.name.toUpperCase()}
    </div>
  );
};
export default DealDetailOffer;
