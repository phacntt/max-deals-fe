import React, { FC, useEffect, useRef, useState } from 'react';
import GoogleMapReact from 'google-map-react';

const API_KEY = 'AIzaSyADfAOFysvcMoDqvYPYN48bZf9wSGONJmU';

interface Props {
  fullAddress: string;
  onChange: (location: GeoLocation) => void;
}

interface GeoLocation {
  lat: number;
  lng: number;
}
interface CustomMarkerProps {
  text?: string;
  lat: number;
  lng: number;
}
const CustomMarker: FC<CustomMarkerProps> = () => (
  <img width={32} height={32} src="/marker.png" alt="marker" />
);

const HOCHIMINH_LOCATION = { lat: 10.762622, lng: 106.660172 };

const Maps: FC<Props> = ({ fullAddress, onChange }) => {
  console.log('hello', fullAddress);
  const address: string = fullAddress;

  const [location, setLocation] = useState({
    lat: -1,
    lng: -1,
  });
  const geocoderRef = useRef<any>(null);
  const [mapCenter, setMapCenter] = useState<GeoLocation>();

  useEffect(() => {
    if (navigator.geolocation) {
      console.log('get current user location');
      const options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0,
      };
      const updateMapCenter = (position: any) => {
        const crd = position.coords;

        console.log('Your current position is:');
        console.log(`Latitude : ${crd.latitude}`);
        console.log(`Longitude: ${crd.longitude}`);
        console.log(`More or less ${crd.accuracy} meters.`);

        setMapCenter({
          lat: crd.latitude,
          lng: crd.longitude,
        });
      };
      navigator.permissions
        .query({ name: 'geolocation' })
        .then(function (result) {
          if (result.state === 'granted') {
            console.log(result.state);
            navigator.geolocation.getCurrentPosition(updateMapCenter);
          } else if (result.state === 'prompt') {
            navigator.geolocation.getCurrentPosition(
              updateMapCenter,
              err => {
                console.warn(`ERROR(${err.code}): ${err.message}`);
              },
              options,
            );
          } else if (result.state === 'denied') {
            // show message that user has denied location permission
          }
        });
    }
  }, []);

  useEffect(() => {
    if (fullAddress) {
      geocoderRef.current.geocode(
        { address, region: 'vn' },
        (results: any[], status: string) => {
          if (status === 'OK') {
            const geolocation = {
              lat: results[0].geometry.location.lat(),
              lng: results[0].geometry.location.lng(),
            };
            setLocation(geolocation);
            setMapCenter(geolocation);
            onChange(geolocation);
          } else {
            console.error(
              `Geocode was not successful for the following reason: ${status}`,
            );
          }
        },
      );
    }
  }, [fullAddress]);

  const handleGoogleMapApiLoaded = (_map: any, maps: any) => {
    geocoderRef.current = new maps.Geocoder();
  };

  const handleClickOnMap = (clickValue: GoogleMapReact.ClickEventValue) => {
    console.log('On Map Click', clickValue);
    setLocation({
      lat: clickValue.lat,
      lng: clickValue.lng,
    });
    onChange(location);
  };

  return (
    <div style={{ height: 500, width: '100%' }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: API_KEY }}
        defaultCenter={HOCHIMINH_LOCATION}
        defaultZoom={17}
        center={mapCenter}
        yesIWantToUseGoogleMapApiInternals
        onGoogleApiLoaded={({ map, maps }) =>
          handleGoogleMapApiLoaded(map, maps)
        }
        onClick={handleClickOnMap}
      >
        {location.lat !== -1 && (
          <CustomMarker
            lat={location.lat}
            lng={location.lng}
            text="My Marker"
          />
        )}
      </GoogleMapReact>
    </div>
  );
};

export default Maps;
