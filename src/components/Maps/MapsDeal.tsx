import React, { FC, useEffect, useRef, useState } from 'react';
import GoogleMapReact from 'google-map-react';

const API_KEY = 'AIzaSyADfAOFysvcMoDqvYPYN48bZf9wSGONJmU';

interface Props {
  location: GeoLocation;
}

interface GeoLocation {
  lat: number;
  lng: number;
}
interface CustomMarkerProps {
  text?: string;
  lat: number;
  lng: number;
}
const CustomMarker: FC<CustomMarkerProps> = () => (
  <img width={32} height={32} src="/marker.png" alt="marker" />
);

const HOCHIMINH_LOCATION = { lat: 10.762622, lng: 106.660172 };

const MapsDeal: FC<Props> = ({ location }) => {
  const [mapCenter, setMapCenter] = useState<GeoLocation>();

  useEffect(() => {
    setMapCenter({
      lat: location.lat,
      lng: location.lng,
    });
  }, []);

  console.log('located', location);
  return (
    <div style={{ height: 248, width: '100%' }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: API_KEY }}
        defaultCenter={HOCHIMINH_LOCATION}
        defaultZoom={17}
        center={mapCenter}
        yesIWantToUseGoogleMapApiInternals
      >
        {location.lat !== -1 && (
          <CustomMarker
            lat={location.lat}
            lng={location.lng}
            text="My Marker"
          />
        )}
      </GoogleMapReact>
    </div>
  );
};

export default MapsDeal;
