/* eslint-disable react/no-unused-prop-types */
/* eslint-disable react/require-default-props */
import React, { FC, useEffect, useRef, useState } from 'react';
import GoogleMapReact from 'google-map-react';
import { Deal } from 'types/Deal';
import { Link } from 'react-router-dom';
import { Popover } from 'antd';
import {
  CaretDownOutlined,
  EnvironmentFilled,
  RocketOutlined,
  TagOutlined,
  TagsFilled,
} from '@ant-design/icons';

const API_KEY = 'AIzaSyADfAOFysvcMoDqvYPYN48bZf9wSGONJmU';

interface Props {
  deals: Deal[];
  onChange: (location: GeoLocation) => void;
}

interface GeoLocation {
  lat: number;
  lng: number;
}

interface CustomMarkerProps {
  deal?: Deal;
  text?: string;
  lat: number;
  lng: number;
}
const CustomMarker: FC<CustomMarkerProps> = ({ deal }) => {
  const content = (
    <div>
      <div>
        <a
          style={{
            color: 'black',
            fontWeight: 600,
          }}
          href={`/deals/${deal?.id}`}
        >
          <TagsFilled /> {deal?.name}
        </a>
      </div>
      <br />
      <div
        style={{
          marginBottom: 10,
        }}
      >
        <span
          style={{
            color: 'green',
            fontSize: 15,
            marginRight: 30,
          }}
        >
          <CaretDownOutlined /> Discount: {deal?.discount}%
        </span>
        <span>
          <RocketOutlined /> Distance:{' '}
          {(deal?.store.distance as number) > 1000
            ? `${Math.round(deal?.store.distance as number) / 1000} Km`
            : `${Math.round(deal?.store.distance as number)} m`}
        </span>
      </div>
      <p>
        <EnvironmentFilled /> {deal?.store.fullAddress}m
      </p>
    </div>
  );
  return (
    <Popover content={content} title={deal?.store.name}>
      <img width={32} height={32} src="/marker.png" alt="marker" />
    </Popover>
  );
};

const UserMarker: FC<CustomMarkerProps> = () => (
  <img width={32} height={32} src="/pngwing.com.png" alt="marker" />
);

const HOCHIMINH_LOCATION = { lat: 10.762622, lng: 106.660172 };

const MapNearMe: FC<Props> = ({ deals = [], onChange }) => {
  // console.log('deals', deals);
  const [location, setLocation] = useState({
    lat: -1,
    lng: -1,
  });
  const geocoderRef = useRef<any>(null);
  const [mapDatas, setMapDatas] = useState<{ map: any; maps: any }>();
  const [mapCenter, setMapCenter] = useState<GeoLocation>();

  useEffect(() => {
    if (navigator.geolocation) {
      console.log('get current user location');
      const options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0,
      };
      const updateMapCenter = (position: any) => {
        const crd = position.coords;

        console.log('Your current position is:');
        console.log(`Latitude : ${crd.latitude}`);
        console.log(`Longitude: ${crd.longitude}`);
        console.log(`More or less ${crd.accuracy} meters.`);

        setMapCenter({
          lat: crd.latitude,
          lng: crd.longitude,
        });
        onChange({
          lat: crd.latitude,
          lng: crd.longitude,
        });
      };
      navigator.permissions
        .query({ name: 'geolocation' })
        .then(function (result) {
          if (result.state === 'granted') {
            console.log(result.state);
            navigator.geolocation.getCurrentPosition(updateMapCenter);
          } else if (result.state === 'prompt') {
            navigator.geolocation.getCurrentPosition(
              updateMapCenter,
              err => {
                console.warn(`ERROR(${err.code}): ${err.message}`);
              },
              options,
            );
          } else if (result.state === 'denied') {
            // show message that user has denied location permission
          }
        });
    }
  }, [location]);

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(position => {
      const query: GeoLocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      };
      setLocation(query);
    });
  }, []);

  useEffect(() => {
    if (!mapDatas || !deals || !deals.length) {
      return;
    }

    const bounds = new mapDatas.maps.LatLngBounds();
    deals.forEach(deal => {
      bounds.extend({
        lat: deal.store.lat,
        lng: deal.store.lng,
      });
    });
    bounds.extend(location);

    mapDatas.map.fitBounds(bounds);
  }, [mapDatas, deals, location]);

  const handleGoogleMapApiLoaded = (_map: any, maps: any) => {
    geocoderRef.current = new maps.Geocoder();
    setMapDatas({
      map: _map,
      maps,
    });
  };

  return (
    <div style={{ height: 500, width: '100%' }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: API_KEY }}
        defaultCenter={HOCHIMINH_LOCATION}
        defaultZoom={14}
        center={mapCenter}
        yesIWantToUseGoogleMapApiInternals
        onGoogleApiLoaded={({ map, maps }) =>
          handleGoogleMapApiLoaded(map, maps)
        }
      >
        <UserMarker lat={location.lat} lng={location.lng} text="My Marker" />
        {deals.map(deal => (
          <CustomMarker
            lat={deal.store.lat}
            lng={deal.store.lng}
            text="My Marker"
            deal={deal}
          />
        ))}
      </GoogleMapReact>
    </div>
  );
};

export default MapNearMe;
