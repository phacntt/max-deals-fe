import { Card, Carousel, Col, Image, Row } from 'antd';
import DealBanner from 'components/DealBanner/DealBanner';
import React, { FC } from 'react';

import { File } from 'types/File';
import ListImagePreview from './ListImagePreview';

interface Props {
  images: File[];
}

const ListImageDeal: FC<Props> = ({ images = [] }) => {
  return (
    <Carousel autoplay>
      {images.map(image => (
        <DealBanner image={image} />
      ))}
    </Carousel>
  );
};

export default ListImageDeal;
