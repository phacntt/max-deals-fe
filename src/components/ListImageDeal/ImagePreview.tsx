import { Image } from 'antd';
import React, { FC } from 'react';

import { File } from 'types/File';
import { ImagePreviewItem } from './ImageDetail.style';

interface Props {
  image: File;
}

const ImagePreview: FC<Props> = ({ image }) => {
  return <Image style={{ width: 80, height: 80 }} src={image.url} />;
};

export default ImagePreview;
