import { Col, Row } from 'antd';
import React, { FC } from 'react';
import { File } from 'types/File';
import { ImagePreviewItem } from './ImageDetail.style';
import ImagePreview from './ImagePreview';

interface Props {
  images: File[];
}

const ListImagePreview: FC<Props> = ({ images }) => {
  return (
    <Row>
      <Col span={24}>
        <ImagePreviewItem>
          {images.map(image => (
            <ImagePreview image={image} />
          ))}
        </ImagePreviewItem>
      </Col>
    </Row>
  );
};

export default ListImagePreview;
