import { Card } from 'antd';
import styled from 'styled-components';

export const CardDetailDeal = styled(Card)`
  .ant-card-body {
    padding: 10px 24px;
  }
`;
