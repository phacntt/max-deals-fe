import { Image } from 'antd';
import styled from 'styled-components';

export const ImagePreviewItem = styled.div`
  .ant-image {
    margin: 0 10px;
  }
`;
