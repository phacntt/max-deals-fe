import { Col } from 'antd';
import React, { FC } from 'react';

const FooterFollow: FC = () => {
  return (
    <div style={{ textAlign: 'center' }}>
      <h2>Follow ZAMZA</h2>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'start',
        }}
      >
        <a href="/">Facebook</a>
        <a href="/">Youtube</a>
        <a href="/">Twitter</a>
      </div>
    </div>
  );
};

export default FooterFollow;
