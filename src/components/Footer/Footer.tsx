import { Col, Layout, Row } from 'antd';
import Container from 'components/Container/Container';
import React, { FC } from 'react';
import FooterContact from './FooterContact';
import FooterCorp from './FooterCorp';
import FooterDiscover from './FooterDiscorver';
import FooterFollow from './FooterFollow';

const { Footer } = Layout;

const AppFooter: FC = () => {
  return (
    <Footer style={{ background: 'white', textAlign: 'center' }}>
      <Container>
        <Row>
          <Col span={24}>
            <div>©2022 JAMJA. Auth Tran Anh Pha. TDTU</div>
          </Col>
        </Row>
      </Container>
    </Footer>
  );
};

export default AppFooter;
