import { Col } from 'antd';
import React, { FC } from 'react';

const FooterDiscover: FC = () => {
  return (
    <Col span={8}>
      <h2>KHÁM PHÁ</h2>
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <a href="/">HomePage</a>
        <a href="/">About Us</a>
        <a href="/">Contact Us</a>
      </div>
    </Col>
  );
};

export default FooterDiscover;
