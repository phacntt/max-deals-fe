import { Col, Row } from 'antd';
import DealBannerItem from 'components/DealBannerItem/DealBannerItem';
import React, { FC } from 'react';
import { Deal } from 'types/Deal';

interface Props {
  listDeal: Deal[];
}

const TopBannerCollection: FC<Props> = ({ listDeal = [] }) => {
  console.log(listDeal);

  return (
    <Row style={{ height: '400px' }}>
      <Col span={18}>
        {listDeal[0] && <DealBannerItem deal={listDeal[0]} />}
      </Col>
      <Col span={6}>
        <Row style={{ height: '100%' }}>
          {listDeal.slice(1, 3).map(deal => (
            <Col span={24}>
              <DealBannerItem deal={deal} />
            </Col>
          ))}
        </Row>
      </Col>
    </Row>
  );
};

export default TopBannerCollection;
