import { Checkbox } from 'antd';
import { FillterCardItem } from 'components/DealMainCategory/DealMainCategory.style';
import React, { FC, useEffect, useState } from 'react';
import { categoryService } from 'services';
import { Category } from 'types/Category';
import { FillterCard, ItemFilter } from './FilterSideBarCategory.style';

interface Props {
  category: Category;
  categories: Category[];
}

const FilterSideBarCategory: FC<Props> = ({ category, categories = [] }) => {
  const parentCategories = categories.filter(
    categoryItem => categoryItem.parentId === category.id,
  );

  return (
    <FillterCard
      title={<span>{category.name.toUpperCase()} FILTER</span>}
      bordered={false}
    >
      <div
        style={{
          height: '180px',
          overflow: 'auto',
          display: 'flex',
          flexDirection: 'column',
        }}
      >
        {parentCategories.map(categoryEle => (
          // <FillterCardItem to={`/deals?categoryId=${categoryEle.id}`}>
          <FillterCardItem to={`/deals/category/${categoryEle.slug}`}>
            <ItemFilter>{categoryEle.name}</ItemFilter>
          </FillterCardItem>
        ))}
      </div>
    </FillterCard>
  );
};

export default FilterSideBarCategory;
