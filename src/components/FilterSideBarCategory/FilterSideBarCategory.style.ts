import { Card } from 'antd';
import styled from 'styled-components';

export const FillterCard = styled(Card)`
  width: 250px;
  margin-bottom: 20px;
  .ant-card-head-title {
    padding: 0;
  }
  .ant-card-head {
    padding: 10px 20px;
    background-color: #e73948;
    .ant-card-head-title {
      color: white;
      font-size: 16px;
      font-weight: 700;
    }
  }
  .ant-card-body {
    padding: 0 0 0 10px;
  }
`;

export const ItemFilter = styled.div`
  padding: 10px 10px 10px 10px;
  color: black;
  &:hover {
    font-weight: 700;
    border-left: 4px solid #e73948;
    color: black;
  }
`;
