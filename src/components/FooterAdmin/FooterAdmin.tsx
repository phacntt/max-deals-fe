import { Layout } from 'antd';
import React, { FC } from 'react';

const { Footer } = Layout;

const AppFooterAdmin: FC = () => {
  return (
    <Footer style={{ marginLeft: 200, textAlign: 'center' }}>
      Ant-design 2022
    </Footer>
  );
};

export default AppFooterAdmin;
