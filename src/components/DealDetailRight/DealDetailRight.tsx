import { Card, Col } from 'antd';
import { DealAPI } from 'apis/deal.api';
import Banner from 'components/Banner/Banner';
import DetailBanner from 'components/Banner/DetailBanner';
import DealBanner from 'components/DealBanner/DealBanner';
import DealDetailAddress from 'components/DealDetailAddress/DealDetailAddress';
import DealDetailCondition from 'components/DealDetailCondittion/DealDetailCondition';
import DealDetailImg from 'components/DealDetailImg/DealDetailImg';
import DealDetailOffer from 'components/DealDetailOffer/DealDetailOffer';
import DealDetailRelated from 'components/DealDetailRelated/DealDetailRelated';
import DealDetailStore from 'components/DealDetailStore/DealDetailStore';

import InfoRegisterDeal from 'components/InfoBookDeal/InfoBookDeal';
import ListImageDeal from 'components/ListImageDeal/ListImageDeal';
import { CardDetailDeal } from 'components/ListImageDeal/ListImageDeal.style';
import ListImagePreview from 'components/ListImageDeal/ListImagePreview';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import { dealService, fileService, storeService } from 'services';
import { Deal } from 'types/Deal';
import { File } from 'types/File';
import { Store } from 'types/Store';

interface Props {
  deal: Deal;
  id: number;
}

const DealDetailRight: FC<Props> = ({ deal, id }) => {
  console.log('deal', deal);

  const [images, setImages] = useState<File[]>([]);
  const [store, setStore] = useState<Store>();
  const [deals, setdeals] = useState<Deal[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      // const listImages = await fileService.getFileById(deal.id, 'DealEntity');
      // setImages(listImages);
      const dealByStore = await storeService.getStoreById(deal.storeId);
      setStore(dealByStore);
      const listDealByCate = await dealService.getAllDeal({
        categoryId: deal.categoryId,
        expiryDate: moment().toISOString(),
      });
      setdeals(listDealByCate);
    };
    fetchData();
  }, []);
  // console.log('deala', deals);

  return (
    <Col span={8}>
      <InfoRegisterDeal id={id} deal={deal} />

      <CardDetailDeal style={{ margin: '20px 0' }}>
        {store && <DealDetailStore store={store as Store} />}
      </CardDetailDeal>
      <CardDetailDeal
        style={{ margin: '20px 0' }}
        title="Maybe you are interested"
      >
        {deals && (
          <DealDetailRelated
            idDealCurrent={deal.id}
            deals={deals.slice(0, 6) as Deal[]}
          />
        )}
      </CardDetailDeal>
    </Col>
  );
};

export default DealDetailRight;
