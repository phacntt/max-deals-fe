import { CloseOutlined, DeleteOutlined } from '@ant-design/icons';
import { Image } from 'antd';
import PreviewGroup from 'antd/lib/image/PreviewGroup';
import Item from 'antd/lib/list/Item';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const GroupImage = styled(Item)`
  justify-content: start;
  .ant-form-item-control-input-content {
    display: flex;
  }
`;
export const ImageCard = styled.div`
  position: relative;

  .ant-image {
    position: relative;
  }

  .ant-image-img {
    display: block;
  }

  Button {
    position: absolute;
    top: -8px;
    right: -10px;
    color: #5dc0ff;

    &:hover {
      color: white;
      background: black;
      font-weight: bold;
    }
  }
`;

export const IconDelete = styled(CloseOutlined)``;
