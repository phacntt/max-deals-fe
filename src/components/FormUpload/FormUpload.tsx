import { CloseOutlined, DeleteOutlined } from '@ant-design/icons';
import { Form, Input, Image, Button, Col, Row, Tooltip } from 'antd';
import { fileAPI } from 'apis';
import { FileRequestBody } from 'apis/file.api';
import { GroupImage, ImageCard } from 'components/FormUpload/FormUpload.style';
import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { fileService } from 'services';
import { File } from 'types/File';

const MAX_UPLOAD_FILE = 3;
const MAX_UPLOAD_SINGLE_FILE = 1;

interface Props {
  onChange: (file: File[]) => void;
  value: File[];
  isMultiple: boolean;
}

// Set default [] if value is undefined
const Uploader: FC<Props> = ({ onChange, value = [], isMultiple }) => {
  console.log('Single?: ', isMultiple);
  const [errorMessage, setErrorMessage] = useState('');
  console.log('Vaue before render 1', value);

  const handleChangeFile = async (event: ChangeEvent<HTMLInputElement>) => {
    const selectedFiles = event.currentTarget.files;
    const selectedFilesArray = Array.from(selectedFiles as FileList);
    const formData = new FormData();
    selectedFilesArray.forEach(file =>
      formData.append('formFileMultiple', file),
    );
    if (selectedFiles && isMultiple === true) {
      if (selectedFilesArray.length > MAX_UPLOAD_FILE) {
        setErrorMessage('Maximum upload files is 3');
        return;
      }

      try {
        const resp = await fileAPI.uploadFile(formData);
        onChange(value.concat(resp.data));
      } catch (error) {
        console.log(error);
      }
    }
    if (selectedFiles && isMultiple === false) {
      if (selectedFilesArray.length > MAX_UPLOAD_SINGLE_FILE) {
        setErrorMessage('Maximum upload files is 1');
        return;
      }

      try {
        const resp = await fileAPI.uploadSingleFile(formData);
        onChange(value.concat(resp.data));
      } catch (error) {
        console.log(error);
      }
    }
  };

  const handleDeleteImage = (currentIndex: number) => {
    const cloneData = value.slice();
    cloneData.splice(currentIndex, 1);
    onChange(cloneData);
  };

  return (
    <>
      <Input
        type="file"
        className="form-control"
        id="formFileMultiple"
        name="file"
        multiple={isMultiple}
        onChange={handleChangeFile}
      />
      <div style={{ display: 'flex' }}>
        {value.map((file, index) => {
          return (
            <ImageCard key={file.id} style={{ margin: 10 }}>
              <Image
                style={{ objectFit: 'cover' }}
                width={100}
                height={100}
                src={file.url}
              />
              <Button
                shape="circle"
                onClick={() => handleDeleteImage(index)}
                icon={<CloseOutlined />}
                size="small"
              />
            </ImageCard>
          );
        })}
      </div>
      {/* <Image.PreviewGroup>
        <GroupImage>
          
        </GroupImage>
      </Image.PreviewGroup> */}
    </>
  );
};

export default Uploader;
