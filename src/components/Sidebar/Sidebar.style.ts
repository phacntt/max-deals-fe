import { Card } from 'antd';
import styled from 'styled-components';

export const SidlebarHeader = styled.h2`
  background: white;
  color: black;
  margin-bottom: 0;
  padding: 10px 0 10px 30px;
`;

export const SideBarListItemCategory = styled(Card)`
  display: flex;
  flex-direction: column;
  .ant-card-head {
    background-color: #e73948;
    .ant-card-head-title {
      font-size: 20px;
      font-weight: bold;
      color: white;
    }
  }

  .ant-card-body {
    padding: 0 0 0 12px;
  }
`;

export const ListItemCategory = styled.div`
  display: flex;
  flex-direction: column;
`;

export const ItemCategory = styled.a`
  margin: 10px 15px 10px 0;
  padding: 10px 0;
  color: black;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  font-size: 15px;
  position: relative;
  &:hover {
    font-weight: 700;
    border-left: 4px solid #e73948;
    color: black;
  }
`;
