import { Avatar, Checkbox, CheckboxProps, List } from 'antd';
import { CheckboxChangeEvent } from 'antd/lib/checkbox';
import Column from 'antd/lib/table/Column';
import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import { dealService } from 'services';
import { Category } from 'types/Category';
import { Deal } from 'types/Deal';
import {
  HeaderCategory,
  TitleCategory,
  ItemCategory,
} from './FillterCategory.style';

interface Props {
  mainCategories: Category[];
  parentCategories: Category[];
}

export type CategoryFilterSideBar = {
  nameMainCategory?: Category;
  parentCategory?: Category[];
};

const FillterCategory: FC<Props> = ({ parentCategories, mainCategories }) => {
  // Get data Category from DB in here
  console.log(parentCategories);
  console.log(mainCategories);

  const categoriesFilter = [];

  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < mainCategories.length; i++) {
    // eslint-disable-next-line no-plusplus
    const categoryFilter: CategoryFilterSideBar = {
      nameMainCategory: mainCategories[i],
    };
    // eslint-disable-next-line no-plusplus
    for (let j = 0; j < parentCategories.length; j++) {
      if (parentCategories[j].parentCategory.id === mainCategories[i].id) {
        categoryFilter.parentCategory = parentCategories.filter(
          category => category.parentCategory.id === mainCategories[i].id,
        );
      }
    }
    categoriesFilter.push(categoryFilter);
  }

  const onChange = (a: CheckboxChangeEvent) => {
    console.log(`check = ${a.target.checked}`);
  };

  return (
    <div>
      <HeaderCategory>FILLTER</HeaderCategory>
      <List
        itemLayout="horizontal"
        dataSource={categoriesFilter}
        renderItem={category => (
          <List.Item
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'flex-start',
            }}
          >
            <TitleCategory>{category.nameMainCategory?.name}</TitleCategory>
            <ItemCategory>
              {category.parentCategory?.map(prtCategory => (
                <Checkbox
                  style={{ margin: '0', padding: '10px 0px' }}
                  onChange={onChange}
                >
                  {prtCategory.name}
                </Checkbox>
              ))}
            </ItemCategory>
          </List.Item>
        )}
      />
    </div>
  );
};

export default FillterCategory;
