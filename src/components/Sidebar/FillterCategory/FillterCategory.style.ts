import styled from 'styled-components';

export const HeaderCategory = styled.h1`
  text-align: center;
  margin-top: 20px;
`;

export const TitleCategory = styled.h2`
  margin-left: 20px;
`;

export const ItemCategory = styled.div`
  margin-left: 20px;
  display: flex;
  flex-direction: column;
`;
