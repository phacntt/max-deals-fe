import { Card, Menu } from 'antd';
import SubMenu from 'antd/lib/menu/SubMenu';
import React, { FC, useEffect, useState } from 'react';
import {
  DoubleRightOutlined,
  RightOutlined,
  UserOutlined,
} from '@ant-design/icons';
import { Link } from 'react-router-dom';
import { categoryService, dealService } from 'services';
import { Category } from 'types/Category';
import { QueryGetCategory } from 'apis/category.api';
import FillterCategory from './FillterCategory/FillterCategory';
import {
  ItemCategory,
  ListItemCategory,
  SideBarListItemCategory,
  SidlebarHeader,
} from './Sidebar.style';

const Sidebar: FC = () => {
  const [parentCategories, setParentCategories] = useState<Category[]>([]);
  const [mainCategories, setMainCategories] = useState<Category[]>([]);
  const [categories, setCategories] = useState<Category[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      const getCategorySidebar = await categoryService.getAllCategory();
      setCategories(getCategorySidebar);

      const mainCategory = getCategorySidebar.filter(
        category => category.parentCategory === null,
      );
      const parentCategory = getCategorySidebar.filter(
        category => category.parentCategory !== null,
      );
      setMainCategories(mainCategory);
      setParentCategories(parentCategory);
    };
    fetchData();
  }, []);

  return (
    <div style={{ position: 'fixed', width: '18%' }}>
      {/* <SidlebarHeader>CATEGORY</SidlebarHeader>
      <Menu
        mode="inline"
        defaultSelectedKeys={['1']}
        defaultOpenKeys={['sub1']}
        style={{ height: '100%', borderRight: 0 }}
      >
        {mainCategories.map(category => (
          <Link to={`/category/${category.id}`}>
            <Menu.Item
              style={{ paddingLeft: '25px' }}
              key={category.id}
              icon={<DoubleRightOutlined />}
              title={category.name}
            >
              {category.name}
            </Menu.Item>
          </Link>
        ))}
      </Menu> */}
      <SideBarListItemCategory title="CATEGORY">
        <ListItemCategory>
          {mainCategories.map(category => (
            <ItemCategory href={`/deals/category/${category.slug}`}>
              <div style={{ paddingLeft: 12 }}>{category.name}</div>
              <RightOutlined />
            </ItemCategory>
          ))}
        </ListItemCategory>
      </SideBarListItemCategory>

      {/* </Affix> */}
      {/* <FillterCategory
    mainCategories={mainCategories}
    parentCategories={parentCategories}
  /> */}
    </div>
  );
};

export default Sidebar;
