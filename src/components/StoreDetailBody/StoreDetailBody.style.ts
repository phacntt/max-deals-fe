import { Anchor, Card } from 'antd';
import styled from 'styled-components';

export const NameStore = styled.div`
  font-size: 20px;
  font-weight: bold;
`;

export const TreeMenu = styled(Anchor)`
  margin-top: 30px;
  background-color: white;

  .ant-anchor-fixed {
    margin: 15px;
  }

  .ant-anchor-link {
    padding: 10px 0 10px 16px;
    .ant-anchor-link-title {
      height: 30px;
      font-weight: 500;
      &:hover {
        text-decoration: underline;
      }
    }
  }
`;

export const CardTitle = styled(Card)`
  .ant-card-head {
    background-color: #e73948;
    .ant-card-head-title {
      font-size: 18px;
      color: white;
      font-weight: 700;
    }
  }
`;
