import { EnvironmentFilled, PhoneFilled } from '@ant-design/icons';
import { Anchor, Card, Col, Divider, List, Row } from 'antd';
import DealCard from 'components/DealCard/DealCard';
import MapsDeal from 'components/Maps/MapsDeal';
import StoreRelate from 'components/StoreRelate/StoreRelate';
import React, { FC, useEffect } from 'react';
import { Deal } from 'types/Deal';
import { File } from 'types/File';
import { Store } from 'types/Store';
import { CardTitle, NameStore, TreeMenu } from './StoreDetailBody.style';

interface GeoLocation {
  lat: number;
  lng: number;
}

interface Props {
  deals: Deal[];
  store: Store;
  dealsRelate: Deal[];
  idStoreRelate: number[];
  location: GeoLocation;
  image: File;
}

const StoreDetailBody: FC<Props> = ({
  deals = [],
  store,
  dealsRelate = [],
  idStoreRelate = [],
  location,
  image,
}) => {
  const { Link } = Anchor;

  return (
    <Row gutter={16}>
      <Col span={4}>
        <NameStore>{store.name.toUpperCase()}</NameStore>
        <TreeMenu affix={false}>
          <Link href="#list-deals" title="Deals" />
          <Link href="#list-deals-interested" title="Deals Interested" />
          <Link href="#about" title="About" />
          <Link href="#store-address" title="Address" />
        </TreeMenu>
      </Col>
      <Col span={14}>
        <CardTitle id="list-deals" title={`Deals Valid (${deals.length})`}>
          <Row gutter={18}>
            {deals.map(deal => (
              <Col span={8}>
                <DealCard deal={deal} />
              </Col>
            ))}
          </Row>
        </CardTitle>
        <CardTitle
          id="list-deals-interested"
          style={{
            margin: '20px 0',
          }}
          title="Deals you may be interested in"
        >
          <Row gutter={18}>
            {dealsRelate.map(deal => (
              <Col span={8}>
                <DealCard deal={deal} />
              </Col>
            ))}
          </Row>
        </CardTitle>

        <CardTitle id="about" title={`Introduce about ${store.name}`}>
          <Divider orientation="center">
            <img
              style={{
                maxHeight: 60,
                maxWidth: '100%',
              }}
              src={image.url}
              alt="imageStore"
            />
          </Divider>
          <div dangerouslySetInnerHTML={{ __html: store.description }} />
        </CardTitle>

        <CardTitle
          style={{
            margin: '20px 0',
          }}
          id="store-address"
          title="Address"
        >
          <div style={{ marginBottom: 20, fontSize: 15, fontWeight: 400 }}>
            <EnvironmentFilled /> {store.fullAddress}
          </div>
          {location && <MapsDeal location={location} />}
        </CardTitle>
      </Col>
      <Col span={6}>
        <CardTitle title="Store Relate">
          <List>
            {idStoreRelate.map(id => (
              <StoreRelate idStore={id} />
            ))}
          </List>
        </CardTitle>
      </Col>
    </Row>
  );
};

export default StoreDetailBody;
