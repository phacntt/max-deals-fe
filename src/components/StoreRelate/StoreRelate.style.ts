import styled from 'styled-components';

export const StoreRelateName = styled.div`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  font-size: 16px;
  font-weight: bold;
  color: black;
`;

export const StoreRelateAddress = styled.div`
  white-space: nowrap;
  overflow: hidden;
  color: black;
  text-overflow: ellipsis;
`;
