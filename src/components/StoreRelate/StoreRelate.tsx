import { Row, Col } from 'antd';
import React, { FC, useEffect, useState } from 'react';
import { fileService, storeService } from 'services';
import { File } from 'types/File';
import { Store } from 'types/Store';
import { StoreRelateAddress, StoreRelateName } from './StoreRelate.style';

interface Props {
  idStore: number;
}

const StoreRelate: FC<Props> = ({ idStore }) => {
  const [store, setStore] = useState<Store>();
  const [image, setImage] = useState<File>();

  useEffect(() => {
    const fetchData = async () => {
      const storeRelateInfo = await storeService.getStoreById(idStore);
      setStore(storeRelateInfo);
      const imageStore = await fileService.getFileById(idStore, 'StoreEntity');
      setImage(imageStore[0]);
    };
    fetchData();
  }, []);

  return (
    <a href={`/store/${store?.id}`}>
      <Row gutter={8} style={{ marginBottom: 10 }}>
        <Col span={8}>
          <img
            style={{
              width: '100%',
              height: '100%',
            }}
            src={image?.url}
            alt=""
            srcSet=""
          />
        </Col>
        <Col span={16}>
          <Row style={{ lineHeight: '25px' }}>
            <Col span={24}>
              <a href={`/store/${store?.slug}`}>
                <StoreRelateName>{store?.name}</StoreRelateName>
              </a>
            </Col>
            <Col span={24}>
              <StoreRelateAddress>{store?.fullAddress}</StoreRelateAddress>
            </Col>
            <Col span={24}>
              <div style={{ color: 'black' }}>{store?.phone}</div>
            </Col>
          </Row>
        </Col>
      </Row>
    </a>
  );
};

export default StoreRelate;
