import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const DealItemContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  background-size: cover;
  flex-direction: column;
  padding: 10px;
  position: relative;
`;

export const DealItemBook = styled(Link)`
  padding: 5px 10px;
  background: white;
  color: black;
  border-radius: 8px;
  font-size: 16px;
  font-weight: bold;
  z-index: 1;
`;

export const DealItemStoreName = styled(Link)`
  margin-top: 20px;
  color: white;
  font-size: 18px;
  font-weight: bold;
  z-index: 1;
`;

export const DealItemOffer = styled.div`
  padding: 0px 15px;
  border-top: 2px solid white;
  border-bottom: 1px solid white;
  width: max-content;
  margin-top: 20px;
  font-size: 18px;
  color: white;
  z-index: 1;
`;

export const DealItemGradient = styled.img`
  position: absolute;
  bottom: 0px;
  left: 0px;
  right: 0px;
  width: 100%;
  height: 50%;
  z-index: 0;
  display: block;
`;
