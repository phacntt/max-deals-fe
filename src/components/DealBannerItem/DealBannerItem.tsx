import { Button, Card } from 'antd';
import { Meta } from 'antd/lib/list/Item';
import { url } from 'inspector';
import { relative } from 'path';
import React, { FC, useEffect, useState } from 'react';

import { Link } from 'react-router-dom';
import { fileService } from 'services';
import { Deal } from 'types/Deal';
import { File } from 'types/File';
import {
  DealItemBook,
  DealItemContainer,
  DealItemGradient,
  DealItemOffer,
  DealItemStoreName,
} from './DealBannerItem.style';

interface Props {
  deal: Deal;
}

const DealBannerItem: FC<Props> = ({ deal }) => {
  const [fileImage, setFileImage] = useState<File>();
  useEffect(() => {
    const fetchData = async () => {
      setFileImage(deal.images.shift());
    };
    fetchData();
  }, []);

  return (
    <DealItemContainer
      style={{
        backgroundImage: `url(${fileImage?.url})`,
        backgroundSize: 'cover',
      }}
    >
      <DealItemBook to={`/deals/${deal.slug}`}>Booking</DealItemBook>
      <DealItemStoreName to={`/deals/${deal.store.slug}`}>
        {deal.store.name}
      </DealItemStoreName>
      <DealItemOffer>- {deal.discount}%</DealItemOffer>
      <DealItemGradient src="/img-gradient.png" alt="none" />
    </DealItemContainer>
  );
};

export default DealBannerItem;
