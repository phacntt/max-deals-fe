import { Col, Layout, Row } from 'antd';
import AccountButton from 'components/AccountButton/AccountButton';
import Container from 'components/Container/Container';
import { Logo, InputWrapper } from 'components/Header/Header.style';
import SearchInput from 'components/SearchInput/SearchInput';
import React, { FC, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { dealService } from 'services';
import { Deal } from 'types/Deal';

const { Header } = Layout;

interface Props {
  fixed: boolean;
}

const AppHeaderAdmin: FC<Props> = ({ fixed }) => {
  const onSearch = (text: string) => {
    console.log(text);
  };

  // useEffect(() => {
  //   const fetchData = async () => {
  //     const dataDeals = await dealService.getAllDeal();
  //     setDeals(dataDeals);
  //   };
  //   fetchData();
  // }, []);
  return (
    <Header
      style={{
        position: fixed ? 'fixed' : 'initial',
        width: '100%',
        zIndex: 3,
        background: 'white',
      }}
      className="header"
    >
      <Container>
        <Row>
          <Col span={8}>
            <Link style={{ color: 'black', marginLeft: '10px' }} to="/">
              <Logo src="/jamja-logo-2018-2x.png" alt="" />
            </Link>
          </Col>
          <Col span={8}>
            <InputWrapper>
              <SearchInput onSearch={onSearch} />
            </InputWrapper>
          </Col>
          <Col span={8}>
            <AccountButton />
          </Col>
        </Row>
      </Container>
    </Header>
  );
};

export default AppHeaderAdmin;
