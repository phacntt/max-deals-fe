import { Card, Image } from 'antd';
import Meta from 'antd/lib/card/Meta';
import React, { FC } from 'react';
import { Link } from 'react-router-dom';
import { Deal } from 'types/Deal';
import { File } from 'types/File';

interface Props {
  image: File;
}

const DealBanner: FC<Props> = ({ image }) => {
  return (
    <div>
      <img
        style={{
          height: 450,
          width: '100%',
          backgroundSize: 'cover',
        }}
        alt="example"
        src={image?.url}
      />
    </div>
  );
};
export default DealBanner;
