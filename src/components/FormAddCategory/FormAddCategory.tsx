import { InfoCircleOutlined } from '@ant-design/icons';
import { Button, Col, Form, Input, Radio, Row, Select } from 'antd';
import form from 'antd/lib/form';
import React, { FC } from 'react';

const FormAddCategory: FC = () => {
  const { Option } = Select;
  return (
    <Form layout="vertical">
      <Row gutter={16}>
        <Col span={8}>
          <Form.Item label="Name Category" required>
            <Input placeholder="input placeholder" />
          </Form.Item>
        </Col>
        <Col span={8}>
          <Form.Item label="Slug">
            <Input placeholder="input placeholder" />
          </Form.Item>
        </Col>
        <Col span={8}>
          <Form.Item label="ParentId">
            <Select defaultValue=".com" className="select-after">
              <Option value=".com">.com</Option>
              <Option value=".jp">.jp</Option>
              <Option value=".cn">.cn</Option>
              <Option value=".org">.org</Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
      <Form.Item>
        <Button type="primary">Submit</Button>
      </Form.Item>
    </Form>
  );
};

export default FormAddCategory;
