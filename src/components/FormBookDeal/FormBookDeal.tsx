import { Alert, Col, DatePicker, Form, Row } from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { getAPIErrorMessage, APIError } from 'apis/api';
import { AddBookRequestBody } from 'apis/book.api';
import { randomCode } from 'helpers/randomCode';
import moment, { Moment } from 'moment';
import React, { FC, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from 'redux/hooks';
import { selectAuth } from 'redux/slices/auth.slice';
import { selectCurrentUser } from 'redux/slices/current-user.slice';
import { bookService } from 'services';
import { Deal } from 'types/Deal';
import { ButtonBook, ButtonDisable, ItemForm } from './FormBookDeal.style';

interface Props {
  id: number;
  deal: Deal;
}

type BookFormValues = {
  codeVerify: string;
  dateBook: moment.Moment;
  user: number;
  deal: number;
};

const FormBookDeal: FC<Props> = ({ id, deal }) => {
  const CODE_LENGTH = 8;

  const navigate = useNavigate();
  const [errorMessage, setErrorMessage] = useState('');
  const [bookForm] = useForm();
  const [inProgress, setInProgress] = useState(false);
  const [status, setStatus] = useState<boolean>();
  const [quantityBook, setQuantityBook] = useState<number>();
  const dispath = useAppDispatch();
  const [description, setDescription] = useState('');

  const auth = useAppSelector(selectAuth);
  const currentUser = useAppSelector(selectCurrentUser);

  const disabledDate = (current: Moment) => {
    return current && current < moment().endOf('days');
  };

  const onChangeDate = (date: Moment) => {
    if (
      moment(deal.expiryDate).diff(date.toISOString() as string, 'days') < 0
    ) {
      setStatus(false);
    } else {
      setStatus(true);
    }
  };

  const handleBookDeal = async (formValue: BookFormValues) => {
    try {
      if (auth.isAuthenticated) {
        const getDateSelect = formValue.dateBook.toISOString();

        console.log(getDateSelect);

        console.log(moment(getDateSelect).add(1, 'day').toISOString());

        const data: AddBookRequestBody = {
          dateBook: formValue.dateBook.toISOString(),
          dateExpiryBook: moment(getDateSelect).add(1, 'day').toISOString(),
          codeVerify: randomCode(CODE_LENGTH),
          status: 'Unused',
          dealId: id,
          userId: Number(currentUser.id),
        };

        if (status === false) {
          setErrorMessage(
            'The booking date must be before the expiration date',
          );
        }

        if (status === true) {
          const resp = await bookService.createBook(data);
          if (resp) {
            return navigate('/book/result', {
              state: {
                book: data,
                user: currentUser,
                deal,
              },
            });
          }

          setErrorMessage('Create Book is Failure');
        }
      }
    } catch (error: unknown) {
      setErrorMessage(getAPIErrorMessage(error as APIError));
    }

    return setInProgress(false);
  };

  return (
    <Form
      layout="vertical"
      name="addBook"
      form={bookForm}
      onFinish={handleBookDeal}
      encType="multipart/form-data"
    >
      {errorMessage && (
        <Alert
          style={{
            margin: '10px 0',
          }}
          message={errorMessage}
          type="error"
          showIcon
        />
      )}

      <Row gutter={16}>
        <Col span={10}>
          <ItemForm label="Date Book" name="dateBook">
            <DatePicker
              disabledDate={disabledDate}
              onChange={date => onChangeDate(date as Moment)}
            />
          </ItemForm>
        </Col>
        <Col span={14}>
          <ItemForm
            style={{
              marginLeft: 20,
            }}
            label="Expiry Date"
            name="expiryDate"
          >
            {moment(deal.expiryDate).format('YYYY-MM-DD')}
          </ItemForm>
        </Col>

        <Col span={24}>
          {!auth.isAuthenticated ? (
            <Form.Item>
              <ButtonDisable disabled block>
                Please Login Before Get Deal
              </ButtonDisable>
            </Form.Item>
          ) : (
            <Form.Item>
              <ButtonBook block htmlType="submit" loading={inProgress}>
                Get Deal
              </ButtonBook>
            </Form.Item>
          )}

          {/* {auth.isAuthenticated && (
            
          )} */}

          {/* {auth.isAuthenticated && (
            <ButtonDisable block disabled>
              Each account only gets 1 discount code on 1 deal
            </ButtonDisable>
          )} */}
        </Col>
      </Row>
    </Form>
  );
};

export default FormBookDeal;
