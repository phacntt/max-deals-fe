import { Button, Form } from 'antd';
import styled from 'styled-components';

export const ButtonBook = styled(Button)`
  background-color: #e73948;
  height: 40px;
  border-radius: 5px;
  font-size: 17px;
  font-weight: 700;
  color: white;
  &:hover {
    background-color: #e73948;
    color: white;
    border: none;
  }
`;

export const ButtonDisable = styled(Button)`
  background-color: #e73948;
  height: 40px;
  border-radius: 5px;
  font-size: 14px;
  font-weight: 700;
  color: white;
  &:hover {
    background-color: #e73948;
    color: white;
    border: none;
  }
`;

export const ItemForm = styled(Form.Item)`
  .ant-form-item-label {
    label {
      font-weight: 700;
    }
  }
  .ant-form-item-control-input {
    margin: 10px 0;
    .ant-form-item-control-input-content {
      font-weight: 700;
      font-size: 21px;
    }
  }
  #addBook_dateBook {
    font-size: 16px;
    font-weight: 500;
  }
`;
