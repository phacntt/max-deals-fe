import { Card } from 'antd';
import FormBookDeal from 'components/FormBookDeal/FormBookDeal';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import { Deal } from 'types/Deal';
import { CardTilte } from './InfoBookDeal.style';

interface Props {
  id: number;
  deal: Deal;
}

const InfoRegisterDeal: FC<Props> = ({ id, deal }) => {
  const [dayLeft, setDayLeft] = useState<string>();
  useEffect(() => {
    const getDayLeft = () => {
      const dateLeft = moment(deal.expiryDate).diff(
        moment().toISOString() as string,
        'days',
      );
      setDayLeft(dateLeft.toString());
    };
    getDayLeft();
  }, []);

  return (
    <CardTilte title={`ĐẶT CHỖ NHANH ( CÒN ${dayLeft} NGÀY )`}>
      {console.log(dayLeft)}
      <FormBookDeal id={id} deal={deal} />
    </CardTilte>
  );
};

export default InfoRegisterDeal;
