import { Card } from 'antd';
import styled from 'styled-components';

export const CardTilte = styled(Card)`
  .ant-card-head {
    background-color: #e73948;
  }
  .ant-card-body {
    padding: 5px;
  }
  .ant-card-head-title {
    color: white;
    font-weight: bold;
    font-size: 18px;
  }
`;
