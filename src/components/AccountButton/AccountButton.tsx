import { LoginOutlined, LogoutOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Popover } from 'antd';
import React, { FC } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import { useAppDispatch, useAppSelector } from 'redux/hooks';
import { logoutAsync, selectAuth } from 'redux/slices/auth.slice';
import { selectCurrentUser } from 'redux/slices/current-user.slice';

const AccountButton: FC = () => {
  const dispath = useAppDispatch();
  const auth = useAppSelector(selectAuth);
  const currentUser = useAppSelector(selectCurrentUser);
  const navigate = useNavigate();

  const logoutAccount = () => {
    navigate('/');
    dispath(logoutAsync());
  };

  const content = (
    <>
      {currentUser?.role === 'user' && (
        <div>
          <Button type="link" href="/user">
            <UserOutlined />
            User Detail
          </Button>
        </div>
      )}

      {currentUser?.role === 'admin' && (
        <div>
          <Button type="link" href="/admin">
            <LoginOutlined />
            Log Admin Page
          </Button>
        </div>
      )}

      {currentUser?.role === 'super_admin' && (
        <div>
          <Button type="link" href="/superadmin">
            <LoginOutlined />
            Log Super Admin Page
          </Button>
        </div>
      )}

      <div>
        {auth.isAuthenticated && (
          <Button
            type="link"
            onClick={() => logoutAccount()}
            style={{
              color: 'red',
            }}
          >
            <LogoutOutlined /> Logout
          </Button>
        )}
      </div>
    </>
  );

  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        justifyContent: 'flex-end',
        padding: '0 10px',
      }}
    >
      {!auth.isAuthenticated && (
        <Button
          style={{
            color: 'black',
            border: '1px solid black',
          }}
          type="link"
          href="/login"
        >
          <LoginOutlined /> Login
        </Button>
      )}
      {auth.isAuthenticated && (
        <Button
          style={{
            color: 'black',
            border: '1px solid black',
          }}
          type="link"
          href="/user"
        >
          <Popover content={content} placement="bottom">
            {' '}
            <UserOutlined /> {currentUser.username}
          </Popover>
        </Button>
      )}
    </div>
  );
};

export default AccountButton;
