import { Col } from 'antd';
import { ItemResultRes } from 'components/Header/Header.style';
import React, { FC } from 'react';
import { Link } from 'react-router-dom';
import { Deal } from 'types/Deal';

interface Props {
  deal: Deal;
}

const ResultItem: FC<Props> = ({ deal }) => {
  return (
    <Link
      style={{ display: 'inline', float: 'none' }}
      to={`/deals/${deal.slug}`}
    >
      <ItemResultRes>{deal.name}</ItemResultRes>
    </Link>
  );
};

export default ResultItem;
