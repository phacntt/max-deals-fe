import { Form, List, Spin } from 'antd';
import Search from 'antd/lib/input/Search';
import { ItemResultRes } from 'components/Header/Header.style';
import moment from 'moment';
import React, {
  ChangeEvent,
  ChangeEventHandler,
  FC,
  useEffect,
  useState,
} from 'react';
import { Link } from 'react-router-dom';
import { dealService } from 'services';
import { Deal } from 'types/Deal';
import ResultItem from './ResultItem';

interface Props {
  onSearch(text: string): void;
}

const SearchInput: FC<Props> = ({ onSearch }) => {
  const [deals, setDeals] = useState<Deal[]>([]);
  const [dealsFake, setDealsFake] = useState<Deal[]>([]);

  const [filteredDeals, setFilteredDeals] = useState<Deal[]>();
  const [inProgress, setInProgress] = useState(false);
  const [key, setKey] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      const dataDeals = await dealService.getAllDeal({
        expiryDate: moment().toISOString(),
      });
      setDeals(dataDeals);
    };
    fetchData();
  }, []);

  let filterTimeout: NodeJS.Timeout;

  const dealsFilter = (query: string) => {
    clearTimeout(filterTimeout);
    if (!query) {
      setFilteredDeals([]);
      return;
    }

    filterTimeout = setTimeout(() => {
      console.log('====>', query);
      setFilteredDeals(
        deals.filter(deal =>
          deal.name.toLowerCase().includes(query.toLowerCase()),
        ),
      );
      setInProgress(false);
    }, 2000);
  };

  return (
    <>
      {console.log(filteredDeals)}
      <Form style={{ width: '100%' }}>
        <Form.Item style={{ marginBottom: '0px' }}>
          <Search
            style={{
              width: '100%',
            }}
            placeholder="Search Deals here...."
            onSearch={onSearch}
            onChange={(e: ChangeEvent<HTMLInputElement>) => {
              setInProgress(true);
              setKey(e.target.value);
              dealsFilter(e.target.value);
            }}
            enterButton
          />
        </Form.Item>
      </Form>
      {key.length !== 0 ? (
        <div
          style={{
            position: 'absolute',
            width: '100%',
            top: 48,
            height: '340px',
            overflow: 'auto',
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          <List
            style={{ background: 'white' }}
            bordered
            dataSource={filteredDeals}
            loading={inProgress}
            renderItem={item => <ResultItem deal={item} />}
          />
        </div>
      ) : (
        ''
      )}
    </>
  );
};

export default SearchInput;
