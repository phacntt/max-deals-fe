import { Card } from 'antd';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const FillterCardItem = styled(Link)`
  padding: 10px 0;
`;

export const QuickFillter = styled(Link)`
  color: red;
  padding: 0 20px;
  &:hover {
    color: red;
    text-decoration: underline;
    font-weight: bold;
  }
  &::after {
    display: block;
    content: '';
    width: 1px;
    background-color: #ededed;
    height: calc(100% - 6px);
    position: absolute;
    right: 0;
    top: 50%;
    transform: translateY(-50%);
  }
`;

export const HeadingOffer = styled.div`
  font-size: 20px;
  font-weight: bold;
  padding-bottom: 8px;
  &::before {
    display: block;
    position: absolute;
    content: '';
    height: 2px;
    max-width: 120px;
    min-width: 40px;
    width: 70%;
    bottom: 0;
    left: 0;
    background-color: #e73948;
  }
`;

export const HeadingFillterOffer = styled.div`
  font-size: 20px;
  font-weight: bold;
  padding-bottom: 10px;
  &::after {
    display: block;
    position: absolute;
    content: '';
    height: 2px;
    max-width: 120px;
    min-width: 40px;
    width: 70%;
    bottom: 0;
    left: 0;
    background-color: #e73948;
  }
`;
