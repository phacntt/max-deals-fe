import { Row, Col, Card, Checkbox } from 'antd';
import Column from 'antd/lib/table/Column';
import DealCard from 'components/DealCard/DealCard';
import DealCardByCategory from 'components/DealCardByCategory/DealCardByCategory';
import DealNearMe from 'components/DealNearMe/DealNearMe';
import DealSlideByCate from 'components/DealSlideByCate/DealSlideByCate';
import FilterSideBarCategory from 'components/FilterSideBarCategory/FilterSideBarCategory';
import ListDealCategory from 'components/ListDealCategory/ListDealCategory';
import MapNearMe from 'components/Maps/MapNearMe';
import FillterCategory from 'components/Sidebar/FillterCategory/FillterCategory';
import React, { FC, useState } from 'react';
import { Link, useLocation, useParams } from 'react-router-dom';
import { Category } from 'types/Category';
import { Deal } from 'types/Deal';
import { HeadingOffer, QuickFillter } from './DealMainCategory.style';

interface Props {
  mainCategory: Category[];
  categories: Category[];
  listDeal: Deal[];
}

const DealMainCategory: FC<Props> = ({
  listDeal = [],
  mainCategory = [],
  categories = [],
}) => {
  const location = useLocation();
  const [quantity, setQuantity] = useState<number>();
  const handleQuantityDealNear = (quantityDeal: number) => {
    setQuantity(quantityDeal);
  };

  return (
    <Row style={{ marginTop: 25 }}>
      <Col span={6}>
        <Row>
          <Col span={24}>
            <HeadingOffer>FILLTER ADVANCE</HeadingOffer>
          </Col>
        </Row>
        <br />
        {mainCategory.map(category => (
          <FilterSideBarCategory categories={categories} category={category} />
        ))}
      </Col>
      <Col span={18}>
        <Row style={{ marginBottom: 20 }}>
          <Col span={8}>
            {location.pathname === '/deals/nearme' ? (
              <HeadingOffer>{quantity} PROMOTION</HeadingOffer>
            ) : (
              <HeadingOffer>{listDeal.length} PROMOTION</HeadingOffer>
            )}
          </Col>
          <Col span={16}>
            <div
              style={{
                paddingBottom: 10,
                display: 'flex',
                justifyContent: 'space-around',
              }}
            >
              <QuickFillter to="/deals">News</QuickFillter>
              <QuickFillter to="/deals/nearme">Near</QuickFillter>
              <QuickFillter to="/deals?big-discount=true">
                Big Discount
              </QuickFillter>
              <QuickFillter to="/deals?expiration-soon=true">
                Expiration soon
              </QuickFillter>
            </div>
          </Col>
        </Row>
        {location.pathname !== '/deals/nearme' && (
          <>
            <DealSlideByCate deals={listDeal} />
            <DealCardByCategory deals={listDeal} />
          </>
        )}
        {location.pathname === '/deals/nearme' && (
          <DealNearMe quantityDealNear={handleQuantityDealNear} />
        )}
      </Col>
    </Row>
  );
};

export default DealMainCategory;
