import { Carousel } from 'antd';
import React, { FC, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { dealService } from 'services';
import { Deal } from 'types/Deal';
import { BannerItem } from './Banner.style';
import DetailBanner from './DetailBanner';

interface Props {
  deals: Deal[];
}

const Banner: FC<Props> = ({ deals = [] }) => {
  return (
    <Carousel autoplay>
      {deals.length < 4
        ? deals.map(deal => <DetailBanner deal={deal} />)
        : deals.slice(0, 4).map(deal => <DetailBanner deal={deal} />)}
    </Carousel>
  );
};

export default Banner;
