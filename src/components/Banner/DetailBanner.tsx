import { Carousel } from 'antd';
import React, { FC, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { dealService, fileService } from 'services';
import { Deal } from 'types/Deal';
import { File } from 'types/File';
import { BannerItem } from './Banner.style';

interface Props {
  deal: Deal;
}

const DetailBanner: FC<Props> = ({ deal }) => {
  const [image, setImage] = useState<File>();
  useEffect(() => {
    const fetchData = async () => {
      setImage(deal.images[0]);
    };
    fetchData();
  }, []);

  return (
    <div>
      <Link to={`/deals/${deal.slug}`}>
        <BannerItem>
          <img
            style={{
              height: 'inherit',
              width: '100%',
              // objectFit: 'contain',
              backgroundSize: 'cover',
            }}
            src={deal.images[0].url}
            alt="ok"
            srcSet=""
          />
        </BannerItem>
      </Link>
    </div>
  );
};

export default DetailBanner;
