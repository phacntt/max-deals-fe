import styled from 'styled-components';

export const BannerItem = styled.div`
  height: 350px;
  color: #fff;
  line-height: 350px;
  text-align: center;
  background: #364d79;
`;
