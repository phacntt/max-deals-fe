import Item from 'antd/lib/list/Item';
import styled from 'styled-components';

export const ItemStoreAddress = styled(Item)`
  border-left: 4px solid #e73948;
  padding-left: 20px;
  background-color: #fafafa;
  color: rgb(102, 102, 102);
`;
