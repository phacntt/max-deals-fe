import { EnvironmentFilled, PhoneOutlined } from '@ant-design/icons';
import { Col, List, Row } from 'antd';
import Item from 'antd/lib/descriptions/Item';
import MapsDeal from 'components/Maps/MapsDeal';
import React, { FC, useEffect, useState } from 'react';
import { provinceService } from 'services';
import { Province } from 'types/Province';
import { Store } from 'types/Store';
import { ItemStoreAddress } from './DealDetailAddress.style';

interface Props {
  store: Store;
}

interface GeoLocation {
  lat: number;
  lng: number;
}

const DealDetailAddress: FC<Props> = ({ store }) => {
  const [province, setProvince] = useState<Province>();
  const [location, setLocation] = useState<GeoLocation>();
  useEffect(() => {
    const fetchData = async () => {
      const nameProvince = await provinceService.getProvinceById(
        store.province,
      );
      setProvince(nameProvince);
      const located: GeoLocation = {
        lat: store.lat,
        lng: store.lng,
      };
      setLocation(located);
    };
    fetchData();
  }, []);

  return (
    <>
      <div style={{ marginBottom: 20 }}>
        Applied in <b>{province?.name}</b> area{' '}
      </div>

      <Row gutter={16}>
        <Col span={10}>
          {location && <MapsDeal location={location as GeoLocation} />}
        </Col>
        <Col span={14}>
          <ItemStoreAddress>
            <EnvironmentFilled /> {store.fullAddress}
          </ItemStoreAddress>
          <ItemStoreAddress>
            <PhoneOutlined /> {store.phone}
          </ItemStoreAddress>
        </Col>
      </Row>
    </>
  );
};
export default DealDetailAddress;
