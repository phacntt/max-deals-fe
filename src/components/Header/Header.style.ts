import { List } from 'antd';
import Item from 'antd/lib/list/Item';
import styled from 'styled-components';

export const InputWrapper = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

export const Logo = styled.img`
  width: 120px;
  height: 59px;
`;

export const ItemResultRes = styled(Item)`
  display: block;
  white-space: nowrap;
  overflow: hidden;
  color: black;
  text-overflow: ellipsis;
`;
