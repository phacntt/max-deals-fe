import {
  Layout,
  Menu,
  Row,
  Col,
  Form,
  Input,
  Button,
  Image,
  List,
  Typography,
} from 'antd';
import { SearchOutlined, UserOutlined } from '@ant-design/icons';
import 'antd/dist/antd.css';
import Container from 'components/Container/Container';
import React, { FC, useEffect, useState } from 'react';
import SearchInput from 'components/SearchInput/SearchInput';
import AccountButton from 'components/AccountButton/AccountButton';
import { Link } from 'react-router-dom';
import { Deal } from 'types/Deal';
import { dealService } from 'services';
import { InputWrapper, ItemResultRes, Logo } from './Header.style';

const { Header } = Layout;

interface Props {
  fixed: boolean;
}

const AppHeader: FC<Props> = ({ fixed }) => {
  const onSearch = (text: string) => {
    console.log(text);
  };

  return (
    <Header
      style={{
        position: fixed ? 'fixed' : 'initial',
        width: '100%',
        zIndex: 3,
        background: 'white',
      }}
      className="header"
    >
      <Container>
        <Row>
          <Col span={8}>
            <Link style={{ color: 'black', marginLeft: '10px' }} to="/">
              <Logo src="/jamja-logo-2018-2x.png" alt="" />
            </Link>
          </Col>
          <Col span={8}>
            <InputWrapper>
              <SearchInput onSearch={onSearch} />
            </InputWrapper>
          </Col>
          <Col span={8}>
            <AccountButton />
          </Col>
        </Row>
      </Container>
    </Header>
  );
};

export default AppHeader;
