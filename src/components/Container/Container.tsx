import styled from 'styled-components';

interface ContainerProps {
  direction?: 'vertical' | 'horizontal';
  flex?: boolean;
}

const Container = styled.div<ContainerProps>`
  width: 100%;
  margin: 0 auto;
  padding: 0 1rem;
  display: ${p => (p.flex ? 'flex' : 'block')};
  flex-direction: ${p => (p.direction === 'vertical' ? 'column' : 'row')};

  @media (min-width: 576px) {
    max-width: 576px;
  }

  @media (min-width: 768px) {
    max-width: 768px;
  }

  @media (min-width: 992px) {
    max-width: 992px;
  }

  @media (min-width: 1200px) {
    max-width: 1200px;
  }

  @media (min-width: 1600px) {
    max-width: 1600px;
  }
`;

export default Container;
