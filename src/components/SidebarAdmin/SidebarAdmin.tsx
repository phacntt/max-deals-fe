import {
  LaptopOutlined,
  DashboardOutlined,
  HomeOutlined,
  TagOutlined,
  UserOutlined,
} from '@ant-design/icons';
import { Menu } from 'antd';
import Sider from 'antd/lib/layout/Sider';
import SubMenu from 'antd/lib/menu/SubMenu';
import React, { FC, useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from 'redux/hooks';
import { selectAuth } from 'redux/slices/auth.slice';
import { selectCurrentUser } from 'redux/slices/current-user.slice';
import { storeService, userService } from 'services';
import { Store } from 'types/Store';
import { User } from 'types/User';

const SidebarAdmin: FC = () => {
  const dispath = useAppDispatch();
  const auth = useAppSelector(selectAuth);
  const currentUser = useAppSelector(selectCurrentUser);

  const [user, setUser] = useState<User>();
  const [store, setStore] = useState<Store>();

  useEffect(() => {
    const fetchData = async () => {
      const infoUser = await userService.getUserById(Number(currentUser.id));
      setUser(infoUser);
      const infoStore = await storeService.getStoreById(infoUser.storeId);
      setStore(infoStore);
    };
    fetchData();
  }, [user]);

  return (
    <Sider
      style={{
        flex: '1 auto',
        zIndex: 2,
        height: '100%',
        position: 'fixed',
      }}
      width={240}
      className="site-layout-background"
    >
      <Menu
        mode="inline"
        defaultSelectedKeys={['1']}
        defaultOpenKeys={['sub1']}
        style={{ height: '100%', borderRight: 0 }}
      >
        <SubMenu key="sub1" icon={<DashboardOutlined />} title="DashBoard">
          <Menu.Item key="1">
            <Link to="/admin">HomePage</Link>
          </Menu.Item>
        </SubMenu>
        <SubMenu key="sub2" icon={<TagOutlined />} title="Deal Manage">
          <Menu.Item key="5">
            <Link to="/admin/dealmanage">List Deals</Link>
          </Menu.Item>
          <Menu.Item key="6">
            <Link to="/admin/dealmanage/add">Add New Deal</Link>
          </Menu.Item>
        </SubMenu>
        <SubMenu key="sub3" icon={<UserOutlined />} title="Book Manage">
          <Menu.Item key="7">
            <Link to="/admin/bookmanage">List Booking</Link>
          </Menu.Item>
          <Menu.Item key="8">
            <Link to="/admin/bookmanage/checkbook">Check Booking</Link>
          </Menu.Item>
        </SubMenu>
        <SubMenu key="sub4" icon={<HomeOutlined />} title="Store Manage">
          <Menu.Item key="9">
            <Link to={`/admin/infomation/${store?.slug}`}>
              Infomation Store
            </Link>
          </Menu.Item>
        </SubMenu>
      </Menu>
    </Sider>
  );
};

export default SidebarAdmin;
