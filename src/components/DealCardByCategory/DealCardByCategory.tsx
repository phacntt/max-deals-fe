import { Col, Row } from 'antd';
import DealCard from 'components/DealCard/DealCard';
import React, { FC } from 'react';
import { Deal } from 'types/Deal';

interface Props {
  deals: Deal[];
}

const DealCardByCategory: FC<Props> = ({ deals }) => {
  return (
    <Row gutter={[16, 24]}>
      {deals.map(item => (
        <Col span={8}>
          <DealCard deal={item} />
        </Col>
      ))}
    </Row>
  );
};

export default DealCardByCategory;
