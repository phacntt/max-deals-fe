import { Button, Select, Skeleton, Spin } from 'antd';
import { QueryGetDeal } from 'apis/deal.api';
import DealCardByCategory from 'components/DealCardByCategory/DealCardByCategory';
import MapNearMe from 'components/Maps/MapNearMe';
import React, { FC, MouseEventHandler, useEffect, useState } from 'react';
import { dealService } from 'services';
import { Deal } from 'types/Deal';

interface Props {
  quantityDealNear: (quantity: number) => void;
}

interface GeoLocation {
  lat: number;
  lng: number;
}

const DealNearMe: FC<Props> = ({ quantityDealNear }) => {
  const [location, setLocation] = useState<GeoLocation>();
  const [deals, setDeals] = useState<Deal[]>([]);
  const [radius, setRadius] = useState(100);
  const { Option } = Select;

  const handleChange = (value: string) => {
    console.log('radius', value);
    setRadius(Number(value));
  };

  useEffect(() => {
    if (!location) {
      return;
    }
    const fetchData = async () => {
      const listDealNearMe = await dealService.getAllDeal({
        center_lat: location?.lat,
        center_lng: location?.lng,
        center_radius: radius,
      });

      const sortDeals = listDealNearMe.sort(
        (a, b) => a.store.distance - b.store.distance,
      );

      setDeals(sortDeals);
      quantityDealNear(listDealNearMe.length);
    };
    fetchData();
  }, [radius, location]);

  const handleLocation = (locations: GeoLocation) => {
    setLocation(locations);
  };

  return (
    <>
      <div style={{ textAlign: 'right' }}>
        <span style={{ marginRight: 20, fontWeight: 600, fontSize: 18 }}>
          Search Range (Unit: Km)
        </span>
        <Select
          defaultValue="500"
          style={{ width: 120 }}
          onChange={handleChange}
        >
          <Option value="500">0.5 Km</Option>
          <Option value="1000">1 Km</Option>
          <Option value="2000">2 Km</Option>
          <Option value="3000">3 Km</Option>
          <Option value="4000">4 Km</Option>
          <Option value="5000">5 Km</Option>
          <Option value="7000">7 Km</Option>
          <Option value="10000">10 Km</Option>
        </Select>
      </div>
      <br />

      <MapNearMe deals={deals} onChange={handleLocation} />
      <br />

      <DealCardByCategory deals={deals} />
    </>
  );
};

export default DealNearMe;
