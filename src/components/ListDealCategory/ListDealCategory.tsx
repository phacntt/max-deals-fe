import { QueryGetDeal } from 'apis/deal.api';
import DealByCategorySection from 'components/DealByCategorySection/DealByCategorySection';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import { dealService } from 'services';
import { Category } from 'types/Category';
import { Deal } from 'types/Deal';

interface Props {
  mainCategories: Category[];
  parentCategories: Category[];
}

type DataShowDealByCategory = {
  mainCategory?: Category;
  parentCategories?: Category[];
  deals?: Deal[];
};

const ListDealCategory: FC<Props> = ({ mainCategories, parentCategories }) => {
  const [deals, setDeals] = useState<Deal[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      const queryParam: QueryGetDeal = {
        id: 'DESC',
        expiryDate: moment().toISOString(),
      };
      const listDealsValid = await dealService.getAllDeal(queryParam);
      setDeals(listDealsValid);
    };
    fetchData();
  }, []);

  const categoriesFilter = [];

  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < mainCategories.length; i++) {
    const categoryFilter: DataShowDealByCategory = {
      mainCategory: mainCategories[i],
    };
    // eslint-disable-next-line no-plusplus
    for (let j = 0; j < parentCategories.length; j++) {
      if (parentCategories[j].parentCategory.id === mainCategories[i].id) {
        categoryFilter.parentCategories = parentCategories.filter(
          category => category.parentCategory.id === mainCategories[i].id,
        );

        categoryFilter.deals = deals.filter(
          deal => deal.category.parentId === mainCategories[i].id,
        );
      }
    }
    categoriesFilter.push(categoryFilter);
  }

  return (
    <>
      {console.log('list1', deals)}
      {categoriesFilter.map(section => (
        <DealByCategorySection
          categories={section.mainCategory as Category}
          parentCategories={section.parentCategories as Category[]}
          deals={section.deals as Deal[]}
        />
      ))}
    </>
  );
};

export default ListDealCategory;
