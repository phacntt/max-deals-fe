import React, { FC, useEffect, useState } from 'react';
import { Row, Col } from 'antd';

import DealCard from 'components/DealCard/DealCard';
import ListByCategoryHeader from 'components/DealByCategorySection/ListByCategoryHeader';
import { QueryGetDeal } from 'apis/deal.api';
import { dealService } from 'services';
import { Category } from 'types/Category';
import { Deal } from 'types/Deal';
import { Link } from 'react-router-dom';
import moment from 'moment';

interface Props {
  mainCategory: Category[];
}

export type DataShowDeal = {
  categories: Category[];
  deals: Deal[];
};

const ListDeals: FC<Props> = ({ mainCategory }) => {
  const [deals, setDeals] = useState<Deal[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      const queryParams: QueryGetDeal = {
        id: 'DESC',
        limit: 3,
        expiryDate: moment().toISOString(),
      };
      const getDeals = await dealService.getAllDeal(queryParams);
      setDeals(getDeals);
    };
    fetchData();
  }, []);

  return (
    <>
      <ListByCategoryHeader title="NEWS" categories={mainCategory} />
      <Row justify="space-between">
        {deals.map(item => (
          <Col span={7}>
            <DealCard deal={item} />
          </Col>
        ))}
      </Row>
      <Row>
        <Col span={24}>
          <Link
            style={{
              width: '100%',
              padding: '10px 0',
              display: 'flex',
              justifyContent: 'center',
              color: 'rgba(231, 57, 72, 0.8)',
              background: '#fff',
            }}
            to="/deals"
          >
            See more...
          </Link>
        </Col>
      </Row>
    </>
  );
};

export default ListDeals;
