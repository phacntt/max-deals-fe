import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const HeaderListDealContain = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  flex-direction: row;
  justify-content: space-between;
  margin: 20px 0;
`;

export const HeaderListDealLeft = styled.div`
  color: black;
  font-size: 20px;
  font-weight: bold;
  position: relative;
  padding: 5px 0;
  &::after {
    display: block;
    position: absolute;
    content: '';
    height: 2px;
    max-width: 120px;
    min-width: 40px;
    width: 70%;
    bottom: 0;
    left: 0;
    background-color: #e73948;
  }
`;

export const HeaderListDealRight = styled.ul`
  display: flex;
  list-style: none;
  margin: 0;
`;

export const CategoryItem = styled(Link)`
  color: red;
  display: flex;
  margin: 0 20px;
  padding: 0 20px;
  &:hover {
    color: red;
    text-decoration: underline;
    font-weight: 700;
  }
`;
