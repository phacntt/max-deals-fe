import { Col, Row } from 'antd';
import React, { FC } from 'react';
import { Link } from 'react-router-dom';
import { Category } from 'types/Category';
import { LinkSeeMore } from './DealByCategorySection.style';

interface Props {
  category: Category;
}

const ListByCategoryFooter: FC<Props> = ({ category }) => {
  return (
    <Row>
      <Col span={24}>
        <LinkSeeMore to={`/deals/category/${category.slug}`}>
          See more...
        </LinkSeeMore>
      </Col>
    </Row>
  );
};

export default ListByCategoryFooter;
