import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const LinkSeeMore = styled(Link)`
  width: 100%;
  padding: 10px 0;
  display: flex;
  justify-content: center;
  color: rgba(231, 57, 72, 0.8);
  background: #fff;
  font-size: 16px;
  font-weight: 600;
  &:hover {
    text-decoration: underline;
    color: rgba(231, 57, 72, 0.8);
  }
`;
