import { render } from '@testing-library/react';
import { Col, Row } from 'antd';
import DealCard from 'components/DealCard/DealCard';
import { appendFile } from 'fs';
import React, { FC, useEffect, useState } from 'react';

import { Deal } from 'types/Deal';

interface Props {
  deals: Deal[];
}

const ListByCategoryBody: FC<Props> = ({ deals = [] }) => {
  return (
    <Row gutter={16}>
      <Col span={12}>{deals[0] && <DealCard deal={deals[0]} />} </Col>
      <Col span={12}>
        <Row gutter={16}>
          {deals.length === 1 ||
            deals.slice(1, deals.length).map(item => (
              <Col span={12}>
                <DealCard deal={item} />
              </Col>
            ))}
        </Row>
      </Col>
    </Row>
  );
};

export default ListByCategoryBody;
