import { render } from '@testing-library/react';
import { Col, Row } from 'antd';
import {
  CategoryItem,
  HeaderListDealContain,
  HeaderListDealLeft,
  HeaderListDealRight,
} from 'components/ListDeals/ListDeals.style';
import React, { FC, useEffect, useState } from 'react';

import { Link } from 'react-router-dom';
import { Category } from 'types/Category';

interface Props {
  categories: Category[];
  title: string;
}

const ListByCategoryHeader: FC<Props> = ({ categories = [], title }) => {
  return (
    <Row>
      {console.log('show', categories)}
      <Col span={24}>
        <HeaderListDealContain>
          <HeaderListDealLeft>{title} DEALS</HeaderListDealLeft>
          <HeaderListDealRight>
            {categories.map(cate => (
              <CategoryItem to={`deals/category/${cate.slug}`}>
                {cate.name}
              </CategoryItem>
            ))}
          </HeaderListDealRight>
        </HeaderListDealContain>
      </Col>
    </Row>
  );
};

export default ListByCategoryHeader;
