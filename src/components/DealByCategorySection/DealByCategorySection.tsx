import Container from 'components/Container/Container';
import React, { FC, useState } from 'react';
import { Category } from 'types/Category';
import { Deal } from 'types/Deal';
import ListByCategoryBody from './ListByCategoryBody';
import ListByCategoryFooter from './ListByCategoryFooter';
import ListByCategoryHeader from './ListByCategoryHeader';

interface Props {
  categories: Category;
  parentCategories: Category[];
  deals: Deal[];
}

export interface Section {
  categories: Category;
  parentCategories: Category[];
  deals: Deal[];
}

const DealByCategorySection: FC<Props> = ({
  categories,
  parentCategories = [],
  deals = [],
}) => {
  return (
    <Container style={{ padding: 0 }}>
      <ListByCategoryHeader
        categories={parentCategories}
        title={categories.name.toLocaleUpperCase()}
      />
      <ListByCategoryBody deals={deals.slice(0, 5)} />
      <ListByCategoryFooter category={categories} />
    </Container>
  );
};

export default DealByCategorySection;
