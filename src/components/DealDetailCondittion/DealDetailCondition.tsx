import React, { FC } from 'react';
import { Deal } from 'types/Deal';

interface Props {
  deal: Deal;
}

const DealDetailCondition: FC<Props> = ({ deal }) => {
  return (
    <div
      style={{
        maxHeight: 400,
        overflow: 'auto',
      }}
      dangerouslySetInnerHTML={{ __html: deal.description }}
    />
  );
};
export default DealDetailCondition;
