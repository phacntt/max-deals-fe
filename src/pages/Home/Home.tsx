import { Button } from 'antd';
import { QueryGetCategory } from 'apis/category.api';
import { QueryGetDeal } from 'apis/deal.api';
import Banner from 'components/Banner/Banner';
import ListDealCategory from 'components/ListDealCategory/ListDealCategory';
import ListDeals from 'components/ListDeals/ListDeals';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from 'redux/hooks';
import { logoutAsync, selectAuth } from 'redux/slices/auth.slice';
import { selectCurrentUser } from 'redux/slices/current-user.slice';
import { categoryService, dealService } from 'services';
import { Category } from 'types/Category';
import { Deal } from 'types/Deal';

const HomePage: FC = () => {
  const dispath = useAppDispatch();
  const auth = useAppSelector(selectAuth);
  const currentUser = useAppSelector(selectCurrentUser);
  const [parentCategories, setParentCategories] = useState<Category[]>([]);
  const [mainCategories, setMainCategories] = useState<Category[]>([]);
  const [categories, setCategories] = useState<Category[]>([]);
  const [deals, setDeals] = useState<Deal[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      const query: QueryGetCategory = {
        sort: 'ASC',
      };

      const getCategory = await categoryService.getAllCategory(query);
      setCategories(getCategory);

      const mainCategory = getCategory.filter(
        category => category.parentCategory === null,
      );
      setMainCategories(mainCategory);

      const parentCategory = getCategory.filter(
        category => category.parentCategory !== null,
      );

      setParentCategories(parentCategory);

      const querys: QueryGetDeal = {
        id: 'DESC',
        expiryDate: moment().toISOString(),
      };
      const listDealsValid = await dealService.getAllDeal(querys);
      setDeals(listDealsValid);
    };
    fetchData();
  }, []);

  return (
    <>
      {console.log('bbv', deals)}
      {console.log('categoryPar2', parentCategories)}

      <Banner deals={deals} />
      <ListDeals mainCategory={mainCategories} />
      <ListDealCategory
        mainCategories={mainCategories}
        parentCategories={parentCategories}
      />
      <br />
    </>
  );
};

export default HomePage;
