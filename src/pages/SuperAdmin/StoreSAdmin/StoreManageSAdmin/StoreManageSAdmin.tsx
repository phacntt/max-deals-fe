import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import { Button, notification, Table } from 'antd';
import { getAPIErrorMessage, APIError } from 'apis/api';
import React, { FC, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useAppSelector } from 'redux/hooks';
import { selectAuth } from 'redux/slices/auth.slice';
import { selectCurrentUser } from 'redux/slices/current-user.slice';
import { provinceService, storeService } from 'services';
import { Province } from 'types/Province';
import { Store } from 'types/Store';

const StoreManageSAdmin: FC = () => {
  const navigate = useNavigate();
  const [inProgress, setInProgress] = useState(false);
  const currentUser = useAppSelector(selectCurrentUser);
  const [provinces, setProvince] = useState<Province[]>([]);

  const [stores, setStores] = useState<Store[]>([]);
  useEffect(() => {
    const fetchDataStore = async () => {
      const listStores = await storeService.getAllStore();
      setStores(listStores);
      const listProvinces = await provinceService.getAllProvince();
      setProvince(listProvinces);
    };
    fetchDataStore();
  }, []);

  const setNotification = (type: boolean, error?: unknown) => {
    if (type === true) {
      notification.open({
        message: 'Delete Store Success',

        icon: <CheckCircleOutlined style={{ color: '#10e926' }} />,
      });
      setTimeout(() => notification.destroy(), 5000);
    } else {
      notification.open({
        message: 'Delete Store Failure',
        description: `${getAPIErrorMessage(error as APIError)}`,
        icon: <CloseCircleOutlined style={{ color: '#fc3131' }} />,
      });
      setTimeout(() => notification.destroy(), 5000);
    }
  };

  const hanldeDeleteStore = async (idStore: number) => {
    try {
      await storeService.deleteStore(idStore);
      const getStores = await storeService.getAllStore();
      setStores(getStores);
      setNotification(true);
      return navigate(`/superadmin/storemanage`);
    } catch (error) {
      setNotification(false, error);
    }
    return setInProgress(false);
  };

  const columns = [
    {
      title: 'Id',
      dataIndex: 'id',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      render: (text: string, record: Store) => (
        <a href={`/superadmin/storemanage/${record.slug}`}>{text}</a>
      ),
    },
    {
      title: 'Address',
      dataIndex: 'fullAddress',
      with: '30%',
      render: (record: string) => <p>{record}</p>,
    },

    {
      title: 'Action',
      render: (record: Store) => (
        <Button onClick={() => hanldeDeleteStore(record.id)}>Delete</Button>
      ),
    },
  ];

  return (
    <>
      {console.log(stores)}
      <h1
        style={{
          borderBottom: '1px solid black',
          paddingBottom: 15,
          marginTop: 20,
          fontSize: 24,
          width: '50%',
          fontWeight: 'bold',
        }}
      >
        LIST STORES
      </h1>
      <Table columns={columns} dataSource={stores} />
    </>
  );
};

export default StoreManageSAdmin;
