import { Form, Row, Col, Select, Input, Button, Alert } from 'antd';
import { useForm } from 'antd/lib/form/Form';
import TextArea from 'antd/lib/input/TextArea';
import { getAPIErrorMessage, APIError } from 'apis/api';
import { AddStoreRequestBody } from 'apis/store.api';
import { ButtonSearchMap } from 'components/ButtonSearchMap/ButtonSearchMap';
import FormUpload from 'components/FormUpload/FormUpload';
import Maps from 'components/Maps/Maps';
import { configEditor } from 'helpers/editorConfig';
import { setSlugify } from 'helpers/slugify';
import JoditEditor from 'jodit-react';
import React, { FC, useEffect, useMemo, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAppSelector } from 'redux/hooks';
import { selectAuth } from 'redux/slices/auth.slice';
import { selectCurrentUser } from 'redux/slices/current-user.slice';
import { provinceService, storeService } from 'services';

import { File } from 'types/File';
import { Province } from 'types/Province';
import { Ward } from 'types/Ward';

export type StoreFormValues = {
  name: string;
  slug: string;
  description: string;
  phone: string;
  province: string;
  district: string;
  ward: string;
  street: string;
  fullAdrress: string;
  location: string;
};

export type StoreFormLocal = {
  province: string;
  district: string;
  ward: string;
  street: string;
  fullAdrress: string;
};

interface GeoLocation {
  lat: number;
  lng: number;
}

const StoreAddSAdmin: FC = () => {
  const [storeForm] = useForm();
  const { Option } = Select;
  const navigate = useNavigate();

  const [errorMessage, setErrorMessage] = useState('');
  const [inProgress, setInProgress] = useState(false);
  const [province, setProvince] = useState('');
  const [district, setDistrict] = useState('');
  const [address, setAddress] = useState('');
  const [street, setStreet] = useState('');

  const auth = useAppSelector(selectAuth);
  const currentUser = useAppSelector(selectCurrentUser);

  const [location, setLocation] = useState<GeoLocation>();
  const [ward, setWard] = useState('');

  const [provinceData, setProvinceData] = useState<Province[]>([]);
  const [listDistrictWard, setListDistrictWard] = useState<Province>();
  const [wards, setWards] = useState<Ward[]>([]);
  const [images, setImages] = useState<File[]>([]);
  const [isMultiple, setIsMultiple] = useState<boolean>();
  const [placeholder, setPlaceholder] = useState<string>('');

  const editor = useRef(null);
  const [content, setContent] = useState('');

  const config = useMemo<any>(configEditor, [placeholder]);

  useEffect(() => {
    const fetchDataAddress = async () => {
      const listProvinces = await provinceService.getAllProvince();
      setProvinceData(listProvinces);
      setIsMultiple(false);
    };
    fetchDataAddress();
  }, []);

  const onChangeProvince = async (e: React.ChangeEvent<HTMLSelectElement>) => {
    const listAdministrativeUnits = await provinceService.getProvinceById(
      e.toString(),
    );
    setProvince(listAdministrativeUnits.fullName);
    setListDistrictWard(listAdministrativeUnits);
  };

  const onChangeDistrict = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const districtCode = e.toString();
    const listDistrict = listDistrictWard?.districts?.find(
      districtEle => districtEle.code === districtCode,
    );
    const listWard = listDistrict?.wards?.filter(item => item);
    setDistrict(listDistrict?.fullName as string);
    setWards(listWard as Ward[]);
  };

  const onChangeWard = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const wardCode = e.toString();
    const getNameWard = wards.find(wardItem => wardItem.code === wardCode);
    setWard(getNameWard?.fullName as string);
  };

  const onChangeStreet = (e: React.ChangeEvent<HTMLInputElement>) => {
    setStreet(e.target.value);
  };

  const handleAddStore = async (formValue: StoreFormValues) => {
    try {
      setInProgress(true);
      setErrorMessage('');

      const fileIds = images.map(image => image.id);

      const data: AddStoreRequestBody = {
        name: formValue.name,
        slug: formValue.slug,
        description: formValue.description,
        phone: formValue.phone,
        imageIds: fileIds,
        province: formValue.province,
        district: formValue.district,
        ward: formValue.ward,
        street: formValue.street,
        fullAddress: `${street}, ${ward}, ${district}, ${province}`,
        lat: location?.lat as number,
        lng: location?.lng as number,
      };
      setAddress(data.fullAddress as string);
      console.log('mmm', data);

      const resp = await storeService.addStore(data);
      if (resp) {
        return navigate(`/superadmin/storemanage`);
      }

      setErrorMessage('Create Deal is Failure');
    } catch (error: unknown) {
      setErrorMessage(getAPIErrorMessage(error as APIError));
    }
    return setInProgress(false);
  };

  const handleUploadFile = (newImages: File[]) => {
    setImages(newImages);
  };

  const getNameAndSetSlug = (e: React.ChangeEvent<HTMLInputElement>) => {
    storeForm.setFieldsValue({
      slug: setSlugify(e.target.value),
    });
  };

  const handleGetAddress = () => {
    const fullAdrress = `${street}, ${ward}, ${district}, ${province}`;
    setAddress(fullAdrress);
  };

  const handleLocation = (locations: GeoLocation) => {
    setLocation(locations);
  };

  return (
    <>
      <h1
        style={{
          borderBottom: '1px solid black',
          paddingBottom: 15,
          marginTop: 20,
          fontSize: 24,
          width: '50%',
          fontWeight: 'bold',
        }}
      >
        CREATE NEW STORE
      </h1>
      {errorMessage && <Alert message={errorMessage} type="error" />}

      <Form
        layout="vertical"
        name="addStore"
        form={storeForm}
        onFinish={handleAddStore}
        encType="multipart/form-data"
      >
        <Row gutter={8}>
          <Col span={4}>
            <Form.Item
              label="Name Store"
              name="name"
              rules={[{ required: true }]}
            >
              <Input
                onChange={getNameAndSetSlug}
                name="name"
                placeholder="Enter Name Store"
              />
            </Form.Item>
          </Col>

          <Col span={4}>
            <Form.Item label="Slug" name="slug" rules={[{ required: true }]}>
              <Input name="slug" placeholder="Enter Name Slug" />
            </Form.Item>
          </Col>

          <Col span={4}>
            <Form.Item
              label="Phone number"
              name="phone"
              rules={[{ required: true }]}
            >
              <Input name="phone" placeholder="Enter phone number" />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Form.Item
              label="Description"
              name="description"
              rules={[{ required: true }]}
            >
              <JoditEditor
                ref={editor}
                value={content}
                config={config}
                onBlur={newContent => setContent(newContent)} // preferred to use only this option to update the content for performance reasons
                onChange={newContent => ({})}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={8}>
            <Form.Item label="Image" name="img" rules={[{ required: true }]}>
              <FormUpload
                isMultiple={isMultiple as boolean}
                value={images}
                onChange={handleUploadFile}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={8}>
          <Col span={4}>
            <Form.Item
              label="Provinces"
              name="province"
              rules={[{ required: true }]}
            >
              <Select
                placeholder="--All Provinces--"
                className="select-after"
                onChange={onChangeProvince}
              >
                {provinceData.map(provinceItem => (
                  <Option value={provinceItem.code}>
                    {provinceItem.fullName}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={4}>
            <Form.Item
              label="Districts"
              name="district"
              rules={[{ required: true }]}
            >
              <Select
                placeholder="--All Districts--"
                className="select-after"
                onChange={onChangeDistrict}
              >
                {listDistrictWard?.districts?.map(unit => (
                  <Option value={unit.code}>{unit.fullName}</Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={4}>
            <Form.Item label="Ward" name="ward" rules={[{ required: true }]}>
              <Select
                placeholder="--All Wards--"
                className="select-after"
                onChange={onChangeWard}
              >
                {wards.map(wardItem => (
                  <Option value={wardItem.code}>{wardItem.fullName}</Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={4}>
            <Form.Item
              label="Street"
              name="street"
              rules={[{ required: true }]}
            >
              <Input
                onChange={onChangeStreet}
                name="street"
                placeholder="Enter Name Street"
              />
            </Form.Item>
          </Col>
          <Col span={4}>
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                marginTop: 30,
              }}
            >
              <ButtonSearchMap onClick={handleGetAddress}>
                Search
              </ButtonSearchMap>
            </div>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Maps fullAddress={address} onChange={handleLocation} />
          </Col>
        </Row>

        <Row style={{ justifyContent: 'center' }}>
          <Form.Item>
            <Button
              style={{ marginTop: 30 }}
              block
              type="primary"
              htmlType="submit"
              loading={inProgress}
            >
              Create
            </Button>
          </Form.Item>
        </Row>
      </Form>
    </>
  );
};

export default StoreAddSAdmin;
