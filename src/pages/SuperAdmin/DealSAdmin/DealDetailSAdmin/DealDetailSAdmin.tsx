import {
  Row,
  Alert,
  Button,
  Col,
  DatePicker,
  Form,
  Input,
  InputNumber,
  Select,
} from 'antd';
import { useForm } from 'antd/lib/form/Form';
import TextArea from 'antd/lib/input/TextArea';
import moment, { Moment } from 'moment';
import FormUpload from 'components/FormUpload/FormUpload';
import React, { FC, useEffect, useMemo, useRef, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import {
  categoryService,
  dealService,
  fileService,
  storeService,
} from 'services';
import { File } from 'types/File';
import { Category } from 'types/Category';
import { Deal } from 'types/Deal';
import { Store } from 'types/Store';
import { getAPIErrorMessage, APIError } from 'apis/api';
import { UpdateDealRequestBody } from 'apis/deal.api';
import slugify from 'slugify';
import { setSlugify } from 'helpers/slugify';
import { useAppSelector } from 'redux/hooks';
import { selectCurrentUser } from 'redux/slices/current-user.slice';
import { configEditor } from 'helpers/editorConfig';
import JoditEditor from 'jodit-react';
import { getIdFromSlug } from 'helpers/getIdFromSlug';
import { DealFormValues } from '../DealAddSAdmin/DealAddSAdmin';

const DealDetailSAdmin: FC = () => {
  const { Option } = Select;
  const navigate = useNavigate();
  const [updateForm] = useForm();
  const [placeholder, setPlaceholder] = useState<string>('');

  const [errorMessage, setErrorMessage] = useState('');

  const [inProgress, setInProgress] = useState(false);

  const [deal, setDeal] = useState<Deal>();
  const [categories, setCategories] = useState<Category[]>([]);
  const [stores, setStores] = useState<Store[]>([]);
  const [images, setImages] = useState<File[]>([]);
  const [isMultiple, setIsMultiple] = useState<boolean>();

  const { dealId } = useParams();
  const currentUser = useAppSelector(selectCurrentUser);
  const idDeal = getIdFromSlug(dealId);

  useEffect(() => {
    const fetchData = async () => {
      const typeEntity = 'DealEntity';
      const getDealById = await dealService.getDealById(idDeal as number);
      const getImageFile = await fileService.getFileById(
        idDeal as number,
        typeEntity,
      );
      const getListCategories = await categoryService.getAllCategory({
        parentOnly: false,
      });
      const getListStores = await storeService.getAllStore();
      getDealById.images = getImageFile;
      setDeal(getDealById);
      setImages(getImageFile);
      setCategories(getListCategories);
      setStores(getListStores);
      setIsMultiple(true);
      updateForm.setFieldsValue({
        name: getDealById.name,
        slug: getDealById.slug,
        expiryDate: moment(getDealById.expiryDate),
        quantity: getDealById.quantity,
        description: getDealById.description,
        discount: getDealById.discount,
        categoryId: getDealById.category.id,
        storeId: getDealById.store.id,
      });
    };
    fetchData();

    return () => {
      console.log('Unmount');
    };
  }, []);

  const handleUploadFile = (newImages: File[]) => {
    setImages(newImages);
  };

  const handleUpdateDeal = async (formValue: DealFormValues) => {
    try {
      if (images.length === 0) {
        setErrorMessage('Deal must be at least 1 image');
        return navigate(`/superadmin/dealmanage/${deal?.slug}`);
      }
      setInProgress(true);
      setErrorMessage('');

      const data: UpdateDealRequestBody = {
        name: formValue.name,
        slug: formValue.slug,
        expiryDate: formValue.expiryDate.toISOString(),
        quantity: formValue.quantity,
        description: formValue.description,
        discount: formValue.discount,
        categoryId: parseInt(formValue.categoryId, 10),
        storeId: parseInt(formValue.storeId, 10),
      };

      const currentIds = images.map(image => image.id);

      const resp = await dealService.updateDeal(
        idDeal as number,
        data,
        currentIds,
      );

      if (resp) {
        return navigate(`/superadmin/dealmanage`);
      }

      setErrorMessage('Update Deal is Failure');
    } catch (error: unknown) {
      setErrorMessage(getAPIErrorMessage(error as APIError));
    }

    return setInProgress(false);
  };

  const disabledDate = (current: Moment) => {
    return current && current < moment().endOf('day');
  };

  const getNameAndSetSlug = (e: React.ChangeEvent<HTMLInputElement>) => {
    updateForm.setFieldsValue({
      slug: setSlugify(e.target.value),
    });
  };

  const editor = useRef(null);
  const [content, setContent] = useState('');

  const config = useMemo<any>(configEditor, [placeholder]);

  return (
    <>
      {console.log(images)}
      <h1
        style={{
          borderBottom: '1px solid black',
          paddingBottom: 15,
          marginTop: 20,
          fontSize: 24,
          whiteSpace: 'nowrap',
          overflow: 'hidden',
          textOverflow: 'ellipsis',
          width: '50%',
          fontWeight: 'bold',
        }}
      >
        DEAL DETAIL {deal?.name.toUpperCase()}
      </h1>
      {errorMessage && <Alert message={errorMessage} type="error" />}

      <Form
        layout="vertical"
        name="addDeal"
        form={updateForm}
        onFinish={handleUpdateDeal}
        encType="multipart/form-data"
      >
        <Row gutter={16}>
          <Col span={8}>
            <Form.Item
              label="Name Deal"
              name="name"
              rules={[{ required: true }]}
            >
              <Input
                onChange={getNameAndSetSlug}
                placeholder="Enter Name Deal"
                name="name"
              />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              label="Slug Deal"
              name="slug"
              rules={[{ required: true }]}
            >
              <Input placeholder="Slug Deal" name="slug" />
            </Form.Item>
          </Col>
          <Col span={4}>
            <Form.Item
              label="Expiry Date"
              name="expiryDate"
              rules={[{ required: true }]}
            >
              <DatePicker
                disabledDate={disabledDate}
                id="expiryDate"
                style={{ width: '100%' }}
              />
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item label="Quantity" name="quantity">
              <InputNumber defaultValue={1} min={1} name="quantity" />
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item label="Discount(%)" name="discount">
              <InputNumber defaultValue={1} min={1} name="discount" />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              label="Category"
              name="categoryId"
              rules={[{ required: true }]}
            >
              <Select className="select-after">
                {categories.map(category => (
                  <Option value={category.id}>{category.name}</Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              label="Store"
              name="storeId"
              rules={[{ required: true }]}
            >
              <Select className="select-after">
                {stores.map(store => (
                  <Option name="store" value={store.id}>
                    {store.name}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>

          <Col span={24}>
            <Form.Item label="Description" name="description">
              {/* <TextArea rows={8} /> */}
              <JoditEditor
                ref={editor}
                value={content}
                config={config}
                onBlur={newContent => setContent(newContent)} // preferred to use only this option to update the content for performance reasons
                onChange={newContent => ({})}
              />
            </Form.Item>
          </Col>
          <Col span={8}>
            <FormUpload
              isMultiple={isMultiple as boolean}
              value={images}
              onChange={handleUploadFile}
            />
          </Col>
        </Row>
        <Row style={{ justifyContent: 'center' }}>
          <Form.Item>
            <Button block type="primary" htmlType="submit" loading={inProgress}>
              Update
            </Button>
          </Form.Item>
        </Row>
      </Form>
    </>
  );
};

export default DealDetailSAdmin;
