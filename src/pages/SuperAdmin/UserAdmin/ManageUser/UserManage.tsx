import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import { Button, Col, notification, Row, Select, Table, Tag } from 'antd';
import { accountAPI } from 'apis';
import { getAPIErrorMessage, APIError } from 'apis/api';
import { QueryGetUser, RoleAccount } from 'apis/user.api';
import React, { FC, useEffect, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { useAppSelector } from 'redux/hooks';
import { selectCurrentUser } from 'redux/slices/current-user.slice';
import { accountService, storeService, userService } from 'services';
import { Store } from 'types/Store';
import { User } from 'types/User';

const UserManage: FC = () => {
  const { Option } = Select;

  const navigate = useNavigate();
  const [inProgress, setInProgress] = useState(false);
  const currentUser = useAppSelector(selectCurrentUser);
  const [users, setUsers] = useState<User[]>([]);
  const [filterValues, setFilterValues] = useState<QueryGetUser>({});
  const [disable, setDisable] = useState<boolean>(false);
  const [stores, setStores] = useState<Store[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      const listUsers = await userService.getUsers();
      const listStores = await storeService.getAllStore();
      setStores(listStores);
      setUsers(listUsers);
    };
    fetchData();
  }, []);

  console.log(users);

  const setNotification = (type: boolean, error?: unknown) => {
    if (type === true) {
      notification.open({
        message: 'Delete User Success',

        icon: <CheckCircleOutlined style={{ color: '#10e926' }} />,
      });
      setTimeout(() => notification.destroy(), 5000);
    } else {
      notification.open({
        message: 'Delete User Failure',
        description: `${getAPIErrorMessage(error as APIError)}`,
        icon: <CloseCircleOutlined style={{ color: '#fc3131' }} />,
      });
      setTimeout(() => notification.destroy(), 5000);
    }
  };

  const hanldeDeleteUser = async (idUser: number) => {
    try {
      await userService.deleteUser(idUser);
      const listUsers = await userService.getUsers();
      setUsers(listUsers);
      setNotification(true);
      return navigate(`/superadmin/usermanage`);
    } catch (error) {
      setNotification(false, error);
    }
    return setInProgress(false);
  };
  console.log(users);
  const columns = [
    {
      title: 'Id',
      dataIndex: 'id',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      render: (text: string, record: User) => (
        <Link to={`/superadmin/usermanage/${record.id}`}>{text}</Link>
      ),
    },
    {
      title: 'Email',
      dataIndex: 'email',
      render: (record: string) => <p>{record}</p>,
    },
    {
      title: 'Role',
      dataIndex: 'role',
      render: (record: string) => <p>{record}</p>,
    },
    {
      title: 'Store',
      dataIndex: 'store',
      render: (record: Store) => <p>{record?.name}</p>,
    },
    {
      title: 'Action',
      render: (record: User) => (
        <Button onClick={() => hanldeDeleteUser(record.id)}>Delete</Button>
      ),
    },
  ];

  const handleSelectRole = (newRoleId: string) => {
    setFilterValues({
      ...filterValues,
      role: newRoleId as RoleAccount,
    });
  };

  const handleSelectStore = (newStoreId: string) => {
    setFilterValues({ ...filterValues, storeId: parseInt(newStoreId, 10) });
  };

  const fillterDeal = async () => {
    const queryParams: QueryGetUser = {};
    // Check not: null, undefined, 0, '', false
    if (filterValues.role) {
      queryParams.role = filterValues.role;
    }

    if (filterValues.storeId) {
      queryParams.storeId = filterValues.storeId;
    }

    const usersFilter = await userService.getUsers(queryParams);
    setUsers(usersFilter);
    setDisable(false);
  };

  const resetFillter = async () => {
    setDisable(true);
    const callUsersApi = await userService.getUsers();
    setUsers(callUsersApi);
    setFilterValues({});
  };

  return (
    <>
      <h1
        style={{
          borderBottom: '1px solid black',
          paddingBottom: 15,
          marginTop: 20,
          fontSize: 24,
          width: '50%',
          fontWeight: 'bold',
        }}
      >
        LIST USER
      </h1>
      <Row>
        <Col span={3}>
          <Select
            style={{ width: 150 }}
            id="dropdown-category"
            className="select-after"
            placeholder="All Role"
            onChange={handleSelectRole}
            value={filterValues.role ? `${filterValues.role}` : undefined}
          >
            <Option className="option" value="super_admin">
              Super Admin
            </Option>
            <Option className="option" value="admin">
              Admin
            </Option>
            <Option className="option" value="user">
              User
            </Option>
          </Select>
        </Col>
        <Col span={3}>
          <Select
            style={{ width: 150 }}
            id="dropdown-store"
            className="select-after"
            placeholder="All Stores"
            onChange={handleSelectStore}
            value={filterValues.storeId ? `${filterValues.storeId}` : undefined}
          >
            {stores.map(store => (
              <Option value={`${store.id}`}>{store.name}</Option>
            ))}
          </Select>
        </Col>
        <Col span={2}>
          <Button style={{ width: 100 }} onClick={fillterDeal}>
            Fillter
          </Button>
        </Col>
        <Col span={3}>
          <Button onClick={resetFillter} disabled={disable}>
            Reset Fillter
          </Button>
        </Col>
      </Row>
      <Table columns={columns} dataSource={users} />;
    </>
  );
};

export default UserManage;
