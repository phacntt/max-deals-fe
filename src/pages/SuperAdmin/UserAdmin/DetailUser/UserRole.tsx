import { Radio, RadioChangeEvent } from 'antd';
import React, { FC } from 'react';

interface Props {
  onChange: (value: string) => void;
  value: string;
}

const UserRole: FC<Props> = ({ onChange, value }) => {
  console.log(value);

  const handleOnChange = (e: RadioChangeEvent) => {
    onChange(e.target.value);
  };

  return (
    <div>
      <Radio.Group
        style={{ textAlign: 'left', padding: '0 25px' }}
        onChange={handleOnChange}
        value={value}
      >
        <Radio value="Super Admin">Super Admin</Radio>
        <Radio value="Admin">Admin</Radio>
        <Radio value="User">User</Radio>
      </Radio.Group>
    </div>
  );
};

export default UserRole;
