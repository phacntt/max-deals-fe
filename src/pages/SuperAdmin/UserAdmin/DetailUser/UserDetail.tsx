import {
  EyeTwoTone,
  EyeInvisibleOutlined,
  ArrowLeftOutlined,
} from '@ant-design/icons';
import {
  Form,
  Input,
  Row,
  Col,
  Button,
  Modal,
  Result,
  Radio,
  RadioChangeEvent,
  Select,
} from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { getAPIErrorMessage, APIError } from 'apis/api';
import { UpdateUserRequestBody } from 'apis/user.api';
import React, { FC, useEffect, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { useAppDispatch } from 'redux/hooks';
import { accountService, storeService, userService } from 'services';
import { Store } from 'types/Store';
import { User } from 'types/User';
import UserRole from './UserRole';

type RegisterFormValues = {
  username: string;
  password: string;
  email: string;
  name: string;
  phone: string;
  storeId?: number;
};

const UserDetail: FC = () => {
  const { Option } = Select;

  const dispath = useAppDispatch();
  const navigate = useNavigate();
  const [updateForm] = useForm();
  const [user, setUser] = useState<User>();
  const { userId } = useParams();

  const [errorMessage, setErrorMessage] = useState('');
  const [inProgress, setInProgress] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [result, setResult] = useState(false);
  const [value, setValue] = useState<string>('');
  const [stores, setStores] = useState<Store[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      const infoUser = await userService.getUserById(Number(userId));
      const listStores = await storeService.getAllStore();
      setStores(listStores);
      setUser(infoUser);
      setValue(infoUser.role);
      updateForm.setFieldsValue({
        username: infoUser.username,
        email: infoUser.email,
        name: infoUser.name,
        phone: infoUser.phone,
        storeId: infoUser.storeId,
      });
      console.log(infoUser);
    };
    fetchData();
  }, []);

  const handleUpdate = async (formValue: RegisterFormValues) => {
    try {
      const data: UpdateUserRequestBody = {
        username: formValue.username,
        email: formValue.email,
        name: formValue.name,
        phone: formValue.phone,
        password: formValue.password,
        role: value,
      };

      if (data.role === 'admin') {
        data.storeId = formValue.storeId;
      } else {
        data.storeId = null;
      }

      const resp = await userService.updateUser(Number(userId), data);

      if (resp) {
        return navigate(`/superadmin/usermanage`);
      }
      console.log(data);

      setErrorMessage('Update User is Failure');
    } catch (error) {
      setErrorMessage(getAPIErrorMessage(error as APIError));
    }
    return setInProgress(false);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  // const onChange = (e: string) => {
  //   console.log(e);
  //   setValue(e);
  // };

  const onChange = (e: RadioChangeEvent) => {
    // onChange(e.target.value);
    setValue(e.target.value);
  };

  console.log('result ', value.localeCompare(user?.role as string));
  console.log('result1 ', value.length);
  console.log('result2 ', user?.role.length);

  return (
    <>
      <h1
        style={{
          borderBottom: '1px solid black',
          paddingBottom: 15,
          marginTop: 20,
          fontSize: 24,
          width: '50%',
          fontWeight: 'bold',
        }}
      >
        UPDATE USER {user?.name.toUpperCase()}
      </h1>

      <div style={{ textAlign: 'center' }}>
        <Form
          form={updateForm}
          name="login"
          layout="vertical"
          onFinish={handleUpdate}
          style={{
            width: '40%',
            display: 'inline-block',
            padding: '20px',
            background: '#fff',
          }}
        >
          <Form.Item
            name="username"
            rules={[{ required: true }]}
            label="Username"
          >
            <Input />
          </Form.Item>
          <Form.Item name="email" rules={[{ required: true }]} label="Email">
            <Input type="email" />
          </Form.Item>
          <Form.Item>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  name="name"
                  rules={[{ required: true }]}
                  label="Name"
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="phone"
                  rules={[{ required: true }]}
                  label="Phone Number"
                >
                  <Input />
                </Form.Item>
              </Col>
            </Row>
          </Form.Item>
          <Form.Item name="role" label="Role">
            {console.log(value)}

            <Radio.Group
              style={{ textAlign: 'left', padding: '0 25px' }}
              onChange={onChange}
              value={value}
            >
              <Radio value="super_admin">Super Admin</Radio>
              <Radio value="admin">Admin</Radio>
              <Radio value="user">User</Radio>
            </Radio.Group>
          </Form.Item>
          {value === 'admin' && (
            <Form.Item>
              <Col span={12}>
                <Form.Item
                  label="Store"
                  name="storeId"
                  rules={[{ required: true }]}
                >
                  <Select placeholder="--All Store--" className="select-after">
                    {stores.map(store => (
                      <Option value={store.id}>{store.name}</Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>
            </Form.Item>
          )}
          <Form.Item>
            <Button block type="primary" htmlType="submit" loading={inProgress}>
              Update
            </Button>
          </Form.Item>
          {result ? (
            <Modal
              visible={isModalVisible}
              onOk={handleOk}
              onCancel={handleCancel}
              footer={[
                <Button key="back" onClick={handleCancel}>
                  Confirm
                </Button>,
              ]}
            >
              <Result
                status="success"
                title="Login Success"
                subTitle="Please wait a second to redirect to Login page"
              />
            </Modal>
          ) : (
            <Modal
              visible={isModalVisible}
              onOk={handleOk}
              onCancel={handleCancel}
              footer={[
                <Button key="back" onClick={handleCancel}>
                  Confirm
                </Button>,
              ]}
            >
              <Result
                status="error"
                title="Login Failure"
                subTitle={errorMessage}
              />
            </Modal>
          )}
        </Form>
      </div>
    </>
  );
};

export default UserDetail;
