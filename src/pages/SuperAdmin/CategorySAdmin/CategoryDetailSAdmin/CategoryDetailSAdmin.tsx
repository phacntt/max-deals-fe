import { Select, Form, Row, Col, Input, Button, Alert } from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { getAPIErrorMessage, APIError } from 'apis/api';
import { QueryGetCategory, UpdateCategoryRequestBody } from 'apis/category.api';
import { getIdFromSlug } from 'helpers/getIdFromSlug';
import { setSlugify } from 'helpers/slugify';
import React, { FC, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { categoryService } from 'services';
import { Category } from 'types/Category';

export type CategoryFormValues = {
  name: string;
  slug: string;
  parentId?: number;
};

const CategoryDetailSAdmin: FC = () => {
  const { Option } = Select;
  const navigate = useNavigate();
  const [updateForm] = useForm();
  const [errorMessage, setErrorMessage] = useState('');
  const [inProgress, setInProgress] = useState(false);
  const [status, setStatus] = useState(true);
  const [statusSelect, setStatusSelect] = useState(false);
  const [valueOption, setValueOption] = useState(1);
  const [categoriy, setCategory] = useState<Category>();
  const [categories, setCategories] = useState<Category[]>([]);

  const { categoryId } = useParams();
  const idSCategory = getIdFromSlug(categoryId);

  useEffect(() => {
    const fetchData = async () => {
      const queryParams: QueryGetCategory = {};

      queryParams.parentOnly = true;

      const getCategories = await categoryService.getAllCategory(queryParams);
      const getCategoryById = await categoryService.getCategoryById(
        idSCategory as number,
      );
      setCategories(getCategories);
      setCategory(getCategoryById);
      updateForm.setFieldsValue({
        name: getCategoryById.name,
        slug: getCategoryById.slug,
      });
      if (getCategoryById.parentId != null) {
        updateForm.setFieldsValue({
          parentId: getCategoryById.parentCategory.id,
        });
      } else {
        setStatusSelect(true);
        setStatus(false);
      }
    };
    fetchData();
  }, []);

  const handleUpdateCategory = async (formValue: CategoryFormValues) => {
    try {
      setInProgress(true);
      setErrorMessage('');

      const data: UpdateCategoryRequestBody = {
        name: formValue.name,
        slug: formValue.slug,
        parentId: formValue.parentId,
      };

      console.log(data);

      const resp = await categoryService.updateCategory(
        idSCategory as number,
        data,
      );
      if (resp) {
        return navigate('/superadmin/categorymanage');
      }

      setErrorMessage('Update Category is Failure');
    } catch (error: unknown) {
      setErrorMessage(getAPIErrorMessage(error as APIError));
    }
    console.log('Received values of form: ', formValue);

    return setInProgress(false);
  };

  const getNameAndSetSlug = (e: React.ChangeEvent<HTMLInputElement>) => {
    updateForm.setFieldsValue({
      slug: setSlugify(e.target.value),
    });
  };

  return (
    <>
      <h1
        style={{
          borderBottom: '1px solid black',
          paddingBottom: 15,
          marginTop: 20,
          fontSize: 24,
          width: '50%',
          fontWeight: 'bold',
        }}
      >
        CATEGORY DETAIL {categoriy?.name.toUpperCase()}
      </h1>
      {errorMessage && <Alert message={errorMessage} type="error" />}

      <Form
        layout="vertical"
        name="addDeal"
        form={updateForm}
        onFinish={handleUpdateCategory}
        encType="multipart/form-data"
      >
        <Row gutter={8}>
          <Col span={4}>
            <Form.Item
              label="Category"
              name="parentId"
              rules={[{ required: status }]}
            >
              <Select
                disabled={statusSelect}
                placeholder="--All Category--"
                className="select-after"
              >
                {categories.map(category => (
                  <Option value={category.id}>{category.name}</Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              label="Name Category"
              name="name"
              rules={[{ required: true }]}
            >
              <Input
                name="name"
                onChange={getNameAndSetSlug}
                placeholder="Enter Name Category"
              />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              label="Slug Category"
              name="slug"
              rules={[{ required: true }]}
            >
              <Input name="slug" placeholder="Slug Category" />
            </Form.Item>
          </Col>
        </Row>
        <Row style={{ justifyContent: 'center' }}>
          <Form.Item>
            <Button block type="primary" htmlType="submit" loading={inProgress}>
              Update
            </Button>
          </Form.Item>
        </Row>
      </Form>
    </>
  );
};

export default CategoryDetailSAdmin;
