import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import { Button, notification, Table } from 'antd';
import { getAPIErrorMessage, APIError } from 'apis/api';
import { QueryGetCategory } from 'apis/category.api';
import React, { FC, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { categoryService } from 'services';
import { Category } from 'types/Category';

const CategoryManageSAdmin: FC = () => {
  const [categories, setCategories] = useState<Category[]>([]);

  const navigate = useNavigate();
  const [inProgress, setInProgress] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      const queryParams: QueryGetCategory = {};

      const getListCategories = await categoryService.getAllCategory(
        queryParams,
      );
      setCategories(getListCategories);
    };
    fetchData();
  }, []);

  const setNotification = (type: boolean, error?: unknown) => {
    if (type === true) {
      notification.open({
        message: 'Delete Category Success',

        icon: <CheckCircleOutlined style={{ color: '#10e926' }} />,
      });
      setTimeout(() => notification.destroy(), 5000);
    } else {
      notification.open({
        message: 'Delete Category Failure',
        description: `${getAPIErrorMessage(error as APIError)}`,
        icon: <CloseCircleOutlined style={{ color: '#fc3131' }} />,
      });
      setTimeout(() => notification.destroy(), 5000);
    }
  };

  const hanldeDeleteCategory = async (id: number) => {
    try {
      await categoryService.deleteCategory(id);
      const getListCategories = await categoryService.getAllCategory();
      setCategories(getListCategories);
      setNotification(true);
      return navigate('/superadmin/categorymanage');
    } catch (error) {
      setNotification(false, error);
    }
    return setInProgress(false);
  };

  const columns = [
    {
      title: 'Id',
      dataIndex: 'id',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      render: (text: string, record: Category) => (
        <a href={`/superadmin/categorymanage/${record.slug}`}>{text}</a>
      ),
    },
    {
      title: 'Slug',
      dataIndex: 'slug',
    },
    {
      title: 'Parent Category',
      dataIndex: 'parentCategory',
      render: (record: Category) => (record ? <p>{record.name}</p> : ''),
    },
    {
      title: 'Action',
      render: (record: Category) => (
        <Button onClick={() => hanldeDeleteCategory(record.id)}>Delete</Button>
      ),
    },
  ];

  return (
    <>
      <h1
        style={{
          borderBottom: '1px solid black',
          paddingBottom: 15,
          marginTop: 20,
          fontSize: 24,
          width: '50%',
          fontWeight: 'bold',
        }}
      >
        LIST CATEGORIES
      </h1>

      <Table columns={columns} dataSource={categories} />
    </>
  );
};

export default CategoryManageSAdmin;
