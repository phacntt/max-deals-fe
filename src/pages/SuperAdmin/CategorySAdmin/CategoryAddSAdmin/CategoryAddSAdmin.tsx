import {
  Select,
  Form,
  Row,
  Col,
  Input,
  Button,
  Alert,
  Radio,
  RadioChangeEvent,
} from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { getAPIErrorMessage, APIError } from 'apis/api';
import { AddCategoryRequestBody, QueryGetCategory } from 'apis/category.api';
import { setSlugify } from 'helpers/slugify';
import React, { FC, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { categoryService } from 'services';
import { Category } from 'types/Category';

export type CategoryFormValues = {
  name: string;
  slug: string;
  parentId?: number;
};

const CategoryAddSAdmin: FC = () => {
  const { Option } = Select;
  const navigate = useNavigate();
  const [categoryForm] = useForm();
  const [errorMessage, setErrorMessage] = useState('');
  const [inProgress, setInProgress] = useState(false);
  const [status, setStatus] = useState(true);
  const [statusSelect, setStatusSelect] = useState(false);
  const [valueOption, setValueOption] = useState(1);
  const [categories, setCategories] = useState<Category[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      const queryParams: QueryGetCategory = {};

      queryParams.parentOnly = true;

      const CategoryFilter = await categoryService.getAllCategory(queryParams);
      setCategories(CategoryFilter);
    };
    fetchData();
  }, []);

  const handleAddCategory = async (formValue: CategoryFormValues) => {
    try {
      setInProgress(true);
      setErrorMessage('');

      const data: AddCategoryRequestBody = {
        name: formValue.name,
        slug: formValue.slug,
        parentId: formValue.parentId,
      };

      const resp = await categoryService.createCategory(data);
      if (resp) {
        return navigate('/superadmin/categorymanage');
      }

      setErrorMessage('Create Category is Failure');
    } catch (error: unknown) {
      setErrorMessage(getAPIErrorMessage(error as APIError));
    }
    console.log('Received values of form: ', formValue);

    return setInProgress(false);
  };

  const getNameAndSetSlug = (e: React.ChangeEvent<HTMLInputElement>) => {
    categoryForm.setFieldsValue({
      slug: setSlugify(e.target.value),
    });
  };

  return (
    <>
      <h1
        style={{
          borderBottom: '1px solid black',
          paddingBottom: 15,
          marginTop: 20,
          fontSize: 24,
          width: '50%',
          fontWeight: 'bold',
        }}
      >
        CREATE NEW CATEGORY
      </h1>
      {errorMessage && <Alert message={errorMessage} type="error" />}

      <Form
        layout="vertical"
        name="addDeal"
        form={categoryForm}
        onFinish={handleAddCategory}
        encType="multipart/form-data"
      >
        <Row gutter={8}>
          <Col span={4}>
            <Form.Item label="Parent Category" name="parentId">
              <Select
                disabled={statusSelect}
                placeholder="--All Category--"
                className="select-after"
              >
                <Option value={null}> </Option>

                {categories.map(category => (
                  <Option value={category.id}>{category.name}</Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              label="Name Category"
              name="name"
              rules={[{ required: true }]}
            >
              <Input
                name="name"
                onChange={getNameAndSetSlug}
                placeholder="Enter Name Category"
              />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              label="Slug Category"
              name="slug"
              rules={[{ required: true }]}
            >
              <Input name="slug" disabled placeholder="Slug Category" />
            </Form.Item>
          </Col>
        </Row>
        <Row style={{ justifyContent: 'center' }}>
          <Form.Item>
            <Button block type="primary" htmlType="submit" loading={inProgress}>
              Create
            </Button>
          </Form.Item>
        </Row>
      </Form>
    </>
  );
};

export default CategoryAddSAdmin;
