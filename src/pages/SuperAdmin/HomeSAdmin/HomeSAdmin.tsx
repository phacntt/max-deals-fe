import { Card, Col, Row } from 'antd';
import Banner from 'components/Banner/Banner';
import Container from 'components/Container/Container';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useAppSelector } from 'redux/hooks';
import { selectCurrentUser } from 'redux/slices/current-user.slice';
import {
  categoryService,
  dealService,
  storeService,
  userService,
} from 'services';
import {
  CardTotalCategory,
  CardTotalDeal,
  CardTotalStore,
  CardTotalUser,
} from './HomeSAdmin.style';

const HomeSAdmin: FC = () => {
  const [totalDeal, setTotalDeal] = useState<number>();
  const [totalCategory, setTotalCategory] = useState<number>();
  const [totalStore, setTotalStore] = useState<number>();
  const [totalAdmin, setTotalAdmin] = useState<number>();

  const currentUser = useAppSelector(selectCurrentUser);

  useEffect(() => {
    const fetchData = async () => {
      const listDeals = await dealService.getAllDeal({
        expiryDate: moment().toISOString(),
      });
      const listStores = await storeService.getAllStore();
      const listUser = await userService.getUsers({ role: 'admin' });
      const listCategory = await categoryService.getAllCategory();
      setTotalDeal(listDeals.length);
      setTotalStore(listStores.length);
      setTotalAdmin(listUser.length);
      setTotalCategory(listCategory.length);
    };
    fetchData();
  }, []);

  return (
    <Container>
      <Row>
        <Col span={24}>
          <h1
            style={{
              borderBottom: '1px solid black',
              paddingBottom: 15,
              marginTop: 20,
              fontSize: 24,
              whiteSpace: 'nowrap',
              overflow: 'hidden',
              textOverflow: 'ellipsis',
              width: '50%',
              fontWeight: 'bold',
              marginBottom: 40,
            }}
          >
            WELCOME {currentUser.name} TO THE ZAMZA
          </h1>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col span={6}>
          <CardTotalDeal title="Total Deals" bordered={false}>
            {totalDeal} Deals
          </CardTotalDeal>
        </Col>
        <Col span={6}>
          <CardTotalCategory title="Total Category" bordered={false}>
            {totalCategory} Category
          </CardTotalCategory>
        </Col>
        <Col span={6}>
          <CardTotalStore title="Total Stores" bordered={false}>
            {totalStore} Store
          </CardTotalStore>
        </Col>
        <Col span={6}>
          <CardTotalUser title="Total Admin" bordered={false}>
            {totalAdmin} Admin
          </CardTotalUser>
        </Col>
      </Row>
    </Container>
  );
};

export default HomeSAdmin;
