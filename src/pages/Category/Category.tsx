import { QueryGetDeal } from 'apis/deal.api';
import Container from 'components/Container/Container';
import DealMainCategory from 'components/DealMainCategory/DealMainCategory';
import TopBannerCollection from 'components/TopBannerCollection/TopBannerCollection';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import { useParams, useSearchParams } from 'react-router-dom';
import { categoryService, dealService } from 'services';
import { Category } from 'types/Category';
import { Deal } from 'types/Deal';

const CategoryPage: FC = () => {
  const [deals, setDeals] = useState<Deal[]>([]);
  const [categories, setCategories] = useState<Category[]>([]);
  const [mainCategories, setMainCategories] = useState<Category[]>([]);
  const [paramCategory, setParamCategory] = useState<number>();

  const { id } = useParams();
  const [search] = useSearchParams();
  const expiration = Boolean(search.get('expiration-soon'));
  const bigDiscount = Boolean(search.get('big-discount'));

  const idSearch = id
    ? Number((id as string).slice((id as string).lastIndexOf('-', 0)))
    : undefined;

  useEffect(() => {
    const fetchData = async () => {
      const currentDate = moment().toISOString();

      // Get Deals
      if (!idSearch) {
        const listDeals = await dealService.getAllDeal({
          id: 'DESC',
          expiryDate: currentDate,
        });
        setDeals(listDeals);
      }

      if (idSearch) {
        const listDeals = await dealService.getAllDeal({
          id: 'DESC',
          expiryDate: currentDate,
          categoryId: idSearch,
        });
        let listDealByCategory: Deal[];

        if (listDeals.length === 0) {
          const getAllDeals = await dealService.getAllDeal({
            id: 'DESC',
            expiryDate: currentDate,
          });
          listDealByCategory = getAllDeals.filter(
            deal => deal.category.parentId === idSearch,
          );
          setDeals(listDealByCategory);
        } else {
          setDeals(listDeals);
        }

        // setParamCategory(idSearch);
      }

      if (expiration) {
        const listDeals = await dealService.getAllDeal({
          id: 'DESC',
          expiryDate: currentDate,
        });
        const listDealsExpirySoon = listDeals.filter(
          deal =>
            moment(deal.expiryDate).diff(moment(), 'days') >= 0 &&
            moment(deal.expiryDate).diff(moment(), 'days') <= 7,
        );
        setParamCategory(undefined);
        setDeals(listDealsExpirySoon);
      }

      if (bigDiscount) {
        const listDeals = await dealService.getAllDeal({
          discount: 'DESC',
          expiryDate: currentDate,
        });

        setParamCategory(undefined);
        setDeals(listDeals);
      }
      // console.log('search', idCate);
    };
    fetchData();
  }, [idSearch, expiration]);

  useEffect(() => {
    const fetchData = async () => {
      const getCategories = await categoryService.getAllCategory({
        sort: 'ASC',
        parentOnly: false,
      });
      setCategories(getCategories);

      // Get Category
      const getCategorySidebar = await categoryService.getAllCategory();
      setCategories(getCategorySidebar);

      const mainCategory = getCategorySidebar.filter(
        category => category.parentCategory === null,
      );

      setMainCategories(mainCategory);
    };
    fetchData();
  }, []);

  const dataDeals = deals.filter(deal =>
    paramCategory ? deal.category.id === paramCategory : deal,
  );
  const dealParentCate = deals.filter(deal =>
    paramCategory ? deal.category.parentId === paramCategory : deal,
  );

  return (
    <Container>
      {dataDeals.length > 3 && dealParentCate.length === 0 ? (
        <TopBannerCollection listDeal={dataDeals.slice(0, 3)} />
      ) : (
        ''
      )}
      {dealParentCate.length > 3 && dataDeals.length < 3 ? (
        <TopBannerCollection listDeal={dealParentCate.slice(0, 3)} />
      ) : (
        ''
      )}

      {(dealParentCate.length > 3 && dataDeals.length > 3) || !paramCategory ? (
        <TopBannerCollection listDeal={deals.slice(0, 3)} />
      ) : (
        ''
      )}

      {dataDeals.length !== 0 ? (
        <DealMainCategory
          mainCategory={mainCategories}
          categories={categories}
          listDeal={dataDeals}
        />
      ) : (
        <DealMainCategory
          mainCategory={mainCategories}
          categories={categories}
          listDeal={dealParentCate}
        />
      )}
    </Container>
  );
};

export default CategoryPage;
