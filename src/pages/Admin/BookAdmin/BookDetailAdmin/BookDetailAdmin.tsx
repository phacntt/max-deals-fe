import { Alert, Button, Card, Descriptions } from 'antd';
import moment from 'moment';
import { DescriptionConfig } from 'pages/BookingResult/BookingResult.style';
import React, { FC, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { bookService } from 'services';
import { Book } from 'types/Book';

const BookDetailAdmin: FC = () => {
  const navigate = useNavigate();
  const [errorMessage, setErrorMessage] = useState('');

  const [inProgress, setInProgress] = useState(false);
  const [book, setBook] = useState<Book>();

  const { bookId } = useParams();

  useEffect(() => {
    const fetchData = async () => {
      const bookDetailById = await bookService.getBookById(Number(bookId));
      setBook(bookDetailById);
    };
    fetchData();
  }, []);

  console.log(bookId);
  console.log(book);

  return (
    <>
      <h1
        style={{
          borderBottom: '1px solid black',
          paddingBottom: 15,
          marginTop: 20,
          fontSize: 24,
          whiteSpace: 'nowrap',
          overflow: 'hidden',
          textOverflow: 'ellipsis',
          width: '50%',
          fontWeight: 'bold',
        }}
      >
        BOOKING DETAIL
      </h1>
      {errorMessage && <Alert message={errorMessage} type="error" />}

      <Card>
        <DescriptionConfig title="INSIGHTS" bordered layout="vertical">
          <Descriptions.Item label="Name Deal" span={3}>
            {book?.deal.name}
          </Descriptions.Item>
          <Descriptions.Item label="Discount">
            {book?.deal.discount} %
          </Descriptions.Item>
          <Descriptions.Item label="Category">
            {book?.deal.category.name}
          </Descriptions.Item>
          <Descriptions.Item label="Date Steal">
            {moment(book?.dateBook).format('YYYY-MM-DD')}
          </Descriptions.Item>
          <Descriptions.Item label="Usage Time">
            {moment(book?.deal.expiryDate).diff(moment(book?.dateBook), 'days')}{' '}
            days
          </Descriptions.Item>
          <Descriptions.Item label="Store">
            {book?.deal.store.name}
          </Descriptions.Item>
          <Descriptions.Item label="Address" span={2}>
            {book?.deal.store.fullAddress}
          </Descriptions.Item>
          <Descriptions.Item label="Contact Phone">
            {book?.deal.store.phone}
          </Descriptions.Item>

          <Descriptions.Item label="Code Verify" span={24}>
            <div style={{ fontSize: 20, fontWeight: 700, textAlign: 'center' }}>
              {book?.codeVerify}
            </div>
          </Descriptions.Item>
          <Descriptions.Item label="User">{book?.user.name}</Descriptions.Item>
          <Descriptions.Item label="Gmail">
            {book?.user.email}
          </Descriptions.Item>
          <Descriptions.Item label="Phone">
            {book?.user.phone}
          </Descriptions.Item>
        </DescriptionConfig>
      </Card>
    </>
  );
};
export default BookDetailAdmin;
