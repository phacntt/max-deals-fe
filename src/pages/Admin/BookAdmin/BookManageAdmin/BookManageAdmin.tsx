import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import { Badge, Button, notification, Table } from 'antd';
import { getAPIErrorMessage, APIError } from 'apis/api';
import { QueryGetDeal } from 'apis/deal.api';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useAppSelector } from 'redux/hooks';
import { selectCurrentUser } from 'redux/slices/current-user.slice';
import { categoryService, bookService } from 'services';
import { Book } from 'types/Book';
import { Category } from 'types/Category';
import { Deal } from 'types/Deal';

const BookManageAdmin: FC = () => {
  const navigate = useNavigate();
  const [inProgress, setInProgress] = useState(false);

  const [filterValues, setFilterValues] = useState<QueryGetDeal>({});

  const [categories, setCategories] = useState<Category[]>([]);

  const currentUser = useAppSelector(selectCurrentUser);

  const [books, setBooks] = useState<Book[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      const listBooks = await bookService.getAllBook({ status: 'Unused' });
      const listBooksByStoreID = listBooks.filter(
        book => book.deal.storeId === currentUser.storeId,
      );
      setBooks(listBooksByStoreID);

      const getCategories = await categoryService.getAllCategory({
        parentOnly: false,
      });
      setCategories(getCategories);

      console.log(filterValues);
    };
    fetchData();
  }, []);

  function renderStatus(text: string) {
    if (text === 'Unused') {
      return 'processing';
    }
    if (text === 'Used') {
      return 'success';
    }
    if (text === 'Expired') {
      return 'error';
    }
    return 'default';
  }

  const setNotification = (type: boolean, error?: unknown) => {
    if (type === true) {
      notification.open({
        message: 'Delete Booking Success',

        icon: <CheckCircleOutlined style={{ color: '#10e926' }} />,
      });
      setTimeout(() => notification.destroy(), 5000);
    } else {
      notification.open({
        message: 'Delete Booking Failure',
        description: `${getAPIErrorMessage(error as APIError)}`,
        icon: <CloseCircleOutlined style={{ color: '#fc3131' }} />,
      });
      setTimeout(() => notification.destroy(), 5000);
    }
  };

  const hanldeDeleteBook = async (idBook: number) => {
    try {
      await bookService.deleteBook(idBook);
      const listBookAfterDelete = await bookService.getAllBook();
      setBooks(listBookAfterDelete);
      setNotification(true);
      return navigate(`/admin/bookmanage`);
    } catch (error) {
      setNotification(false, error);
    }
    return setInProgress(false);
  };

  const columns = [
    {
      title: 'Id',
      dataIndex: 'id',
    },
    {
      title: 'Name',
      render: (record: Book) => (
        <a href={`/admin/bookmanage/${record.id}`}>{record.deal.name}</a>
      ),
      width: '40%',
    },
    {
      title: 'Date Book',
      dataIndex: 'dateBook',
      render: (record: string) => <p>{moment(record).format('DD/MM/YYYY')}</p>,
    },
    {
      title: 'Date Expiry',
      dataIndex: 'deal',
      render: (record: Deal) => (
        <p>{moment(record.expiryDate).format('DD/MM/YYYY')}</p>
      ),
    },
    {
      title: 'Code Verify',
      dataIndex: 'codeVerify',
    },

    {
      title: 'Status',
      dataIndex: 'status',
      render: (text: string) => (
        <Badge status={renderStatus(text)} text={text} />
      ),
    },

    {
      title: 'Action',
      render: (record: Deal) => (
        <Button onClick={() => hanldeDeleteBook(record.id)}>Delete</Button>
      ),
    },
  ];

  return (
    <>
      <h1
        style={{
          borderBottom: '1px solid black',
          paddingBottom: 15,
          marginTop: 20,
          fontSize: 24,
          width: '50%',
          fontWeight: 'bold',
        }}
      >
        LIST BOOK
      </h1>

      <Table columns={columns} dataSource={books} />
    </>
  );
};

export default BookManageAdmin;
