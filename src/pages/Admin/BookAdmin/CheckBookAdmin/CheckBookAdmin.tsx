import { Badge, Button, Col, Form, Input, Row, Table } from 'antd';
import Modal, { ModalProps } from 'antd/lib/modal/Modal';
import { getAPIErrorMessage, APIError } from 'apis/api';
import { UpdateBookRequestBody } from 'apis/book.api';
import moment from 'moment';
import React, { ChangeEvent, FC, useEffect, useRef, useState } from 'react';
import { QrReader } from 'react-qr-reader';
import { useNavigate, useParams } from 'react-router-dom';
import { useAppSelector } from 'redux/hooks';
import { selectCurrentUser } from 'redux/slices/current-user.slice';
import { bookService } from 'services';
import { Book } from 'types/Book';
import { Deal } from 'types/Deal';

const CheckBookAdmin: FC = () => {
  const navigate = useNavigate();

  const [code, setCode] = useState<string>('');
  const [books, setBooks] = useState<Book[]>([]);
  const [bookItemArr, setBookItemArr] = useState<Book[]>([]);
  const [inProgress, setInProgress] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const currentUser = useAppSelector(selectCurrentUser);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [status, setStatus] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      const listBookResp = await bookService.getAllBook({ status: 'Unused' });
      setBooks(listBookResp);
    };
    fetchData();
  }, []);

  const takeCodeVerify = (e: ChangeEvent<HTMLInputElement>) => {
    setCode(e.target.value.toString());
  };
  const compareCodeVerify = (codeVerify: string) => {
    const takeBookFilter = books.filter(
      bookItem => bookItem.codeVerify === codeVerify,
    );
    setBookItemArr(takeBookFilter);
  };

  const showModal = () => {
    setIsModalVisible(true);
    setStatus(false);
  };

  const handleOk = () => {
    setIsModalVisible(false);
    setStatus(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setStatus(true);
  };

  function renderStatus(text: string) {
    if (text === 'Unused') {
      return 'processing';
    }
    if (text === 'Used') {
      return 'success';
    }
    if (text === 'Expired') {
      return 'error';
    }
    return 'default';
  }

  const handleUpdateStatusBook = async (idBook: number) => {
    try {
      setInProgress(true);
      setErrorMessage('');

      const data: UpdateBookRequestBody = {
        status: 'Used',
      };

      const resp = await bookService.updatBook(idBook, data);
      if (resp) {
        return navigate('/admin/bookmanage');
      }

      setErrorMessage('Update Category is Failure');
    } catch (error: unknown) {
      setErrorMessage(getAPIErrorMessage(error as APIError));
    }

    return setInProgress(false);
  };

  const columns = [
    {
      title: 'Id',
      dataIndex: 'id',
    },
    {
      title: 'Name',
      dataIndex: 'deal',
      render: (record: Deal) => (
        <a href={`/admin/dealmanage/${record.id}`}>{record.name}</a>
      ),
      width: '40%',
    },
    {
      title: 'Date Book',
      dataIndex: 'dateBook',
      render: (record: string) => <p>{moment(record).format('DD/MM/YYYY')}</p>,
    },
    {
      title: 'Date Expiry',
      dataIndex: 'deal',
      render: (record: Deal) => (
        <p>{moment(record.expiryDate).format('DD/MM/YYYY')}</p>
      ),
    },
    {
      title: 'Code Verify',
      dataIndex: 'codeVerify',
    },

    {
      title: 'Status',
      dataIndex: 'status',
      render: (text: string) => (
        <Badge status={renderStatus(text)} text={text} />
      ),
    },
    {
      title: 'Action',
      render: (record: Deal) => (
        <Button onClick={() => handleUpdateStatusBook(record.id)}>Use</Button>
      ),
    },
  ];

  return (
    <>
      <h1
        style={{
          borderBottom: '1px solid black',
          paddingBottom: 15,
          marginTop: 20,
          fontSize: 24,
          width: '50%',
          fontWeight: 'bold',
        }}
      >
        CHECK BOOK
      </h1>
      <Form>
        <Row>
          <Col span={6}>
            <Form.Item label="Code Verify" required>
              <Input
                onChange={takeCodeVerify}
                placeholder="input placeholder"
              />
            </Form.Item>
          </Col>
          <Col span={1} />
          <Col span={2}>
            <Form.Item>
              <Button onClick={() => compareCodeVerify(code)}>Search</Button>
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item>
              <Button onClick={showModal}>QRCode Check</Button>
              {!status && (
                <Modal
                  title="CHECK QRCODE DEAL"
                  visible={isModalVisible}
                  onOk={handleOk}
                  onCancel={handleCancel}
                  style={{
                    textAlign: 'center',
                  }}
                >
                  <QrReader
                    onResult={(result, error) => {
                      if (result) {
                        compareCodeVerify(result.getText().toString());
                        setIsModalVisible(false);
                        setStatus(true);
                      }

                      if (error) {
                        console.log(error);
                      }
                    }}
                    scanDelay={300}
                    constraints={{ facingMode: 'environment' }}
                    videoId="QrCodeScannerPage__video"
                    containerStyle={{ height: 500 }}
                  />
                  <div
                    style={{
                      textDecoration: 'underline',
                      fontWeight: 'bold',
                    }}
                  >
                    Store staff ask customers to show QR code in front of the
                    camera to check
                  </div>
                </Modal>
              )}
            </Form.Item>
          </Col>
        </Row>

        <Table columns={columns} dataSource={bookItemArr} />
      </Form>
    </>
  );
};
export default CheckBookAdmin;
