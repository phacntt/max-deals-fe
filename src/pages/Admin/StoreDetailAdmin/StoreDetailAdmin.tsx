import { Alert, Button, Col, Form, Input, Row, Select } from 'antd';
import { useForm } from 'antd/lib/form/Form';
import TextArea from 'antd/lib/input/TextArea';
import { getAPIErrorMessage, APIError } from 'apis/api';
import { UpdateStoreRequestBody } from 'apis/store.api';
import { ButtonSearchMap } from 'components/ButtonSearchMap/ButtonSearchMap';
import FormUpload from 'components/FormUpload/FormUpload';
import Maps from 'components/Maps/Maps';
import { configEditor } from 'helpers/editorConfig';
import { getIdFromSlug } from 'helpers/getIdFromSlug';
import { setSlugify } from 'helpers/slugify';
import JoditEditor from 'jodit-react';
import React, { FC, useEffect, useMemo, useRef, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import {
  dealService,
  fileService,
  provinceService,
  storeService,
} from 'services';
import { District } from 'types/District';
import { File } from 'types/File';
import { Province } from 'types/Province';
import { Store } from 'types/Store';
import { Ward } from 'types/Ward';

export type StoreFormValues = {
  name: string;
  slug: string;
  description: string;
  phone: string;
  province: string;
  district: string;
  ward: string;
  street: string;
  fullAdrress: string;
  location: string;
};

export type StoreFormLocal = {
  province: string;
  district: string;
  ward: string;
  street: string;
  fullAdrress: string;
};

interface GeoLocation {
  lat: number;
  lng: number;
}

const StoreDetailAdmin: FC = () => {
  const [updateForm] = useForm();
  const { Option } = Select;
  const navigate = useNavigate();
  const [errorMessage, setErrorMessage] = useState('');
  const [inProgress, setInProgress] = useState(false);
  const [store, setStore] = useState<Store>();
  const [province, setProvince] = useState('');
  const [district, setDistrict] = useState('');
  const [address, setAddress] = useState('');
  const [street, setStreet] = useState('');

  const [location, setLocation] = useState<GeoLocation>();
  const [ward, setWard] = useState('');

  const [provinces, setProvinces] = useState<Province[]>([]);
  const [infoProvince, setInfoProvince] = useState<Province>();
  const [districts, setDistricts] = useState<District[]>([]);
  const [wards, setWards] = useState<Ward[]>([]);
  const [images, setImages] = useState<File[]>([]);
  const [isMultiple, setIsMultiple] = useState<boolean>();
  const [handleUpdate, setHandleUpdate] = useState<boolean>();

  const { storeId } = useParams();

  const idStore = getIdFromSlug(storeId);

  const [placeholder, setPlaceholder] = useState<string>('');

  const editor = useRef(null);
  const [content, setContent] = useState('');

  const config = useMemo<any>(configEditor, [placeholder]);

  useEffect(() => {
    const fetchDataStore = async () => {
      const typeEntity = 'StoreEntity';

      const storeById = await storeService.getStoreById(idStore as number);
      const listProvinces = await provinceService.getAllProvince();

      const listFileImages = await fileService.getFileById(
        idStore as number,
        typeEntity,
      );
      storeById.images = listFileImages;

      const getInfoProvince = await provinceService.getProvinceById(
        storeById.province,
      );

      const districtFind = (getInfoProvince.districts as District[]).find(
        districtEle => districtEle.code === storeById.district,
      );

      const wardFind = (districtFind?.wards as Ward[]).find(
        wardEle => wardEle.code === storeById.ward,
      );

      setInfoProvince(getInfoProvince as Province);
      setDistricts(getInfoProvince.districts as District[]);
      setWards(districtFind?.wards as Ward[]);

      setStore(storeById);
      setImages(listFileImages);

      setProvinces(listProvinces);

      setProvince(getInfoProvince?.fullName as string);
      setDistrict(districtFind?.fullName as string);
      setWard(wardFind?.fullName as string);
      setAddress(storeById.fullAddress);

      setIsMultiple(false);

      updateForm.setFieldsValue({
        name: storeById.name,
        slug: storeById.slug,
        description: storeById.description,
        phone: storeById.phone,
        province: storeById.province,
        district: districtFind?.code,
        ward: wardFind?.code,
        street: storeById.street,
      });
    };
    fetchDataStore();
  }, []);

  const handleUploadFile = (newImages: File[]) => {
    setImages(newImages);
  };

  const onChangeProvince = async (e: React.ChangeEvent<HTMLSelectElement>) => {
    const getInfoProvince = await provinceService.getProvinceById(e.toString());
    setProvince(getInfoProvince.fullName);
    setInfoProvince(getInfoProvince);
    setHandleUpdate(true);
  };

  const onChangeDistrict = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const infoDistrict = infoProvince?.districts?.find(
      districtEle => districtEle.code === e.toString(),
    );
    const listWard = infoDistrict?.wards?.filter(item => item);
    setDistrict(infoDistrict?.fullName as string);
    setWards(listWard as Ward[]);
    setHandleUpdate(true);
  };

  const onChangeWard = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const getNameWard = wards.find(wardItem => wardItem.code === e.toString());
    setWard(getNameWard?.fullName as string);
    setHandleUpdate(true);
  };

  const onChangeStreet = (e: React.ChangeEvent<HTMLInputElement>) => {
    setStreet(e.target.value);
    setHandleUpdate(true);
  };

  const getNameAndSetSlug = (e: React.ChangeEvent<HTMLInputElement>) => {
    updateForm.setFieldsValue({
      slug: setSlugify(e.target.value),
    });
  };

  const handleGetAddress = () => {
    const fullAdrress = `${street}, ${ward}, ${district}, ${province}`;
    setAddress(fullAdrress);
    setHandleUpdate(false);
  };

  const handleLocation = (locations: GeoLocation) => {
    setLocation(locations);
  };

  const handleUpdateStore = async (formValue: StoreFormValues) => {
    try {
      if (images.length === 0) {
        setErrorMessage('Deal must be at least 1 image');
        return navigate(`/admin/storemanage/${storeId}`);
      }
      setInProgress(true);
      setErrorMessage('');

      if (handleUpdate === true) {
        setErrorMessage('Please click button "Search" to get location. Thanks');
        setInProgress(false);
        return navigate(`/admin/storemanage/${storeId}`);
      }

      const data: UpdateStoreRequestBody = {
        name: formValue.name,
        slug: formValue.slug,
        description: formValue.description,
        phone: formValue.phone,
        province: formValue.province,
        district: formValue.district,
        ward: formValue.ward,
        street: formValue.street,
        fullAddress: `${formValue.street}, ${ward}, ${district}, ${province}`,
        lat: location?.lat as number,
        lng: location?.lng as number,
      };
      console.log(data);

      const currentIds = images.map(image => image.id);

      const resp = await storeService.updateStore(
        parseInt(storeId as string, 10),
        data,
        currentIds,
      );

      if (resp) {
        return navigate(`/admin/storemanage`);
      }

      setErrorMessage('Update Store is Failure');
    } catch (error: unknown) {
      setErrorMessage(getAPIErrorMessage(error as APIError));
    }

    return setInProgress(false);
  };

  return (
    <>
      {console.log(images as File[])}
      <h1
        style={{
          borderBottom: '1px solid black',
          paddingBottom: 15,
          marginTop: 20,
          fontSize: 24,
          width: '50%',
          fontWeight: 'bold',
        }}
      >
        UPDATE STORE {store?.name}
      </h1>
      {errorMessage && <Alert message={errorMessage} type="error" />}

      <Form
        layout="vertical"
        name="addStore"
        form={updateForm}
        onFinish={handleUpdateStore}
        encType="multipart/form-data"
      >
        <Row gutter={8}>
          <Col span={4}>
            <Form.Item
              label="Name Store"
              name="name"
              rules={[{ required: true }]}
            >
              <Input
                onChange={getNameAndSetSlug}
                name="name"
                placeholder="Enter Name Store"
              />
            </Form.Item>
          </Col>

          <Col span={4}>
            <Form.Item label="Slug" name="slug" rules={[{ required: true }]}>
              <Input name="slug" placeholder="Enter Name Slug" />
            </Form.Item>
          </Col>

          <Col span={4}>
            <Form.Item
              label="Phone number"
              name="phone"
              rules={[{ required: true }]}
            >
              <Input name="phone" placeholder="Enter phone number" />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Form.Item
              label="Description"
              name="description"
              rules={[{ required: true }]}
            >
              <JoditEditor
                ref={editor}
                value={content}
                config={config}
                onBlur={newContent => setContent(newContent)} // preferred to use only this option to update the content for performance reasons
                onChange={newContent => ({})}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={8}>
            <FormUpload
              isMultiple={isMultiple as boolean}
              value={images}
              onChange={handleUploadFile}
            />
          </Col>
        </Row>
        <Row gutter={8}>
          <Col span={4}>
            <Form.Item
              label="Provinces"
              name="province"
              rules={[{ required: true }]}
            >
              <Select
                placeholder="--All Provinces--"
                className="select-after"
                onChange={onChangeProvince}
              >
                {provinces.map(provinceItem => (
                  <Option value={provinceItem.code}>
                    {provinceItem.fullName}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={4}>
            <Form.Item
              label="Districts"
              name="district"
              rules={[{ required: true }]}
            >
              <Select
                placeholder="--All Districts--"
                className="select-after"
                onChange={onChangeDistrict}
              >
                {districts.map(unit => (
                  <Option value={unit.code}>{unit.fullName}</Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={4}>
            <Form.Item label="Ward" name="ward" rules={[{ required: true }]}>
              <Select
                placeholder="--All Wards--"
                className="select-after"
                onChange={onChangeWard}
              >
                {wards.map(wardItem => (
                  <Option value={wardItem.code}>{wardItem.fullName}</Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={4}>
            <Form.Item
              label="Street"
              name="street"
              rules={[{ required: true }]}
            >
              <Input
                onChange={onChangeStreet}
                name="street"
                placeholder="Enter Name Street"
              />
            </Form.Item>
          </Col>
          <Col span={4}>
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                marginTop: 30,
              }}
            >
              <ButtonSearchMap onClick={handleGetAddress}>
                Search
              </ButtonSearchMap>
            </div>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Maps fullAddress={address} onChange={handleLocation} />
          </Col>
        </Row>

        <Row style={{ justifyContent: 'center' }}>
          <Form.Item>
            <Button
              style={{ marginTop: 30 }}
              block
              type="primary"
              htmlType="submit"
              loading={inProgress}
            >
              Update
            </Button>
          </Form.Item>
        </Row>
      </Form>
    </>
  );
};

export default StoreDetailAdmin;
