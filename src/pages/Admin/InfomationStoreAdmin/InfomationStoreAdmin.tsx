import { Badge, Button, Col, Descriptions, Row } from 'antd';
import Container from 'components/Container/Container';
import MapsDeal from 'components/Maps/MapsDeal';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { useAppSelector } from 'redux/hooks';
import { selectCurrentUser } from 'redux/slices/current-user.slice';
import { fileService, provinceService, storeService } from 'services';
import { District } from 'types/District';
import { File } from 'types/File';
import { Province } from 'types/Province';
import { Store } from 'types/Store';
import parse from 'html-react-parser';
import { getIdFromSlug } from 'helpers/getIdFromSlug';
import { CardInfoStore, DescriptionStore } from './InfomationStoreAdmin.style';

interface GeoLocation {
  lat: number;
  lng: number;
}

const InfomationStoreAdmin: FC = () => {
  const [province, setProvince] = useState('');
  const [district, setDistrict] = useState('');
  const [ward, setWard] = useState('');
  const [store, setStore] = useState<Store>();
  const [image, setImage] = useState<File>();
  const [area, setArea] = useState<Province>();
  const [location, setLocation] = useState<GeoLocation>();
  const currentUser = useAppSelector(selectCurrentUser);
  const { storeId } = useParams();

  const idStore = getIdFromSlug(storeId);

  useEffect(() => {
    const fetchDataStore = async () => {
      const infoStore = await storeService.getStoreById(idStore as number);
      setStore(infoStore);
      const locationStore: GeoLocation = {
        lat: infoStore.lat,
        lng: infoStore.lng,
      };
      setLocation(locationStore as GeoLocation);

      const infoArea = await provinceService.getProvinceById(
        infoStore.province as string,
      );
      setArea(infoArea);
      setProvince(infoArea.fullName);

      const districtStore = infoArea.districts?.find(
        districtItem => districtItem.code === store?.district,
      );

      const wardStore = (districtStore as District).wards?.find(
        item => item.code === store?.ward,
      );
      setDistrict(districtStore?.fullName as string);

      setWard(wardStore?.fullName as string);

      const imageStore = await fileService.getFileById(
        idStore as number,
        'StoreEntity',
      );
      setImage(imageStore[0]);
    };
    fetchDataStore();
  }, [province]);

  return (
    <Container>
      {console.log(store)}
      <Row>
        <Col span={24}>
          <CardInfoStore title="INFOMATION STORE">
            {store && (
              <DescriptionStore
                layout="vertical"
                bordered
                extra={
                  <Link to={`/admin/storedetail/${storeId}`}>
                    <Button type="primary">Edit</Button>
                  </Link>
                }
              >
                <Descriptions.Item label="Name Store">
                  {store.name}
                </Descriptions.Item>
                <Descriptions.Item label="Phone Number">
                  {store.phone}
                </Descriptions.Item>
                <Descriptions.Item label="Date of entry">
                  {moment(store.createdAt).format('YYYY/MM/DD')}
                </Descriptions.Item>
                <Descriptions.Item label="Ward">{ward}</Descriptions.Item>
                <Descriptions.Item label="District">
                  {district}
                </Descriptions.Item>
                <Descriptions.Item label="Province">
                  {province}
                </Descriptions.Item>
                <Descriptions.Item label="Address" span={2}>
                  {store.fullAddress}
                </Descriptions.Item>
                <Descriptions.Item label="Status">
                  <Badge status="processing" text="Online" />
                </Descriptions.Item>
                <Descriptions.Item label="Description" span={3}>
                  <div
                    dangerouslySetInnerHTML={{ __html: store.description }}
                  />
                </Descriptions.Item>
                <Descriptions.Item label="Map">
                  {location && <MapsDeal location={location as GeoLocation} />}
                </Descriptions.Item>
              </DescriptionStore>
            )}
          </CardInfoStore>
        </Col>
      </Row>
    </Container>
  );
};

export default InfomationStoreAdmin;
