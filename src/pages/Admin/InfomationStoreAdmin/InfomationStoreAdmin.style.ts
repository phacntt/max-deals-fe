import { Card, Descriptions } from 'antd';
import styled from 'styled-components';

export const CardInfoStore = styled(Card)`
  margin-top: 20px;
  .ant-card-head-title {
    text-align: center;
    font-size: 21px;
    font-weight: bold;
  }
`;

export const DescriptionStore = styled(Descriptions)`
  .ant-descriptions-item-label {
    font-weight: bold;
  }
`;
