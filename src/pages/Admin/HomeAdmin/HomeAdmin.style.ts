import { Card } from 'antd';
import styled from 'styled-components';

export const CardTotalDeal = styled(Card)`
  .ant-card-head {
    background: #fd4242;
  }
  .ant-card-head-title {
    text-align: center;
    font-size: 20px;
    font-weight: bold;
  }
  .ant-card-body {
    text-align: center;
    font-size: 20px;
    font-weight: bold;
  }
`;

export const CardTotalBooking = styled(Card)`
  .ant-card-head {
    background: #42fde5;
  }
  .ant-card-head-title {
    text-align: center;
    font-size: 20px;
    font-weight: bold;
  }
  .ant-card-body {
    text-align: center;
    font-size: 20px;
    font-weight: bold;
  }
`;
