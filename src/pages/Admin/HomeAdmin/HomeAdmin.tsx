import { Card, Col, Row } from 'antd';
import Banner from 'components/Banner/Banner';
import Container from 'components/Container/Container';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useAppSelector } from 'redux/hooks';
import { selectCurrentUser } from 'redux/slices/current-user.slice';
import {
  bookService,
  categoryService,
  dealService,
  storeService,
  userService,
} from 'services';
import { CardTotalBooking, CardTotalDeal } from './HomeAdmin.style';

const HomeAdmin: FC = () => {
  const [totalDeal, setTotalDeal] = useState<number>();
  const [totalBooking, setTotalBooking] = useState<number>();

  const currentUser = useAppSelector(selectCurrentUser);

  useEffect(() => {
    const fetchData = async () => {
      const infoUser = await userService.getUserById(Number(currentUser.id));
      const listDeals = await dealService.getAllDeal({
        storeId: infoUser.storeId,
        expiryDate: moment().toISOString(),
      });
      const listBooking = await bookService.getAllBook();
      const listBookingByStore = listBooking.filter(
        booking => booking.deal.storeId === currentUser.storeId,
      );
      setTotalDeal(listDeals.length);
      setTotalBooking(listBookingByStore.length);
    };
    fetchData();
  }, []);

  return (
    <Container>
      <Row>
        <Col span={24}>
          <h1
            style={{
              borderBottom: '1px solid black',
              paddingBottom: 15,
              marginTop: 20,
              fontSize: 24,
              whiteSpace: 'nowrap',
              overflow: 'hidden',
              textOverflow: 'ellipsis',
              width: '50%',
              fontWeight: 'bold',
              marginBottom: 40,
            }}
          >
            WELCOME {currentUser.name} TO THE ZAMZA
          </h1>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col span={8}>
          <CardTotalDeal title="Total Deals" bordered={false}>
            {totalDeal} Deals
          </CardTotalDeal>
        </Col>

        <Col span={8}>
          <CardTotalBooking title="Total Booking" bordered={false}>
            {totalBooking} Booking
          </CardTotalBooking>
        </Col>
      </Row>
    </Container>
  );
};

export default HomeAdmin;
