import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import { Button, Col, notification, Row, Select, Table } from 'antd';
import { getAPIErrorMessage, APIError } from 'apis/api';
import { QueryGetDeal } from 'apis/deal.api';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useAppSelector } from 'redux/hooks';
import { selectCurrentUser } from 'redux/slices/current-user.slice';
import {
  categoryService,
  dealService,
  storeService,
  userService,
} from 'services';
import { Category } from 'types/Category';
import { Deal } from 'types/Deal';
import { Store } from 'types/Store';
import { User } from 'types/User';

const DealManageAdmin: FC = () => {
  const { Option } = Select;
  const navigate = useNavigate();
  const [inProgress, setInProgress] = useState(false);

  const [deals, setDeals] = useState<Deal[]>([]);
  const [filterValues, setFilterValues] = useState<QueryGetDeal>({});
  const [categories, setCategories] = useState<Category[]>([]);
  const [stores, setStores] = useState<Store[]>([]);

  const [disable, setDisable] = useState<boolean>(false);

  const [status, setStatus] = useState<boolean>(false);
  const [infomationAdmin, setInfomationAdmin] = useState<User>();

  const currentUser = useAppSelector(selectCurrentUser);

  const [page, setPage] = useState(1);

  useEffect(() => {
    const fetchData = async () => {
      const infoAdmin = await userService.getUserById(Number(currentUser.id));

      const getDeals = await dealService.getAllDeal({
        id: 'DESC',
        storeId: infoAdmin.storeId,
      });
      setInfomationAdmin(infoAdmin);
      const getCategories = await categoryService.getAllCategory({
        parentOnly: false,
      });
      const getStores = await storeService.getAllStore();
      setCategories(getCategories);
      setStores(getStores);
      if (getDeals) {
        setStatus(true);
      }
      setDeals(getDeals);
      setDisable(true);
      console.log('Load');

      console.log(filterValues);
    };
    fetchData();
  }, []);

  const setNotification = (type: boolean, error?: unknown) => {
    if (type === true) {
      notification.open({
        message: 'Delete Deal Success',

        icon: <CheckCircleOutlined style={{ color: '#10e926' }} />,
      });
      setTimeout(() => notification.destroy(), 5000);
    } else {
      notification.open({
        message: 'Delete Deal Failure',
        description: `${getAPIErrorMessage(error as APIError)}`,
        icon: <CloseCircleOutlined style={{ color: '#fc3131' }} />,
      });
      setTimeout(() => notification.destroy(), 5000);
    }
  };

  const hanldeDeleteDeal = async (idDeal: number) => {
    try {
      await dealService.deleteDeal(idDeal);
      const getdeals = await dealService.getAllDeal({
        storeId: infomationAdmin?.storeId,
      });
      setDeals(getdeals);
      setNotification(true);
      return navigate('/admin/dealmanage');
    } catch (error) {
      setNotification(false, error);
    }
    return setInProgress(false);
  };

  const columns = [
    {
      title: 'id',
      dataIndex: 'id',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      render: (text: string, record: Deal) => (
        <a href={`/admin/dealmanage/${record.slug}`}>{text}</a>
      ),
    },
    {
      title: 'Expiry Date',
      dataIndex: 'expiryDate',
      render: (record: string) => <p>{moment(record).format('DD/MM/YYYY')}</p>,
    },
    {
      title: 'Quantity',
      dataIndex: 'quantity',
    },
    {
      title: 'Discount(%)',
      dataIndex: 'discount',
    },
    {
      title: 'Category',
      dataIndex: 'category',
      render: (record: Category) => record.name,
    },

    {
      title: 'Action',
      render: (record: Deal) => (
        <Button onClick={() => hanldeDeleteDeal(record.id)}>Delete</Button>
      ),
    },
  ];

  const handleSelectCategory = (newCategoryId: string) => {
    setFilterValues({
      ...filterValues,
      categoryId: parseInt(newCategoryId, 10),
    });
  };

  const handleSelectStore = (newStoreId: string) => {
    setFilterValues({ ...filterValues, storeId: parseInt(newStoreId, 10) });
  };

  const fillterDeal = async () => {
    const queryParams: QueryGetDeal = {};
    // Check not: null, undefined, 0, '', false
    if (filterValues.categoryId) {
      queryParams.categoryId = filterValues.categoryId;
    }

    queryParams.storeId = Number(currentUser.id);

    const dealsFilter = await dealService.getAllDeal(queryParams);
    setDeals(dealsFilter);
    setDisable(false);
  };

  const resetFillter = async () => {
    setDisable(true);
    const callDealsApi = await dealService.getAllDeal();
    setDeals(callDealsApi);
    setFilterValues({});
  };

  return (
    <>
      <h1
        style={{
          borderBottom: '1px solid black',
          paddingBottom: 15,
          marginTop: 20,
          fontSize: 24,
          width: '50%',
          fontWeight: 'bold',
        }}
      >
        LIST DEAL
      </h1>
      <Row>
        <Col span={3}>
          <Select
            style={{ width: 150 }}
            id="dropdown-category"
            className="select-after"
            placeholder="All Categories"
            onChange={handleSelectCategory}
            value={
              filterValues.categoryId ? `${filterValues.categoryId}` : undefined
            }
          >
            {categories.map(category => (
              <Option className="option" value={`${category.id}`}>
                {category.name}
              </Option>
            ))}
          </Select>
        </Col>
        {/* <Col span={3}>
          <Select
            style={{ width: 150 }}
            id="dropdown-store"
            className="select-after"
            placeholder="All Stores"
            onChange={handleSelectStore}
            value={filterValues.storeId ? `${filterValues.storeId}` : undefined}
          >
            {stores.map(store => (
              <Option value={`${store.id}`}>{store.name}</Option>
            ))}
          </Select>
        </Col> */}
        <Col span={2}>
          <Button style={{ width: 100 }} onClick={fillterDeal}>
            Fillter
          </Button>
        </Col>
        <Col span={3}>
          <Button onClick={resetFillter} disabled={disable}>
            Reset Fillter
          </Button>
        </Col>
      </Row>
      <Table columns={columns} dataSource={deals} />
    </>
  );
};

export default DealManageAdmin;
