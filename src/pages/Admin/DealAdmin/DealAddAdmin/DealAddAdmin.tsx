import {
  Form,
  Row,
  Col,
  Input,
  Select,
  Button,
  DatePicker,
  InputNumber,
  Alert,
  Upload,
} from 'antd';
import { useForm } from 'antd/lib/form/Form';
import TextArea from 'antd/lib/input/TextArea';
import { getAPIErrorMessage, APIError } from 'apis/api';
import { QueryGetCategory } from 'apis/category.api';
import { AddDealRequestBody } from 'apis/deal.api';
import FormUpload from 'components/FormUpload/FormUpload';
import { configEditor } from 'helpers/editorConfig';
import { setSlugify } from 'helpers/slugify';
import JoditEditor from 'jodit-react';
import moment, { Moment } from 'moment';
import React, { FC, useEffect, useMemo, useRef, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useAppSelector } from 'redux/hooks';
import { selectCurrentUser } from 'redux/slices/current-user.slice';
import {
  categoryService,
  dealService,
  storeService,
  userService,
} from 'services';
import slugify from 'slugify';
import { Category } from 'types/Category';
import { File } from 'types/File';
import { Store } from 'types/Store';

export type DealFormValues = {
  name: string;
  slug: string;
  img: number[];
  expiryDate: moment.Moment;
  quantity: number;
  description: string;
  discount: number;
  categoryId: string;
  storeId: string;
};

const DealAddAdmin: FC = () => {
  const { Option } = Select;
  const navigate = useNavigate();
  const [dealForm] = useForm();
  const [errorMessage, setErrorMessage] = useState('');
  const [inProgress, setInProgress] = useState(false);
  const [categories, setCategories] = useState<Category[]>([]);
  const [store, setStore] = useState<Store>();
  const [images, setImages] = useState<File[]>([]);
  const [isMultiple, setIsMultiple] = useState<boolean>();
  const currentUser = useAppSelector(selectCurrentUser);

  const [placeholder, setPlaceholder] = useState<string>('');

  const editor = useRef(null);
  const [content, setContent] = useState('');

  const config = useMemo<any>(configEditor, [placeholder]);

  useEffect(() => {
    const fetchData = async () => {
      const infoAdmin = await userService.getUserById(Number(currentUser.id));
      const listCategories = await categoryService.getAllCategory({
        parentOnly: false,
      });
      const storeInfo = await storeService.getStoreById(infoAdmin.storeId);
      setCategories(listCategories);
      setStore(storeInfo);
      setIsMultiple(true);
    };
    fetchData();

    return () => {
      console.log('Unmount');
    };
  }, []);

  console.log(categories);

  const handleAddDeal = async (formValue: DealFormValues) => {
    try {
      setInProgress(true);
      setErrorMessage('');

      const fileIds = images.map(image => image.id);

      const data: AddDealRequestBody = {
        name: formValue.name,
        slug: formValue.slug,
        imageIds: fileIds,
        expiryDate: formValue.expiryDate.toISOString(),
        quantity: formValue.quantity,
        description: formValue.description,
        discount: formValue.discount,
        categoryId: parseInt(formValue.categoryId, 10),
        storeId: store?.id as number,
      };

      const resp = await dealService.addDeal(data);
      if (resp) {
        return navigate(`/admin/dealmanage`);
      }

      console.log(data);

      setErrorMessage('Create Deal is Failure');
    } catch (error: unknown) {
      setErrorMessage(getAPIErrorMessage(error as APIError));
    }
    console.log('Received values of form: ', formValue);

    return setInProgress(false);
  };

  const handleUploadFile = (newImages: File[]) => {
    setImages(newImages);
  };

  const disabledDate = (current: Moment) => {
    return current && current < moment().endOf('day');
  };

  const getNameAndSetSlug = (e: React.ChangeEvent<HTMLInputElement>) => {
    dealForm.setFieldsValue({
      slug: setSlugify(e.target.value),
    });
  };

  return (
    <>
      <h1
        style={{
          borderBottom: '1px solid black',
          paddingBottom: 15,
          marginTop: 20,
          fontSize: 24,
          width: '50%',
          fontWeight: 'bold',
        }}
      >
        CREATE NEW DEAL
      </h1>
      {errorMessage && <Alert message={errorMessage} type="error" />}

      <Form
        layout="vertical"
        name="addDeal"
        form={dealForm}
        onFinish={handleAddDeal}
        encType="multipart/form-data"
      >
        <Row gutter={16}>
          <Col span={8}>
            <Form.Item
              label="Name Deal"
              name="name"
              rules={[{ required: true }]}
            >
              <Input
                name="name"
                onChange={getNameAndSetSlug}
                placeholder="Enter Name Deal"
              />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              label="Slug Deal"
              name="slug"
              rules={[{ required: true }]}
            >
              <Input name="slug" disabled placeholder="Slug Deal" />
            </Form.Item>
          </Col>
          <Col span={4}>
            <Form.Item
              label="Expiry Date"
              name="expiryDate"
              rules={[{ required: true }]}
            >
              <DatePicker
                disabledDate={disabledDate}
                id="expiryDate"
                style={{ width: '100%' }}
              />
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item label="Quantity" name="quantity">
              <InputNumber defaultValue={1} min={1} />
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item label="Discount(%)" name="discount">
              <InputNumber defaultValue={1} min={1} />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              label="Category"
              name="categoryId"
              rules={[{ required: true }]}
            >
              <Select placeholder="--All Category--" className="select-after">
                {categories.map(category => (
                  <Option value={category.id}>{category.name}</Option>
                ))}
              </Select>
            </Form.Item>
          </Col>

          <Col span={24}>
            <Form.Item label="Description" name="description">
              <JoditEditor
                ref={editor}
                value={content}
                config={config}
                onBlur={newContent => setContent(newContent)} // preferred to use only this option to update the content for performance reasons
                onChange={newContent => ({})}
              />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="Image (Max 3 images)*" name="img">
              <FormUpload
                isMultiple={isMultiple as boolean}
                value={images}
                onChange={handleUploadFile}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row style={{ justifyContent: 'center' }}>
          <Form.Item>
            <Button block type="primary" htmlType="submit" loading={inProgress}>
              Create
            </Button>
          </Form.Item>
        </Row>
      </Form>
    </>
  );
};

export default DealAddAdmin;
