import { Button, Col, Form, Input, Row } from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { getAPIErrorMessage, APIError } from 'apis/api';
import { UpdateUserRequestBody } from 'apis/user.api';
import { RegisterPanel } from 'pages/Register/Register.style';
import React, { FC, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from 'redux/hooks';
import { selectAuth } from 'redux/slices/auth.slice';
import { selectCurrentUser } from 'redux/slices/current-user.slice';
import { userService } from 'services';
import { User } from 'types/User';
import { SideBarListFeature } from '../User.style';

type UpdateFormValues = {
  username: string;
  password: string;
  email: string;
  name: string;
  phone: string;
};

const UserUpdate: FC = () => {
  const dispath = useAppDispatch();
  const auth = useAppSelector(selectAuth);
  const currentUser = useAppSelector(selectCurrentUser);
  const [user, setUser] = useState<User>();
  const [updateForm] = useForm();
  const [errorMessage, setErrorMessage] = useState('');
  const [inProgress, setInProgress] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    const fetchUser = async () => {
      const infoUser = await userService.getUserById(Number(currentUser.id));
      setUser(infoUser);

      updateForm.setFieldsValue({
        name: infoUser.name,
        email: infoUser.email,
        phone: infoUser.phone,
        username: infoUser.username,
      });
    };
    fetchUser();
  }, []);
  console.log(user);

  const handleUpdateUser = async (formValue: UpdateFormValues) => {
    try {
      setInProgress(true);
      setErrorMessage('');

      const data: UpdateUserRequestBody = {
        name: formValue.name,
        email: formValue.email,
        phone: formValue.phone,
        username: formValue.username,
      };

      console.log(data);

      const resp = await userService.updateUser(Number(currentUser.id), data);
      if (resp) {
        return navigate(`/user`);
      }

      setErrorMessage('Update User is Failure');
    } catch (error: unknown) {
      setErrorMessage(getAPIErrorMessage(error as APIError));
    }
    console.log('Received values of form: ', formValue);

    return setInProgress(false);
  };

  return (
    <SideBarListFeature title="USER INFO">
      {auth.isAuthenticated && (
        <RegisterPanel>
          <div
            style={{
              fontSize: 26,
              fontWeight: 'bold',
              textAlign: 'center',
              marginBottom: 20,
              fontFamily: 'initial',
            }}
          >
            UPDATE
          </div>
          <Form
            form={updateForm}
            name="login"
            layout="vertical"
            onFinish={handleUpdateUser}
          >
            <Form.Item
              name="username"
              rules={[{ required: true }]}
              label="Username"
            >
              <Input />
            </Form.Item>
            {/* <Form.Item
              name="password"
              rules={[{ required: true }]}
              label="Password"
            >
              <Input.Password
                // eslint-disable-next-line react/no-unstable-nested-components
                iconRender={visible =>
                  visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                }
              />
            </Form.Item> */}
            <Form.Item name="email" rules={[{ required: true }]} label="Email">
              <Input type="email" />
            </Form.Item>
            <Form.Item>
              <Row gutter={16}>
                <Col span={12}>
                  <Form.Item
                    name="name"
                    rules={[{ required: true }]}
                    label="Name"
                  >
                    <Input />
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item
                    name="phone"
                    rules={[{ required: true }]}
                    label="Phone Number"
                  >
                    <Input />
                  </Form.Item>
                </Col>
              </Row>
            </Form.Item>
            <Form.Item>
              <Button
                block
                type="primary"
                htmlType="submit"
                loading={inProgress}
              >
                Submit
              </Button>
            </Form.Item>
          </Form>
        </RegisterPanel>
      )}
    </SideBarListFeature>
  );
};

export default UserUpdate;
