import { Badge, Button, Col, List, Modal, Row, Table } from 'antd';
import Item from 'antd/lib/list/Item';
import { updateBooking } from 'helpers/updateBooking';
import moment, { Moment } from 'moment';
import { QRCodeSVG } from 'qrcode.react';

import React, { FC, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from 'redux/hooks';
import { selectAuth } from 'redux/slices/auth.slice';
import { selectCurrentUser } from 'redux/slices/current-user.slice';
import { bookService } from 'services';
import { Book } from 'types/Book';
import { Deal } from 'types/Deal';
import { SideBarListFeature } from '../User.style';

const UserBooking: FC = () => {
  const dispatch = useAppDispatch();
  const auth = useAppSelector(selectAuth);
  const currentUser = useAppSelector(selectCurrentUser);
  const [books, setBooks] = useState<Book[]>([]);
  const [isModalVisible, setIsModalVisible] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      const listBookByUserId = await bookService.getAllBook({
        userId: Number(currentUser.id),
        status: 'Unused',
      });

      const listBookUnused = listBookByUserId.filter(
        book => moment(book.deal.expiryDate).diff(moment(), 'days') < 0,
      );
      listBookUnused.map(book => updateBooking(book.id, book.status));

      setBooks(listBookByUserId);
      console.log(listBookByUserId.length);
    };
    fetchData();
  }, []);

  function renderStatus(text: string) {
    if (text === 'Unused') {
      return 'processing';
    }
    if (text === 'Used') {
      return 'success';
    }
    if (text === 'Expired') {
      return 'error';
    }
    return 'default';
  }

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const columns = [
    {
      title: 'Name Deal',
      dataIndex: 'deal',
      render: (record: Deal) => (
        <a href={`/deals/${record.slug}`}>{record.name}</a>
      ),
    },
    {
      title: 'Discount',
      dataIndex: 'deal',
      render: (record: Deal) => <span> {record.discount} %</span>,
    },
    {
      title: 'Code Verify',
      dataIndex: 'codeVerify',
    },
    {
      title: 'Status',
      dataIndex: 'status',
      render: (text: string) => (
        <Badge
          style={{
            whiteSpace: 'nowrap',
          }}
          status={renderStatus(text)}
          text={text}
        />
      ),
    },
    {
      title: 'Expiry Date',
      dataIndex: 'deal',
      render: (record: Deal) => (
        <div
          style={{
            whiteSpace: 'nowrap',
          }}
        >
          {moment(record.expiryDate).format('YYYY-MM-DD')}
        </div>
      ),
    },
    {
      title: 'Store',
      dataIndex: 'deal',
      render: (record: Deal) => (
        <a href={`/store/${record.store.slug}`}>{record.store.name}</a>
      ),
    },
    {
      title: 'Action',
      dataIndex: 'codeVerify',
      render: (record: string) => (
        <>
          <Button onClick={showModal}>Used</Button>
          <Modal
            title="CHECK QRCODE DEAL"
            visible={isModalVisible}
            onOk={handleOk}
            onCancel={handleCancel}
            style={{
              textAlign: 'center',
            }}
          >
            <QRCodeSVG size={150} value={record} />
            <div
              style={{
                marginTop: 20,
                textDecoration: 'underline',
                fontWeight: 'bold',
              }}
            >
              Please show the QR code to the staff at the store to check
            </div>
          </Modal>
        </>
      ),
    },
  ];

  return (
    <SideBarListFeature title="BOOKING">
      <Table dataSource={books} columns={columns} />
    </SideBarListFeature>
  );
};

export default UserBooking;
