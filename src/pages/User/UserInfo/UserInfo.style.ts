import { Descriptions } from 'antd';
import styled from 'styled-components';

export const DescriptionsConfig = styled(Descriptions)`
  .ant-descriptions-title {
    font-size: 23px;
    margin-left: 55.46px;
  }

  .ant-descriptions-item-label {
    font-weight: bold;
  }
`;
