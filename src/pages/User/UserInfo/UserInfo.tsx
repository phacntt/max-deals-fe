import { UserOutlined } from '@ant-design/icons';
import { Badge, Button, Col, Descriptions, Row } from 'antd';
import { updateBooking } from 'helpers/updateBooking';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from 'redux/hooks';
import { selectAuth } from 'redux/slices/auth.slice';
import { selectCurrentUser } from 'redux/slices/current-user.slice';
import { bookService, userService } from 'services';
import { Book } from 'types/Book';
import { User } from 'types/User';
import { SideBarListFeature } from '../User.style';
import { DescriptionsConfig } from './UserInfo.style';

const UserPage: FC = () => {
  const auth = useAppSelector(selectAuth);
  const currentUser = useAppSelector(selectCurrentUser);
  const [user, setUser] = useState<User>();
  const [books, setBooks] = useState<Book[]>();

  useEffect(() => {
    if (!currentUser) {
      return;
    }
    const fetchUser = async () => {
      const infoUser = await userService.getUserById(Number(currentUser.id));
      setUser(infoUser);
      const listBookDeals = await bookService.getAllBook({
        userId: Number(currentUser.id),
      });
      setBooks(listBookDeals);
    };
    fetchUser();
  }, [auth.isAuthenticated]);

  useEffect(() => {
    const updateStatusBooking = async () => {
      const listBooking = await bookService.getAllBook();
      const listBookUnused = listBooking.filter(
        book => moment(book.deal.expiryDate).diff(moment(), 'days') < 0,
      );
      listBookUnused.map(book => updateBooking(book.id, book.status));
    };
    updateStatusBooking();
  }, []);

  console.log(user);

  const countBookDeal = (listBooks: Book[], status: string) => {
    let count = 0;
    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < listBooks.length; i++) {
      if (listBooks[i].status === status) {
        count += 1;
      }
    }
    return count;
  };

  return (
    <SideBarListFeature title="USER INFO">
      {auth.isAuthenticated && (
        <div style={{ textAlign: 'center', margin: 20 }}>
          <Row>
            <Col span={24}>
              <UserOutlined style={{ fontSize: 24 }} />
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <DescriptionsConfig
                title="User Info"
                layout="vertical"
                bordered
                extra={
                  <Link to="/user/update-info">
                    <Button type="primary">Edit</Button>
                  </Link>
                }
              >
                <Descriptions.Item label="User Name">
                  {user && user.username}
                </Descriptions.Item>
                <Descriptions.Item label="Email">
                  {user && user.email}
                </Descriptions.Item>
                <Descriptions.Item label="Name">
                  {user && user.name}
                </Descriptions.Item>
                <Descriptions.Item label="Phone number">
                  {user && user.phone}
                </Descriptions.Item>
                <Descriptions.Item label="Join at">
                  {moment(user && user.createdAt).format('YYYY/MM/DD')}
                </Descriptions.Item>
                <Descriptions.Item label="Status">
                  <Badge status="processing" text="Online" />
                </Descriptions.Item>
                <Descriptions.Item label="Quantity Current Deals">
                  {books && countBookDeal(books as Book[], 'Unused')}
                </Descriptions.Item>
                <Descriptions.Item label="Quantity Used Deals">
                  {books && countBookDeal(books as Book[], 'Used')}
                </Descriptions.Item>
                <Descriptions.Item label="Quantity Expired Deals">
                  {books && countBookDeal(books as Book[], 'Expired')}
                </Descriptions.Item>
              </DescriptionsConfig>
            </Col>
          </Row>
        </div>
      )}
    </SideBarListFeature>
  );
};

export default UserPage;
