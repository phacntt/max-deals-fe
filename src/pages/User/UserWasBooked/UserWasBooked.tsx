import { Badge, Col, List, Row, Table } from 'antd';
import Item from 'antd/lib/list/Item';
import moment, { Moment } from 'moment';

import React, { FC, useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from 'redux/hooks';
import { selectAuth } from 'redux/slices/auth.slice';
import { selectCurrentUser } from 'redux/slices/current-user.slice';
import { bookService } from 'services';
import { Book } from 'types/Book';
import { Deal } from 'types/Deal';
import { SideBarListFeature } from '../User.style';

const UserWasBooked: FC = () => {
  const dispatch = useAppDispatch();
  const auth = useAppSelector(selectAuth);
  const currentUser = useAppSelector(selectCurrentUser);
  const [books, setBooks] = useState<Book[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      const listBookByUserId = await bookService.getAllBook({
        userId: Number(currentUser.id),
      });

      const filterBooks: Book[] = [];
      // eslint-disable-next-line no-plusplus
      for (let i = 0; i < listBookByUserId.length; i++) {
        if (
          listBookByUserId[i].status === 'Used' ||
          listBookByUserId[i].status === 'Expired'
        ) {
          filterBooks.push(listBookByUserId[i]);
        }
      }
      setBooks(filterBooks);
    };
    fetchData();
  }, []);

  function renderStatus(text: string) {
    if (text === 'Unused') {
      return 'processing';
    }
    if (text === 'Used') {
      return 'success';
    }
    if (text === 'Expired') {
      return 'error';
    }
    return 'default';
  }

  const columns = [
    {
      title: 'Name Deal',
      dataIndex: 'deal',
      render: (record: Deal) => record.name,
    },
    {
      title: 'Discount',
      dataIndex: 'deal',
      render: (record: Deal) => <span> {record.discount} %</span>,
    },
    {
      title: 'Code Verify',
      dataIndex: 'codeVerify',
    },
    {
      title: 'Status',
      dataIndex: 'status',
      render: (text: string) => (
        <Badge
          style={{ whiteSpace: 'nowrap' }}
          status={renderStatus(text)}
          text={text}
        />
      ),
    },
    {
      title: 'Expiry Date',
      dataIndex: 'deal',
      render: (record: Deal) => (
        <div style={{ whiteSpace: 'nowrap' }}>
          {moment(record.expiryDate).format('YYYY-MM-DD')}
        </div>
      ),
    },
    {
      title: 'Store',
      dataIndex: 'deal',

      render: (record: Deal) => (
        <a href={`/store/${record.store.slug}`}>{record.store.name}</a>
      ),
    },
  ];

  return (
    <SideBarListFeature title="HISTORY BOOKED">
      <Table dataSource={books} columns={columns} />
    </SideBarListFeature>
  );
};

export default UserWasBooked;
