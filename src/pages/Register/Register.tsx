import {
  EyeTwoTone,
  EyeInvisibleOutlined,
  ArrowLeftOutlined,
} from '@ant-design/icons';
import { Form, Input, Button, Modal, Result, Divider, Row, Col } from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { getAPIErrorMessage, APIError } from 'apis/api';
import React, { FC, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useAppDispatch } from 'redux/hooks';
import { loginAsync } from 'redux/slices/auth.slice';
import { accountService, userService } from 'services';
import { RegisterPanel } from './Register.style';

type RegisterFormValues = {
  username: string;
  password: string;
  email: string;
  name: string;
  phone: string;
};

type LoginFormValues = {
  username: string;
  password: string;
};

const RegisterPage: FC = () => {
  const dispath = useAppDispatch();
  const navigate = useNavigate();
  const [registerForm] = useForm();
  const [errorMessage, setErrorMessage] = useState('');
  const [inProgress, setInProgress] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [result, setResult] = useState(false);

  const handleRegister = async (formValue: RegisterFormValues) => {
    try {
      setInProgress(true);
      setErrorMessage('');
      const data: RegisterFormValues = {
        name: formValue.name,
        username: formValue.username,
        email: formValue.email,
        password: formValue.password,
        phone: formValue.phone,
      };

      console.log(data);
      const resp = await userService.createUser(data);

      if (resp) {
        setResult(true);
        setIsModalVisible(true);
        return setTimeout(() => navigate('/login'), 3000);
      }

      setErrorMessage('Incorrect username or password');
    } catch (error: unknown) {
      setResult(false);
      setErrorMessage(getAPIErrorMessage(error as APIError));
      setIsModalVisible(true);
    }

    return setInProgress(false);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <RegisterPanel>
      <div
        style={{
          fontSize: 26,
          fontWeight: 'bold',
          textAlign: 'center',
          marginBottom: 20,
          fontFamily: 'initial',
        }}
      >
        REGISTER
      </div>
      <Form
        form={registerForm}
        name="login"
        layout="vertical"
        onFinish={handleRegister}
      >
        <Form.Item
          name="username"
          rules={[{ required: true }]}
          label="Username"
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[{ required: true }]}
          label="Password"
        >
          <Input.Password
            // eslint-disable-next-line react/no-unstable-nested-components
            iconRender={visible =>
              visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
            }
          />
        </Form.Item>
        <Form.Item name="email" rules={[{ required: true }]} label="Email">
          <Input type="email" />
        </Form.Item>
        <Form.Item>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item name="name" rules={[{ required: true }]} label="Name">
                <Input />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="phone"
                rules={[{ required: true }]}
                label="Phone Number"
              >
                <Input />
              </Form.Item>
            </Col>
          </Row>
        </Form.Item>
        <Form.Item>
          <Button block type="primary" htmlType="submit" loading={inProgress}>
            Submit
          </Button>
        </Form.Item>
        {result ? (
          <Modal
            visible={isModalVisible}
            onOk={handleOk}
            onCancel={handleCancel}
            footer={[
              <Button key="back" onClick={handleCancel}>
                Confirm
              </Button>,
            ]}
          >
            <Result
              status="success"
              title="Create Success"
              subTitle="Please wait a second to redirect to Login page"
            />
          </Modal>
        ) : (
          <Modal
            visible={isModalVisible}
            onOk={handleOk}
            onCancel={handleCancel}
            footer={[
              <Button key="back" onClick={handleCancel}>
                Confirm
              </Button>,
            ]}
          >
            <Result
              status="error"
              title="Login Failure"
              subTitle={errorMessage}
            />
          </Modal>
        )}

        <Form.Item>
          <div>
            <Link to="/login">
              <ArrowLeftOutlined /> Previous
            </Link>
          </div>
        </Form.Item>
      </Form>
    </RegisterPanel>
  );
};

export default RegisterPage;
