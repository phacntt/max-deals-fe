import { Card } from 'antd';
import styled from 'styled-components';

export const RegisterPanel = styled(Card)`
  width: 400px;
  margin: 30px auto;
  max-width: 100%;
`;
