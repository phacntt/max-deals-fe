import { Alert, Button, Card, Form, Input } from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { APIError, getAPIErrorMessage } from 'apis/api';
import { LoginPanel } from 'pages/Login/Login.styles';
import React, { FC, useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from 'redux/hooks';
import { loginAsync, selectAuth } from 'redux/slices/auth.slice';
import { selectCurrentUser } from 'redux/slices/current-user.slice';
import { authService } from 'services';

type LoginFormValues = {
  username: string;
  password: string;
};

const LoginAdminPage: FC = () => {
  const dispath = useAppDispatch();
  const auth = useAppSelector(selectAuth);
  const currentUser = useAppSelector(selectCurrentUser);

  const navigate = useNavigate();
  const [loginForm] = useForm();
  const [errorMessage, setErrorMessage] = useState('');
  const [inProgress, setInProgress] = useState(false);

  useEffect(() => {
    if (!auth.isAuthenticated) {
      return;
    }
    console.warn('Authenticated, redirecting...');
    navigate('/');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleLogin = async (formValue: LoginFormValues) => {
    try {
      setInProgress(true);
      setErrorMessage('');

      const resp = await dispath(loginAsync(formValue)).unwrap();
      if (resp) {
        if (resp.role === 'admin') {
          return navigate(`/admin`);
        }
        if (currentUser.role === 'user') {
          return navigate('/login');
        }
        if (resp.role === 'super_admin') {
          return navigate(`/superadmin`);
        }
      }

      setErrorMessage('Incorrect username or password');
    } catch (error: unknown) {
      setErrorMessage(getAPIErrorMessage(error as APIError));
    }

    return setInProgress(false);
  };
  return (
    <LoginPanel>
      <div
        style={{
          fontSize: 26,
          fontWeight: 'bold',
          textAlign: 'center',
          marginBottom: 20,
        }}
      >
        LOGIN WITH ADMIN
      </div>
      <Form
        form={loginForm}
        name="login"
        layout="vertical"
        onFinish={handleLogin}
      >
        <Form.Item
          name="username"
          rules={[{ required: true }]}
          label="Username"
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[{ required: true }]}
          label="Password"
        >
          <Input type="password" />
        </Form.Item>
        <Form.Item>
          <Button block type="primary" htmlType="submit" loading={inProgress}>
            Submit
          </Button>
        </Form.Item>
        <Form.Item>
          {errorMessage && (
            <Alert showIcon message={errorMessage} type="error" closable />
          )}
        </Form.Item>
        <Form.Item style={{ textAlign: 'end' }}>
          <Link to="/login">Login With User</Link>
        </Form.Item>
      </Form>
    </LoginPanel>
  );
};

export default LoginAdminPage;
