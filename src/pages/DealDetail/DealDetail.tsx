import { Row } from 'antd';
import Container from 'components/Container/Container';
import DealDetailLeft from 'components/DealDetailLeft/DealDetailLeft';
import DealDetailRight from 'components/DealDetailRight/DealDetailRight';
import { getIdFromSlug } from 'helpers/getIdFromSlug';
import React, { FC, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { dealService } from 'services';
import { Deal } from 'types/Deal';

const DealDetail: FC = () => {
  const [deal, setDeal] = useState<Deal>();
  const { id } = useParams();
  const idSearch = getIdFromSlug(id);
  useEffect(() => {
    const fetchData = async () => {
      const getDealById = await dealService.getDealById(idSearch as number);
      setDeal(getDealById);
    };
    fetchData();
  }, []);

  return (
    <Container>
      <Row gutter={16}>
        {deal && <DealDetailLeft deal={deal} />}
        {deal && <DealDetailRight id={idSearch as number} deal={deal} />}
      </Row>
    </Container>
  );
};

export default DealDetail;
