import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import {
  Alert,
  Button,
  Card,
  Col,
  Divider,
  Form,
  Input,
  Modal,
  Result,
  Row,
} from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { APIError, getAPIErrorMessage } from 'apis/api';
import React, { FC, useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from 'redux/hooks';
import { loginAsync, logoutAsync, selectAuth } from 'redux/slices/auth.slice';
import { authService } from 'services';
import { LoginPanel } from './Login.styles';

type LoginFormValues = {
  username: string;
  password: string;
};

const LoginPage: FC = () => {
  const dispath = useAppDispatch();
  const auth = useAppSelector(selectAuth);
  const navigate = useNavigate();
  const [loginForm] = useForm();
  const [errorMessage, setErrorMessage] = useState('');
  const [inProgress, setInProgress] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [result, setResult] = useState(false);

  useEffect(() => {
    if (!auth.isAuthenticated) {
      return;
    }
    console.warn('Authenticated, redirecting...');
    navigate('/');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleLogin = async (formValue: LoginFormValues) => {
    try {
      setInProgress(true);
      setErrorMessage('');
      const resp = await dispath(loginAsync(formValue)).unwrap();
      if (resp) {
        if (resp.role !== 'user') {
          setResult(false);
          setIsModalVisible(true);
          dispath(logoutAsync());
          return navigate('/login');
        }

        setResult(true);
        setIsModalVisible(true);
        return setTimeout(() => navigate('/'), 3000);
      }

      setErrorMessage('Incorrect username or password');
    } catch (error: unknown) {
      setResult(false);
      setErrorMessage(getAPIErrorMessage(error as APIError));
      setIsModalVisible(true);
    }

    return setInProgress(false);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  return (
    <LoginPanel>
      <div
        style={{
          fontSize: 26,
          fontWeight: 'bold',
          textAlign: 'center',
          marginBottom: 20,
          fontFamily: 'initial',
        }}
      >
        LOGIN WITH USER
      </div>
      <Form
        form={loginForm}
        name="login"
        layout="vertical"
        onFinish={handleLogin}
      >
        <Form.Item
          name="username"
          rules={[{ required: true }]}
          label="Username"
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[{ required: true }]}
          label="Password"
        >
          <Input.Password
            // eslint-disable-next-line react/no-unstable-nested-components
            iconRender={visible =>
              visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
            }
          />
        </Form.Item>
        <Form.Item>
          <Button block type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
        {result ? (
          <Modal
            visible={isModalVisible}
            onOk={handleOk}
            onCancel={handleCancel}
            footer={[
              <Button key="back" onClick={handleCancel}>
                Confirm
              </Button>,
            ]}
          >
            <Result
              status="success"
              title="Login Success"
              subTitle="Please wait a second to redirect to Home page"
            />
          </Modal>
        ) : (
          <Modal
            visible={isModalVisible}
            onOk={handleOk}
            onCancel={handleCancel}
            footer={[
              <Button key="back" onClick={handleCancel}>
                Confirm
              </Button>,
            ]}
          >
            <Result
              status="error"
              title="Login Failure"
              subTitle={errorMessage}
            />
          </Modal>
        )}

        <Form.Item>
          <Divider orientation="center">OR</Divider>
        </Form.Item>
        <Form.Item>
          <Row>
            <Col span={12}>
              <div>
                Do not have an account? <Link to="/register">Create Here</Link>
              </div>
            </Col>
            <Col span={12}>
              <div style={{ textAlign: 'end' }}>
                <Link to="/login/admin">Login with admin</Link>
              </div>
            </Col>
          </Row>
        </Form.Item>
      </Form>
    </LoginPanel>
  );
};

export default LoginPage;
