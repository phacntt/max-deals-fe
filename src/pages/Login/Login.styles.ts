import { Card } from 'antd';
import styled from 'styled-components';

export const LoginPanel = styled(Card)`
  width: 400px;
  margin: auto;
  max-width: 100%;
`;
