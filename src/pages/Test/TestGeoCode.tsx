/* eslint-disable @typescript-eslint/no-explicit-any */

/**
 * References:
 * - Gecoding with Maps JavaScript API: https://developers.google.com/maps/documentation/javascript/geocoding
 * - Get current user location: https://dev.to/codebucks/how-to-get-user-s-location-in-react-js-1691
 */

import { Col, Row } from 'antd';
import Container from 'components/Container/Container';
import React, { FC, useEffect, useRef, useState } from 'react';
import GoogleMapReact from 'google-map-react';

const API_KEY = 'AIzaSyADfAOFysvcMoDqvYPYN48bZf9wSGONJmU';

interface GeoLocation {
  lat: number;
  lng: number;
}
interface CustomMarkerProps {
  text?: string;
  lat: number;
  lng: number;
}
const CustomMarker: FC<CustomMarkerProps> = () => (
  <img width={32} height={32} src="/marker.png" alt="marker" />
);

const HOCHIMINH_LOCATION = { lat: 10.762622, lng: 106.660172 };

const AddressToLatLong: FC = () => {
  const [address, setAddress] = useState('');
  const [location, setLocation] = useState({
    lat: -1,
    lng: -1,
  });
  const geocoderRef = useRef<any>(null);
  const [mapCenter, setMapCenter] = useState<GeoLocation>();

  useEffect(() => {
    if (navigator.geolocation) {
      console.log('get current user location');
      const options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0,
      };
      const updateMapCenter = (position: any) => {
        const crd = position.coords;

        console.log('Your current position is:');
        console.log(`Latitude : ${crd.latitude}`);
        console.log(`Longitude: ${crd.longitude}`);
        console.log(`More or less ${crd.accuracy} meters.`);

        setMapCenter({
          lat: crd.latitude,
          lng: crd.longitude,
        });
      };
      navigator.permissions
        .query({ name: 'geolocation' })
        .then(function (result) {
          if (result.state === 'granted') {
            console.log(result.state);
            navigator.geolocation.getCurrentPosition(updateMapCenter);
          } else if (result.state === 'prompt') {
            navigator.geolocation.getCurrentPosition(
              updateMapCenter,
              err => {
                console.warn(`ERROR(${err.code}): ${err.message}`);
              },
              options,
            );
          } else if (result.state === 'denied') {
            // show message that user has denied location permission
          }
        });
    }
  }, []);

  const setValueInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    setAddress(e.target.value);
  };

  const getValueInput = () => {
    geocoderRef.current.geocode(
      { address, region: 'vn' },
      (results: any[], status: string) => {
        if (status === 'OK') {
          const geolocation = {
            lat: results[0].geometry.location.lat(),
            lng: results[0].geometry.location.lng(),
          };
          setLocation(geolocation);
          setMapCenter(geolocation);
        } else {
          console.error(
            `Geocode was not successful for the following reason: ${status}`,
          );
        }
      },
    );
  };

  const handleGoogleMapApiLoaded = (_map: any, maps: any) => {
    geocoderRef.current = new maps.Geocoder();
  };

  const handleClickOnMap = (clickValue: GoogleMapReact.ClickEventValue) => {
    console.log('On Map Click', clickValue);
    setLocation({
      lat: clickValue.lat,
      lng: clickValue.lng,
    });
  };

  return (
    <Container>
      <Row>
        <Col span={12}>
          <input type="text" onChange={setValueInput} />
          <input type="button" value="Get" onClick={getValueInput} />
        </Col>
      </Row>
      <Row>{`${location.lat}----${location.lng}`}</Row>
      <div style={{ height: 500, width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: API_KEY }}
          defaultCenter={HOCHIMINH_LOCATION}
          defaultZoom={17}
          center={mapCenter}
          yesIWantToUseGoogleMapApiInternals
          onGoogleApiLoaded={({ map, maps }) =>
            handleGoogleMapApiLoaded(map, maps)
          }
          onClick={handleClickOnMap}
        >
          {location.lat !== -1 && (
            <CustomMarker
              lat={location.lat}
              lng={location.lng}
              text="My Marker"
            />
          )}
        </GoogleMapReact>
      </div>
    </Container>
  );
};

export default AddressToLatLong;
