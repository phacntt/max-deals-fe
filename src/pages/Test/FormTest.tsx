import {
  Form,
  Row,
  Col,
  Input,
  Select,
  Button,
  DatePicker,
  InputNumber,
  Alert,
  Upload,
} from 'antd';
import { useForm } from 'antd/lib/form/Form';
import TextArea from 'antd/lib/input/TextArea';
import { getAPIErrorMessage, APIError } from 'apis/api';
import { AddDealRequestBody } from 'apis/deal.api';
import FormUpload from 'components/FormUpload/FormUpload';
import React, { FC, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { categoryService, dealService, storeService } from 'services';
import { Category } from 'types/Category';
import { File } from 'types/File';
import { Store } from 'types/Store';
import Uploader from './Uploader';

const FormTest: FC = () => {
  const [images, setImages] = useState<File[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      const dataImg = [
        {
          id: 20,
          name: '1648695932012-365480248-download.jpeg',
          path: 'uploads/1648695932012-365480248-download.jpeg',
          url: 'http://localhost:3000/api/file/uploads/1648695932012-365480248-download.jpeg',
          size: 10962,
          mimeType: 'image/jpeg',
          entityId: 15,
          entityType: 'DealEntity',
        },
        {
          id: 21,
          name: '1648695932013-9250579-download.png',
          path: 'uploads/1648695932013-9250579-download.png',
          url: 'http://localhost:3000/api/file/uploads/1648695932013-9250579-download.png',
          size: 3893,
          mimeType: 'image/png',
          entityId: 15,
          entityType: 'DealEntity',
        },
        {
          id: 22,
          name: '1648695932014-108671816-manuel-cosentino-n--CMLApjfI-unsplash.jpg',
          path: 'uploads/1648695932014-108671816-manuel-cosentino-n--CMLApjfI-unsplash.jpg',
          url: 'http://localhost:3000/api/file/uploads/1648695932014-108671816-manuel-cosentino-n--CMLApjfI-unsplash.jpg',
          size: 2214513,
          mimeType: 'image/jpeg',
          entityId: 15,
          entityType: 'DealEntity',
        },
      ];
      // setImages(dataImg);
    };
    fetchData();
    return () => {
      console.log('Unmount');
    };
  }, []);

  const handleUploadFile = (newImages: File[]) => {
    setImages(newImages);
    console.log('handleUploadFile', newImages);
  };

  return <Uploader value={images} onChange={handleUploadFile} />;
};

export default FormTest;
