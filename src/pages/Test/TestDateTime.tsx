import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import { dealService } from 'services';
import { Deal } from 'types/Deal';

const TestDateTime: FC = () => {
  const [deals, setDeals] = useState<Deal[]>([]);
  useEffect(() => {
    const fetchData = async () => {
      const listDeals = await dealService.getAllDeal({
        expiryDate: moment().toISOString(),
      });
      setDeals(listDeals);
    };
    fetchData();
  }, []);

  const a = deals.map(deal => deal.expiryDate);
  console.log('bbv', deals);
  const timeCurrent = moment();

  const b = a.filter(date => moment(date).diff(moment(), 'days'));
  const c = moment('2022-05-13T03:30:19.234Z').diff(timeCurrent, 'days');
  console.log('bbvzzz', c);

  const timeDB = moment('2022-05-05T15:20:37.395Z');
  console.log('avc', timeDB.diff(timeCurrent, 'days'));

  return <h1>OI</h1>;
};
export default TestDateTime;
