import { Row } from 'antd';
import Container from 'components/Container/Container';
import StoreDetailBody from 'components/StoreDetailBody/StoreDetailBody';
import StoreDetailHeader from 'components/StoreDetailHeader/StoreDetailHeader';
import { getIdFromSlug } from 'helpers/getIdFromSlug';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { dealService, fileService, storeService } from 'services';
import { Deal } from 'types/Deal';
import { File } from 'types/File';
import { Store } from 'types/Store';

interface GeoLocation {
  lat: number;
  lng: number;
}

const StoreDetail: FC = () => {
  const [deals, setDeals] = useState<Deal[]>([]);
  const [dealsRelate, setDealsRelate] = useState<Deal[]>([]);
  const [store, setStore] = useState<Store>();
  const [idStoresRelate, setIdStoresRelate] = useState<number[]>([]);
  const [idCategory, setIdCategory] = useState<number>();
  const [location, setLocation] = useState<GeoLocation>();
  const [image, setImage] = useState<File>();
  const { id } = useParams();

  const idStore = getIdFromSlug(id);

  useEffect(() => {
    const fetchData = async () => {
      const storeInfo = await storeService.getStoreById(idStore as number);
      const located: GeoLocation = {
        lat: storeInfo.lat,
        lng: storeInfo.lng,
      };
      setLocation(located);
      setStore(storeInfo);
      const listDealByStore = await dealService.getAllDeal({
        id: 'DESC',
        storeId: idStore,
        expiryDate: moment().toISOString(),
      });
      setDeals(listDealByStore);
      const storeImage = await fileService.getFileById(
        idStore as number,
        'StoreEntity',
      );
      setImage(storeImage[0]);
    };
    fetchData();
  }, [deals]);

  useEffect(() => {
    const fetchData = async () => {
      if (deals) {
        const listCategoryId = deals.map(deal => deal.categoryId);
        const categoryIds = listCategoryId.filter(
          (item, index) => listCategoryId.indexOf(item) === index,
        );
        setIdCategory(
          categoryIds[Math.floor(Math.random() * categoryIds.length)],
        );
      }
    };
    fetchData();
  }, [deals]);

  useEffect(() => {
    const fetchData = async () => {
      if (idCategory) {
        const listDealByCategory = await dealService.getAllDeal({
          id: 'DESC',
          categoryId: idCategory,
          expiryDate: moment().toISOString(),
        });

        const listDealRecommend = listDealByCategory.filter(
          deal => deal.storeId !== Number(id),
        );

        const listIdStore = listDealByCategory.map(deal => deal.storeId);
        const listIdStoreSameCate = listIdStore.filter(
          (item, index) =>
            listIdStore.indexOf(item) === index && item !== Number(id),
        );

        setDealsRelate(listDealRecommend);
        setIdStoresRelate(listIdStoreSameCate);
      }
    };
    fetchData();
  }, [idCategory]);

  return (
    <Container>
      {image && (
        <StoreDetailHeader
          deals={deals}
          image={image as File}
          store={store as Store}
        />
      )}
      {image && (
        <StoreDetailBody
          deals={deals}
          store={store as Store}
          dealsRelate={dealsRelate}
          idStoreRelate={idStoresRelate}
          location={location as GeoLocation}
          image={image as File}
        />
      )}
    </Container>
  );
};

export default StoreDetail;
