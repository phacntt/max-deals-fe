import { QuestionCircleOutlined, WarningOutlined } from '@ant-design/icons';
import {
  Alert,
  Badge,
  Button,
  Card,
  Col,
  Descriptions,
  Divider,
  Popover,
  Result,
  Row,
} from 'antd';
import { AddBookRequestBody } from 'apis/book.api';
import Container from 'components/Container/Container';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { Account } from 'services/account/account';
import { Deal } from 'types/Deal';
import { DescriptionConfig } from './BookingResult.style';

const BookingResult: FC = () => {
  const { state }: any = useLocation();

  const content = (
    <div style={{ maxWidth: 500 }}>
      <Alert
        message="Warning"
        description={[
          <div>
            1. When paying please submit a discount code to be applied in-store{' '}
            <br />
            2. The discount code must be used before the expiration date or
            before the date of placing the discount code <br />
          </div>,
        ]}
        type="warning"
      />
    </div>
  );

  return (
    <Container>
      <Row>
        <Col span={24}>
          <Card>
            {console.log(state)}
            <Result
              status="success"
              title={[
                <div>
                  STEAL DEAL IS SUCCESSFUL{' '}
                  <span>
                    <Popover content={content} placement="bottomRight">
                      <QuestionCircleOutlined />
                    </Popover>
                  </span>
                </div>,
              ]}
              subTitle={[
                <div>
                  * Hover in the icon <QuestionCircleOutlined /> for note when
                  using the discount code and check the details below
                </div>,
              ]}
              extra={[
                <div>
                  <DescriptionConfig title="INSIGHTS" bordered>
                    <Descriptions.Item label="Name Deal">
                      {state.deal.name}
                    </Descriptions.Item>
                    <Descriptions.Item label="Discount">
                      {state.deal.discount} %
                    </Descriptions.Item>
                    <Descriptions.Item label="Category">
                      {state.deal.category.name}
                    </Descriptions.Item>
                    <Descriptions.Item label="Date Steal">
                      {moment(state.book.dateBook).format('YYYY-MM-DD')}
                    </Descriptions.Item>
                    <Descriptions.Item label="Usage Time">
                      {moment(state.book.dateExpiryBook).format('YYYY-MM-DD')}
                    </Descriptions.Item>
                    <Descriptions.Item label="Store">
                      {state.deal.store.name}
                    </Descriptions.Item>
                    <Descriptions.Item label="Address" span={2}>
                      {state.deal.store.fullAddress}
                    </Descriptions.Item>
                    <Descriptions.Item label="Contact Phone">
                      {state.deal.store.phone}
                    </Descriptions.Item>

                    <Descriptions.Item label="Code Verify">
                      <div style={{ fontSize: 20, fontWeight: 700 }}>
                        {state.book.codeVerify}
                      </div>
                    </Descriptions.Item>
                  </DescriptionConfig>
                  <div>
                    <Link
                      to={`/deals/${state.deal.id}`}
                      style={{ margin: '0 20px 0 20px' }}
                    >
                      <Button type="primary">Go Back</Button>
                    </Link>

                    <Link
                      to="/user/booking"
                      style={{ margin: '0 20px 0 20px' }}
                    >
                      <Button>Check Deal</Button>
                    </Link>
                  </div>
                </div>,
              ]}
            />
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default BookingResult;
