import { Descriptions, Result } from 'antd';
import styled from 'styled-components';

export const ResultConfig = styled(Result)``;

export const DescriptionConfig = styled(Descriptions)`
  margin-bottom: 30px;

  .ant-descriptions-title {
    font-size: 24px;
  }

  .ant-descriptions-item-label {
    font-weight: 600;
  }
`;
