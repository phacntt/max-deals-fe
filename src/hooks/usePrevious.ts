import { useRef, useEffect } from 'react';

export function usePrevious<T = unknown>(value: T) {
  const ref = useRef<T>();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}
