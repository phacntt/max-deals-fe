export interface Category {
  id: string;
  name: string;
  slug: string;
  parentId?: number;
}
