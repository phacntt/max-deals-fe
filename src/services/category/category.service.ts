import {
  AddCategoryRequestBody,
  CategoryAPI,
  QueryGetCategory,
  UpdateCategoryRequestBody,
} from 'apis/category.api';
import { Category } from 'types/Category';

export class CategoryService {
  // eslint-disable-next-line no-useless-constructor
  constructor(private categoryAPI: CategoryAPI) {}

  async getAllCategory(query?: QueryGetCategory): Promise<Category[]> {
    const resp = await this.categoryAPI.getAllCategory(query);
    return resp.data;
  }

  async getCategoryById(id: number): Promise<Category> {
    const resp = await this.categoryAPI.getCategoryById(id);
    return resp.data;
  }

  async createCategory(data: AddCategoryRequestBody): Promise<Category> {
    const resp = await this.categoryAPI.addCategory(data);
    return resp.data;
  }

  async updateCategory(
    id: number,
    data: UpdateCategoryRequestBody,
  ): Promise<Category> {
    const resp = await this.categoryAPI.updateCategory(id, data);
    return resp.data;
  }

  async deleteCategory(id: number): Promise<Category> {
    const resp = await this.categoryAPI.deletetCategory(id);
    return resp.data;
  }
}
