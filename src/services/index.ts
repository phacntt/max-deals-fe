import {
  accountAPI,
  bookAPI,
  categoryAPI,
  dealAPI,
  fileAPI,
  provinceAPI,
  storeAPI,
  userAPI,
} from 'apis';
import { AccountService } from './account/account.service';
import { CookieAuthService } from './auth/auth.service';
import { BookService } from './book/book.service';
import { CategoryService } from './category/category.service';
import { DealService } from './deal/deal.service';
import { FileService } from './file/file.service';
import { ProvinceService } from './province/province.service';
import { LocalStorageService } from './storage.service';
import { StoreService } from './store/store.service';
import { UserService } from './user/user.service';

export const localStorageService = new LocalStorageService();
export const authService = new CookieAuthService();
export const accountService = new AccountService(accountAPI);
export const dealService = new DealService(dealAPI);
export const categoryService = new CategoryService(categoryAPI);
export const storeService = new StoreService(storeAPI);
export const fileService = new FileService(fileAPI);
export const provinceService = new ProvinceService(provinceAPI);
export const userService = new UserService(userAPI);
export const bookService = new BookService(bookAPI);
