import { AccountAPI } from 'apis/account.api';
import {
  AddUserRequestBody,
  QueryGetUser,
  UpdateUserRequestBody,
  UserAPI,
} from 'apis/user.api';

export class UserService {
  // eslint-disable-next-line no-useless-constructor
  constructor(private userAPI: UserAPI) {}

  async getUsers(optional?: QueryGetUser) {
    const resp = await this.userAPI.getUsers(optional);
    return resp.data;
  }

  async getUserById(id: number) {
    const resp = await this.userAPI.getUserById(id);
    return resp.data;
  }

  async createUser(data: AddUserRequestBody) {
    const resp = await this.userAPI.register(data);
    return resp.data;
  }

  async updateUser(id: number, data: UpdateUserRequestBody) {
    const resp = await this.userAPI.updateUser(id, data);
    return resp.data;
  }

  async deleteUser(id: number) {
    const resp = await this.userAPI.deleteUser(id);
    return resp.data;
  }
}
