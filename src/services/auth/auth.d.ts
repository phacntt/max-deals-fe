export interface AuthService {
  isAuthenticated(): boolean;
  getAccessToken(): string | undefined;
  loginWithPassword(username: string, password: string): Promise<boolean>;
}

export interface Account {
  role: string;
  id: string;
  email: string;
  name: string;
}
