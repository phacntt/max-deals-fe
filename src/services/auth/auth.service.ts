import { accountAPI } from 'apis';
import { getCookie } from 'helpers/cookie';
import { Account, AuthService } from './auth';

const AUTH_TOKEN_KEY = 'access_token';

export class CookieAuthService implements AuthService {
  protected accessToken?: string;

  protected accountInfo?: Account;

  constructor() {
    this.syncFromStorage();
  }

  isAuthenticated(): boolean {
    return !!this.accessToken;
  }

  async loginWithPassword(username: string, password: string) {
    try {
      const loginResp = await accountAPI.login(username, password);
      this.accountInfo = loginResp.data;
      this.syncFromStorage();
      return true;
    } catch (error) {
      return false;
    }
  }

  async logout() {
    await accountAPI.logout();
    this.invalidSession();
  }

  invalidSession() {
    this.accessToken = undefined;
    this.accountInfo = undefined;
  }

  saveAuthToken(accessToken: string) {
    this.accessToken = accessToken;
  }

  getAccessToken() {
    if (!this.accessToken) {
      this.syncFromStorage();
    }

    return this.accessToken;
  }

  getAccount() {
    return this.accountInfo;
  }

  // eslint-disable-next-line class-methods-use-this
  protected readAccessTokenFromStorage() {
    return getCookie(AUTH_TOKEN_KEY);
  }

  private syncFromStorage() {
    const accessToken = this.readAccessTokenFromStorage();
    if (accessToken) {
      this.accessToken = accessToken;
    }
  }
}
