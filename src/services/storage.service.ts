interface KeyValueStorage {
  get<T = unknown>(key: string): T | string | null;
  set<T = unknown>(key: string, value: T): void;
}

export class LocalStorageService implements KeyValueStorage {
  private localStorage: Storage;

  constructor() {
    this.localStorage = window.localStorage;
  }

  get<T = unknown>(key: string): T | string | null {
    const item = this.localStorage.getItem(key);
    if (item === null) {
      return null;
    }

    try {
      return JSON.parse(item) as T;
    } catch (error) {
      return item;
    }
  }

  set<T = unknown>(key: string, value: T): void {
    if (typeof value === 'string') {
      return this.localStorage.setItem(key, value);
    }

    return this.localStorage.setItem(key, JSON.stringify(value));
  }
}
