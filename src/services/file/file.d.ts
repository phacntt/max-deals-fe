import { FileVariants } from 'types/FileVariants';

export interface File {
  id: number;
  name: string;
  path: string;
  url: string;
  size: number;
  mimeType: string;
  entityId: number;
  entityType: string;
  fileVariants: FileVariants[];
}
