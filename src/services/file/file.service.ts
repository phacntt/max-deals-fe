import { DealAPI, AddDealRequestBody } from 'apis/deal.api';
import { EntityType, FileAPI, FileRequestBody } from 'apis/file.api';
import { File } from './file';

export class FileService {
  // eslint-disable-next-line no-useless-constructor
  constructor(private fileAPI: FileAPI) {}

  async checkFile(data: FileRequestBody): Promise<File[]> {
    const resp = await this.fileAPI.checkFile(data);
    return resp.data;
  }

  async getFileById(id: number, typeEntity: EntityType): Promise<File[]> {
    const resp = await this.fileAPI.getFileById(id, typeEntity);
    return resp.data;
  }

  async updateFile(fileIds: number[], dealId: number) {
    const resp = await this.fileAPI.updateFile(fileIds, dealId);
    return resp.data;
  }

  async deleteFile(fileIds: number[]) {
    const resp = await this.fileAPI.deleteFile(fileIds);
    return resp.data;
  }
}
