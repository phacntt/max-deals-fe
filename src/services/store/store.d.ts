export interface Store {
  id: number;
  name: string;
  slug: string;
  address: string;
  location: string;
}
