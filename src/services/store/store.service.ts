import {
  AddStoreRequestBody,
  StoreAPI,
  UpdateStoreRequestBody,
} from 'apis/store.api';
import { Store } from 'types/Store';

export class StoreService {
  // eslint-disable-next-line no-useless-constructor
  constructor(private storeAPI: StoreAPI) {}

  async getAllStore(): Promise<Store[]> {
    const resp = await this.storeAPI.getAllStore();
    return resp.data;
  }

  async getStoreById(id: number): Promise<Store> {
    const resp = await this.storeAPI.getStoreById(id);
    return resp.data;
  }

  async addStore(data: AddStoreRequestBody): Promise<Store> {
    const resp = await this.storeAPI.addStore(data);
    return resp.data;
  }

  async updateStore(
    id: number,
    data: UpdateStoreRequestBody,
    imageIds: number[],
  ): Promise<Store> {
    const resp = await this.storeAPI.updateStore(id, data, imageIds);
    return resp.data;
  }

  async deleteStore(id: number): Promise<Store> {
    const resp = await this.storeAPI.deleteStore(id);
    return resp.data;
  }
}
