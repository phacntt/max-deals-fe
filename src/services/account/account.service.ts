import { AccountAPI } from 'apis/account.api';

export class AccountService {
  // eslint-disable-next-line no-useless-constructor
  constructor(private accountAPI: AccountAPI) {}

  async getMe() {
    const resp = await this.accountAPI.getMe();
    return resp.data;
  }
}
