export interface Account {
  id: string;
  name: string;
  username: string;
  email: string;
  role: string;
  storeId: number;
  avatarURL?: string;
  authMethod: 'password' | '3rd';
  authMethod3rd?: 'facebook' | 'google';
}
