import {
  AddBookRequestBody,
  BookAPI,
  QueryGetBook,
  UpdateBookRequestBody,
} from 'apis/book.api';

import { Book } from 'types/Book';

export class BookService {
  // eslint-disable-next-line no-useless-constructor
  constructor(private bookAPI: BookAPI) {}

  async getAllBook(query?: QueryGetBook): Promise<Book[]> {
    const resp = await this.bookAPI.getAllBook(query);
    return resp.data;
  }

  async getBookById(id: number): Promise<Book> {
    const resp = await this.bookAPI.getBookById(id);
    return resp.data;
  }

  async createBook(data: AddBookRequestBody): Promise<Book> {
    const resp = await this.bookAPI.addBook(data);
    return resp.data;
  }

  async updatBook(id: number, data: UpdateBookRequestBody): Promise<Book> {
    const resp = await this.bookAPI.updateBook(id, data);
    return resp.data;
  }

  async deleteBook(id: number): Promise<Book> {
    const resp = await this.bookAPI.deleteBook(id);
    return resp.data;
  }
}
