import { ProvinceAPI } from 'apis/province.api';
import { Province } from 'types/Province';

export class ProvinceService {
  // eslint-disable-next-line no-useless-constructor
  constructor(private provinceAPI: ProvinceAPI) {}

  async getAllProvince(): Promise<Province[]> {
    const resp = await this.provinceAPI.getAllProvince();
    return resp.data;
  }

  async getProvinceById(provinceId: string): Promise<Province> {
    const resp = await this.provinceAPI.getProvinceById(provinceId);
    return resp.data;
  }
}
