import {
  DealAPI,
  AddDealRequestBody,
  QueryGetDeal,
  UpdateDealRequestBody,
} from 'apis/deal.api';
import { Deal } from 'types/Deal';

export class DealService {
  // eslint-disable-next-line no-useless-constructor
  constructor(private dealAPI: DealAPI) {}

  async getDeal(): Promise<Deal[]> {
    const resp = await this.dealAPI.getDeal();
    return resp.data;
  }

  async getAllDeal(optional?: QueryGetDeal): Promise<Deal[]> {
    const resp = await this.dealAPI.getAllDeal(optional);
    return resp.data;
  }

  async addDeal(dealData: AddDealRequestBody) {
    const resp = await this.dealAPI.addDeal(dealData);
    return resp.data;
  }

  async getDealById(dealId: number) {
    const resp = await this.dealAPI.getDealById(dealId);
    return resp.data;
  }

  async updateDeal(
    dealId: number,
    data: UpdateDealRequestBody,
    imageIds: number[],
  ) {
    const resp = await this.dealAPI.updateDeal(dealId, data, imageIds);
    return resp.data;
  }

  async deleteDeal(id: number): Promise<Deal> {
    const resp = await this.dealAPI.deleteDeal(id);
    return resp.data;
  }
}
