export interface Deal {
  id: string;
  name: string;
  slug: string;
  img: string;
  expiryDate: Date;
  quantity: number;
  description: string;
  discount: number;
  category_id: number;
  store_id: number;
  categoryId: number;
  storeId: number;
}
