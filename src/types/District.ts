import { Province } from './Province';
import { Ward } from './Ward';

export interface District {
  code: string;
  name: string;
  nameEn: string;
  fullName: string;
  fullNameEn: string;
  codeName: string;
  wards?: Ward[];
}
