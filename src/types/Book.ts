import { Deal } from './Deal';
import { User } from './User';

export interface Book {
  id: number;
  dateBook: Date;
  codeVerify: string;
  status: string;
  user: User;
  deal: Deal;
}
