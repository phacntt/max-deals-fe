import { File } from './File';

export interface Store {
  id: number;
  name: string;
  slug: string;
  description: string;
  phone: string;
  province: string;
  district: string;
  ward: string;
  street: string;
  fullAddress: string;
  lat: number;
  lng: number;
  images: File[];
  createdAt: Date;
  distance: number;
}
