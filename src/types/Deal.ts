import { Category } from './Category';
import { File } from './File';
import { Store } from './Store';

export interface Deal {
  id: number;
  name: string;
  slug: string;
  expiryDate: Date;
  quantity: number;
  description: string;
  discount: number;
  category_id: number;
  store_id: number;
  categoryId: number;
  storeId: number;
  store: Store;
  category: Category;
  images: File[];
}
