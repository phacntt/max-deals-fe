import slugify from 'slugify';

export const setSlugify = (text: string) => {
  return slugify(text, {
    replacement: '-',
    lower: true,
    remove: undefined,
    locale: 'vi',
    trim: true,
  });
};
