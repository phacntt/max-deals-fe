export const sleepPromise = (ms: number) =>
  new Promise(resolve => {
    setTimeout(resolve, ms);
  });
