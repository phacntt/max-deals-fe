export const getIdFromSlug = (params: string | undefined) => {
  if (params) {
    return (
      Number((params as string).slice((params as string).lastIndexOf('-'))) * -1
    );
  }
  return undefined;
};
