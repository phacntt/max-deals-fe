import { bookService } from 'services';

export const updateBooking = async (idBook: number, status: string) => {
  if (status === 'Unused') {
    await bookService.updatBook(idBook, { status: 'Expired' });
  }
};
