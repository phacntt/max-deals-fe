import { Spin } from 'antd';
import AppLayout from 'layouts/AppLayout';
import SidebarLayout from 'layouts/SidebarLayout';
import SidebarLayoutSAdmin from 'layouts/SidebarLayoutSAdmin';
import CategoryPage from 'pages/Category/Category';
import DealDetail from 'pages/DealDetail/DealDetail';

import HomePage from 'pages/Home/Home';
import LoginAdminPage from 'pages/LoginAdmin/LoginAdmin';
import React, { lazy, Suspense } from 'react';
import {
  BrowserRouter,
  Route,
  Routes,
  useNavigate,
  useParams,
} from 'react-router-dom';
import FormTest from 'pages/Test/FormTest';

import TestGeoCode from 'pages/Test/TestGeoCode';

import TestDateTime from 'pages/Test/TestDateTime';
import StoreDetail from 'pages/StoreDetail/StoreDetail';
import RegisterPage from 'pages/Register/Register';
import UserLayout from 'layouts/UserLayout';
import UserPage from 'pages/User/UserInfo/UserInfo';

import BookingResult from 'pages/BookingResult/BookingResult';
import UserBooking from 'pages/User/UserBooking/UserBooking';
import UserWasBooked from 'pages/User/UserWasBooked/UserWasBooked';
import { useAppSelector } from 'redux/hooks';
import { selectAuth } from 'redux/slices/auth.slice';
import { selectCurrentUser } from 'redux/slices/current-user.slice';

import DealAddAdmin from 'pages/Admin/DealAdmin/DealAddAdmin/DealAddAdmin';
import DealAddSAdmin from 'pages/SuperAdmin/DealSAdmin/DealAddSAdmin/DealAddSAdmin';
import DealDetailAdmin from 'pages/Admin/DealAdmin/DealDetailAdmin/DealDetailAdmin';
import BookManageAdmin from 'pages/Admin/BookAdmin/BookManageAdmin/BookManageAdmin';
import CheckBookAdmin from 'pages/Admin/BookAdmin/CheckBookAdmin/CheckBookAdmin';
import UserUpdate from 'pages/User/UserUpdate/UserUpdate';
import SidebarLayoutAdmin from 'layouts/SidebarLayoutAdmin';
import HomeAdmin from 'pages/Admin/HomeAdmin/HomeAdmin';
import DealManageAdmin from 'pages/Admin/DealAdmin/DealManageAdmin/DealManageAdmin';
import UserManage from 'pages/SuperAdmin/UserAdmin/ManageUser/UserManage';
import UserDetail from 'pages/SuperAdmin/UserAdmin/DetailUser/UserDetail';
import BookDetailAdmin from 'pages/Admin/BookAdmin/BookDetailAdmin/BookDetailAdmin';
import CategoryManageSAdmin from 'pages/SuperAdmin/CategorySAdmin/CategoryManageSAdmin/CategoryManageSAdmin';
import CategoryAddSAdmin from 'pages/SuperAdmin/CategorySAdmin/CategoryAddSAdmin/CategoryAddSAdmin';
import CategoryDetailSAdmin from 'pages/SuperAdmin/CategorySAdmin/CategoryDetailSAdmin/CategoryDetailSAdmin';
import DealManageSAdmin from 'pages/SuperAdmin/DealSAdmin/DealManageSAdmin/DealManageSAdmin';
import DealDetailSAdmin from 'pages/SuperAdmin/DealSAdmin/DealDetailSAdmin/DealDetailSAdmin';
import StoreManageSAdmin from 'pages/SuperAdmin/StoreSAdmin/StoreManageSAdmin/StoreManageSAdmin';
import StoreAddSAdmin from 'pages/SuperAdmin/StoreSAdmin/StoreAddSAdmin/StoreAddSAdmin';
import StoreDetailSAdmin from 'pages/SuperAdmin/StoreSAdmin/StoreDetailSAdmin/StoreDetailSAdmin';
import InfomationStoreAdmin from 'pages/Admin/InfomationStoreAdmin/InfomationStoreAdmin';
import StoreDetailAdmin from 'pages/Admin/StoreDetailAdmin/StoreDetailAdmin';
import HomeSAdmin from 'pages/SuperAdmin/HomeSAdmin/HomeSAdmin';

const LoginPage = lazy(() => import('pages/Login/Login'));

export default function AppRoutes() {
  const auth = useAppSelector(selectAuth);
  const currentUser = useAppSelector(selectCurrentUser);
  const isLogin = auth.isAuthenticated;
  const { role, id } = currentUser;
  console.log(`${auth.account?.id}====${currentUser.id}`);
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<SidebarLayout />}>
          <Route index element={<HomePage />} />
        </Route>

        <Route path="/login" element={<AppLayout />}>
          <Route
            index
            element={
              <Suspense fallback={<Spin />}>
                <LoginPage />
              </Suspense>
            }
          />
        </Route>

        <Route path="/register" element={<AppLayout />}>
          <Route
            index
            element={
              <Suspense fallback={<Spin />}>
                <RegisterPage />
              </Suspense>
            }
          />
        </Route>

        <Route path="/login/admin" element={<AppLayout />}>
          <Route
            index
            element={
              <Suspense fallback={<Spin />}>
                <LoginAdminPage />
              </Suspense>
            }
          />
        </Route>

        {isLogin && (
          <Route path="/user" element={<UserLayout />}>
            <Route
              index
              element={
                <Suspense fallback={<Spin />}>
                  <UserPage />
                </Suspense>
              }
            />
            <Route
              path="booking"
              element={
                <Suspense fallback={<Spin />}>
                  <UserBooking />
                </Suspense>
              }
            />
            <Route
              path="history-booking"
              element={
                <Suspense fallback={<Spin />}>
                  <UserWasBooked />
                </Suspense>
              }
            />
            <Route
              path="update-info"
              element={
                <Suspense fallback={<Spin />}>
                  <UserUpdate />
                </Suspense>
              }
            />
          </Route>
        )}

        <Route path="/:collectionName" element={<AppLayout />}>
          <Route index element={<CategoryPage />} />
        </Route>

        <Route path="/book/result" element={<AppLayout />}>
          <Route index element={<BookingResult />} />
        </Route>

        <Route path="/deals" element={<AppLayout />}>
          <Route index element={<CategoryPage />} />
          <Route path="nearme" element={<CategoryPage />} />

          <Route path="category/:id" element={<CategoryPage />} />
          <Route path=":id" element={<DealDetail />} />
        </Route>

        <Route path="/store/:id" element={<AppLayout />}>
          <Route index element={<StoreDetail />} />
        </Route>

        {isLogin && role === 'admin' ? (
          <Route path="/admin" element={<SidebarLayoutAdmin />}>
            <Route index element={<HomeAdmin />} />

            <Route path="dealmanage" element={<DealManageAdmin />} />
            <Route path="dealmanage/add" element={<DealAddAdmin />} />
            <Route path="dealmanage/:dealId" element={<DealDetailAdmin />} />

            <Route path="bookmanage" element={<BookManageAdmin />} />
            <Route path="bookmanage/checkbook" element={<CheckBookAdmin />} />
            <Route path="bookmanage/:bookId" element={<BookDetailAdmin />} />

            <Route
              path="infomation/:storeId"
              element={<InfomationStoreAdmin />}
            />
            <Route path="storedetail/:storeId" element={<StoreDetailAdmin />} />
          </Route>
        ) : (
          <Route
            index
            element={
              <Suspense fallback={<Spin />}>
                <LoginPage />
              </Suspense>
            }
          />
        )}

        {isLogin && role === 'super_admin' ? (
          <Route path="/superadmin" element={<SidebarLayoutSAdmin />}>
            <Route index element={<HomeSAdmin />} />

            <Route path="categorymanage" element={<CategoryManageSAdmin />} />
            <Route path="categorymanage/add" element={<CategoryAddSAdmin />} />
            <Route
              path="categorymanage/:categoryId"
              element={<CategoryDetailSAdmin />}
            />

            <Route path="dealmanage" element={<DealManageSAdmin />} />
            <Route path="dealmanage/add" element={<DealAddSAdmin />} />
            <Route path="dealmanage/:dealId" element={<DealDetailSAdmin />} />

            <Route path="storemanage" element={<StoreManageSAdmin />} />
            <Route path="storemanage/add" element={<StoreAddSAdmin />} />
            <Route
              path="storemanage/:storeId"
              element={<StoreDetailSAdmin />}
            />

            <Route path="usermanage" element={<UserManage />} />
            <Route path="usermanage/:userId" element={<UserDetail />} />
          </Route>
        ) : (
          <Route
            index
            element={
              <Suspense fallback={<Spin />}>
                <LoginPage />
              </Suspense>
            }
          />
        )}

        <Route path="/test" element={<SidebarLayoutSAdmin />}>
          <Route index element={<TestGeoCode />} />
          <Route path="1" element={<TestDateTime />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}
