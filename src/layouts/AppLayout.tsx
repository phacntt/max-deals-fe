import { VerticalAlignTopOutlined } from '@ant-design/icons';
import { BackTop, Layout } from 'antd';
import Container from 'components/Container/Container';
import AppFooter from 'components/Footer/Footer';
import AppHeader from 'components/Header/Header';
import React, { FC } from 'react';
import { Outlet } from 'react-router-dom';

const AppLayout: FC = () => {
  return (
    <Layout style={{ minHeight: '100vh' }}>
      <AppHeader fixed />
      <Container
        style={{ flex: '1 auto', marginTop: 64, paddingTop: 16 }}
        direction="vertical"
        flex
      >
        <Outlet />
      </Container>
      <AppFooter />
      <BackTop>
        <div
          style={{
            height: 50,
            width: 50,
            backgroundColor: '#e73948',
            borderRadius: '50%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            textAlign: 'center',
          }}
        >
          <VerticalAlignTopOutlined
            style={{ fontSize: 30, color: 'white', fontWeight: 'bold' }}
          />
        </div>
      </BackTop>
    </Layout>
  );
};

export default AppLayout;
