import { RightOutlined, VerticalAlignTopOutlined } from '@ant-design/icons';
import { BackTop, Button, Col, Layout, Row } from 'antd';
import Container from 'components/Container/Container';
import AppFooter from 'components/Footer/Footer';
import AppHeader from 'components/Header/Header';

import {
  ItemFeature,
  ListItemFeature,
  SideBarListFeature,
} from 'pages/User/User.style';
import React, { FC } from 'react';
import { Outlet, useParams } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from 'redux/hooks';
import { selectAuth, logoutAsync } from 'redux/slices/auth.slice';
import { selectCurrentUser } from 'redux/slices/current-user.slice';

const UserLayout: FC = () => {
  const dispath = useAppDispatch();
  const auth = useAppSelector(selectAuth);
  const currentUser = useAppSelector(selectCurrentUser);
  return (
    <Layout style={{ minHeight: '100vh' }}>
      <AppHeader fixed />
      <Container
        style={{ flex: '1 auto', marginTop: 64, paddingTop: 16 }}
        direction="vertical"
        flex
      >
        <Row gutter={16}>
          <Col span={6}>
            <SideBarListFeature title="USER MANAGE">
              <ListItemFeature>
                <ItemFeature to="/user">
                  <div style={{ paddingLeft: 12 }}>User Info</div>
                  <RightOutlined />
                </ItemFeature>
                <ItemFeature to="/user/booking">
                  <div style={{ paddingLeft: 12 }}>Booking</div>
                  <RightOutlined />
                </ItemFeature>
                <ItemFeature to="/user/history-booking">
                  <div style={{ paddingLeft: 12 }}>History</div>
                  <RightOutlined />
                </ItemFeature>
              </ListItemFeature>
            </SideBarListFeature>
          </Col>
          <Col span={18}>
            <Outlet />
          </Col>
        </Row>
      </Container>
      <AppFooter />
      <BackTop>
        <div
          style={{
            height: 50,
            width: 50,
            backgroundColor: '#e73948',
            borderRadius: '50%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            textAlign: 'center',
          }}
        >
          <VerticalAlignTopOutlined
            style={{ fontSize: 30, color: 'white', fontWeight: 'bold' }}
          />
        </div>
      </BackTop>
    </Layout>
  );
};

export default UserLayout;
