import { Col, Layout, Row } from 'antd';
import AppFooterAdmin from 'components/FooterAdmin/FooterAdmin';
import AppHeaderAdmin from 'components/HeaderAdmin/HeaderAdmin';
import SidebarAdmin from 'components/SidebarAdmin/SidebarAdmin';
import React, { FC } from 'react';
import { Outlet } from 'react-router-dom';

const SidebarLayoutAdmin: FC = () => {
  return (
    <Layout style={{ minHeight: '100vh' }}>
      <AppHeaderAdmin fixed />
      <Layout style={{ marginTop: 64 }}>
        <Row>
          <Col span={4}>
            <SidebarAdmin />
          </Col>
          <Col span={20}>
            <Outlet />
          </Col>
        </Row>
      </Layout>
      <AppFooterAdmin />
    </Layout>
  );
};

export default SidebarLayoutAdmin;
