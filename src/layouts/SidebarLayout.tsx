import { VerticalAlignTopOutlined } from '@ant-design/icons';
import { BackTop, Col, Layout, Row } from 'antd';
import Container from 'components/Container/Container';
import AppFooter from 'components/Footer/Footer';
import AppHeader from 'components/Header/Header';
import Sidebar from 'components/Sidebar/Sidebar';
import React, { FC } from 'react';
import { Outlet } from 'react-router-dom';

const SidebarLayout: FC = () => {
  return (
    <Layout style={{ minHeight: '100vh' }}>
      <AppHeader fixed />
      <Container
        style={{ flex: '1 auto', marginTop: 64, paddingTop: 16 }}
        direction="vertical"
        flex
      >
        <Row gutter={[16, 16]}>
          <Col xs={0} lg={6}>
            <Sidebar />
          </Col>
          <Col xs={24} lg={18}>
            <Outlet />
          </Col>
        </Row>
      </Container>
      <AppFooter />
      <BackTop>
        <div
          style={{
            height: 50,
            width: 50,
            backgroundColor: '#e73948',
            borderRadius: '50%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            textAlign: 'center',
          }}
        >
          <VerticalAlignTopOutlined
            style={{ fontSize: 30, color: 'white', fontWeight: 'bold' }}
          />
        </div>
      </BackTop>
    </Layout>
  );
};

export default SidebarLayout;
