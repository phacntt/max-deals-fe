import { Col, Layout, Row } from 'antd';
import AppFooterAdmin from 'components/FooterAdmin/FooterAdmin';
import AppHeaderAdmin from 'components/HeaderAdmin/HeaderAdmin';
import SidebarSAdmin from 'components/SidebarSAdmin/SidebarSAdmin';
import React, { FC } from 'react';
import { Outlet } from 'react-router-dom';

const SidebarLayoutSAdmin: FC = () => {
  return (
    <Layout style={{ minHeight: '100vh' }}>
      <AppHeaderAdmin fixed />
      <Layout style={{ marginTop: 64 }}>
        <Row>
          <Col span={4}>
            <SidebarSAdmin />
          </Col>
          <Col span={20}>
            <Outlet />
          </Col>
        </Row>
      </Layout>
      <AppFooterAdmin />
    </Layout>
  );
};

export default SidebarLayoutSAdmin;
