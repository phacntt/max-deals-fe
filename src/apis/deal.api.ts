import { AxiosInstance } from 'axios';
import { Deal } from 'types/Deal';
import { APIResponse } from './api';

export interface QueryGetDeal {
  categoryId?: number;
  storeId?: number;
  limit?: number;
  expiryDate?: string;
  id?: 'ASC' | 'DESC';
  discount?: 'ASC' | 'DESC';
  name?: string;
  center_lat?: number;
  center_lng?: number;
  center_radius?: number;
}

type DealResponse = Deal;

type GetDealResponse = Deal;

export type AddDealRequestBody = {
  name: string;
  slug: string;
  imageIds: number[];
  expiryDate: string;
  quantity: number;
  description: string;
  discount: number;
  categoryId: number;
  storeId: number;
};

export type UpdateDealRequestBody = {
  name?: string;
  slug?: string;
  imageIds?: number[];
  expiryDate?: string;
  quantity?: number;
  description?: string;
  discount?: number;
  categoryId?: number;
  storeId?: number;
};

export class DealAPI {
  // eslint-disable-next-line no-useless-constructor
  constructor(private apiInstance: AxiosInstance) {}

  async getAllDeal(query?: QueryGetDeal): Promise<APIResponse<DealResponse[]>> {
    const resp = await this.apiInstance.get<APIResponse<DealResponse[]>>(
      '/api/deals',
      {
        params: query,
      },
    );

    return resp.data;
  }

  async getDealById(id: number) {
    const resp = await this.apiInstance.get<APIResponse<DealResponse>>(
      `/api/deals/${id}`,
      {
        params: id,
      },
    );
    return resp.data;
  }

  async addDeal(
    dealData: AddDealRequestBody,
  ): Promise<APIResponse<DealResponse>> {
    const resp = await this.apiInstance.post<APIResponse<DealResponse>>(
      '/api/deals',

      dealData,
    );
    return resp.data;
  }

  async updateDeal(
    id: number,
    dealData: UpdateDealRequestBody,
    imageIds: number[],
  ): Promise<APIResponse<DealResponse>> {
    const resp = await this.apiInstance.put<APIResponse<DealResponse>>(
      `/api/deals/${id}`,
      { dealData, imageIds },
    );
    return resp.data;
  }

  async deleteDeal(id: number): Promise<APIResponse<DealResponse>> {
    const resp = await this.apiInstance.delete<APIResponse<DealResponse>>(
      `/api/deals/${id}`,
      {
        params: id,
      },
    );
    return resp.data;
  }

  async getDeal(): Promise<APIResponse<GetDealResponse[]>> {
    const resp = await this.apiInstance.get('/api/deals');
    return resp.data;
  }
}
