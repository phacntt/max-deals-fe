import { AxiosInstance } from 'axios';
import { File } from 'types/File';
import { APIResponse } from './api';

type FileResponse = File;

export type FileRequestBody = {
  id: number;
  name: string;
  path: string;
  url: string;
  size: number;
  mimeType: string;
};

export type EntityType = 'DealEntity' | 'CategoryEntity' | 'StoreEntity';

export class FileAPI {
  // eslint-disable-next-line no-useless-constructor
  constructor(private apiInstance: AxiosInstance) {}

  async checkFile(data: FileRequestBody): Promise<APIResponse<FileResponse[]>> {
    const resp = await this.apiInstance.post<APIResponse<FileResponse[]>>(
      '/api/file',
      data,
    );

    return resp.data;
  }

  async getFileById(
    id: number,
    typeEntity: EntityType,
  ): Promise<APIResponse<FileResponse[]>> {
    const resp = await this.apiInstance.get<APIResponse<FileResponse[]>>(
      `/api/file/${id}`,
      {
        params: { id, typeEntity },
      },
    );

    return resp.data;
  }

  async uploadFile(data: FormData): Promise<APIResponse<FileResponse[]>> {
    const resp = await this.apiInstance.post<APIResponse<FileResponse[]>>(
      '/api/file/multiple',
      data,
    );

    return resp.data;
  }

  async uploadSingleFile(data: FormData): Promise<APIResponse<FileResponse>> {
    const resp = await this.apiInstance.post<APIResponse<FileResponse>>(
      '/api/file/single',
      data,
    );

    return resp.data;
  }

  async updateFile(
    fileIds: number[],
    dealId: number,
  ): Promise<APIResponse<FileResponse[]>> {
    const resp = await this.apiInstance.put<APIResponse<FileResponse[]>>(
      `/api/file/${dealId}`,
      fileIds,
    );

    return resp.data;
  }

  async deleteFile(fileIds: number[]): Promise<APIResponse<FileResponse>> {
    const resp = await this.apiInstance.delete<APIResponse<FileResponse>>(
      `/api/file`,
      { data: fileIds },
    );
    return resp.data;
  }
}
