import { AxiosInstance } from 'axios';
import { Account } from 'services/account/account';
import { APIResponse } from './api';

interface LoginResponse {
  id: string;
  name: string;
  email: string;
  role: string;
}

type GetMeResponse = Account;

export class AccountAPI {
  // eslint-disable-next-line no-useless-constructor
  constructor(private apiInstance: AxiosInstance) {}

  // eslint-disable-next-line class-methods-use-this
  async login(
    username: string,
    password: string,
  ): Promise<APIResponse<LoginResponse>> {
    const resp = await this.apiInstance.post<APIResponse<LoginResponse>>(
      '/api/login',
      {
        username,
        password,
      },
    );

    return resp.data;
  }

  // eslint-disable-next-line class-methods-use-this
  async logout(): Promise<void> {
    await this.apiInstance.post('/api/logout');
  }

  // eslint-disable-next-line class-methods-use-this
  async getMe(): Promise<APIResponse<GetMeResponse>> {
    const resp = await this.apiInstance.get('/api/users/me');
    return resp.data;
  }
}
