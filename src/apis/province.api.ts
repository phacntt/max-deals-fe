import { AxiosInstance } from 'axios';
import { Province } from 'types/Province';
import { APIResponse } from './api';

type ProvinceResponse = Province;

export class ProvinceAPI {
  // eslint-disable-next-line no-useless-constructor
  constructor(private apiInstance: AxiosInstance) {}

  async getAllProvince(): Promise<APIResponse<ProvinceResponse[]>> {
    const resp = await this.apiInstance.get<APIResponse<ProvinceResponse[]>>(
      '/api/provinces',
    );

    return resp.data;
  }

  async getProvinceById(
    provinceId: string,
  ): Promise<APIResponse<ProvinceResponse>> {
    const resp = await this.apiInstance.get<APIResponse<ProvinceResponse>>(
      `/api/provinces/${provinceId}`,
      {
        params: provinceId,
      },
    );

    return resp.data;
  }
}
