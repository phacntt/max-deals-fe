import { AxiosInstance } from 'axios';
import { Category } from 'types/Category';
import { APIResponse } from './api';

type CategoryResponse = Category;
// interface CategoryResponse extend Category {} Nếu thêm field Category

export interface QueryGetCategory {
  parentId?: number;
  parentOnly?: boolean;
  sort?: 'ASC' | 'DESC';
}

export type AddCategoryRequestBody = {
  name: string;
  slug: string;
  parentId?: number;
};

export type UpdateCategoryRequestBody = {
  name?: string;
  slug?: string;
  parentId?: number;
};

export class CategoryAPI {
  // eslint-disable-next-line no-useless-constructor
  constructor(private apiInstance: AxiosInstance) {}

  async getAllCategory(
    query?: QueryGetCategory,
  ): Promise<APIResponse<CategoryResponse[]>> {
    const resp = await this.apiInstance.get<APIResponse<CategoryResponse[]>>(
      '/api/categories',
      {
        params: query,
      },
    );

    return resp.data;
  }

  async getCategoryById(id: number): Promise<APIResponse<CategoryResponse>> {
    const resp = await this.apiInstance.get<APIResponse<CategoryResponse>>(
      `/api/categories/${id}`,
      {
        params: id,
      },
    );

    return resp.data;
  }

  async addCategory(
    categoryData: AddCategoryRequestBody,
  ): Promise<APIResponse<CategoryResponse>> {
    const resp = await this.apiInstance.post<APIResponse<CategoryResponse>>(
      '/api/categories',
      categoryData,
    );
    return resp.data;
  }

  async updateCategory(
    id: number,
    categoryData: UpdateCategoryRequestBody,
  ): Promise<APIResponse<CategoryResponse>> {
    const resp = await this.apiInstance.put<APIResponse<CategoryResponse>>(
      `/api/categories/${id}`,

      categoryData,
    );
    return resp.data;
  }

  async deletetCategory(id: number): Promise<APIResponse<CategoryResponse>> {
    const resp = await this.apiInstance.delete<APIResponse<CategoryResponse>>(
      `/api/categories/${id}`,
      {
        params: id,
      },
    );
    return resp.data;
  }
}
