import { AxiosInstance } from 'axios';
import { File } from 'types/File';
import { APIResponse } from './api';

interface StoreResponse {
  id: number;
  name: string;
  slug: string;
  description: string;
  phone: string;
  province: string;
  district: string;
  ward: string;
  street: string;
  fullAddress: string;
  lat: number;
  lng: number;
  images: File[];
  createdAt: Date;
  distance: number;
}

export type AddStoreRequestBody = {
  name: string;
  slug: string;
  description: string;
  phone: string;
  imageIds: number[];
  province: string;
  district: string;
  ward: string;
  street: string;
  fullAddress: string;
  lat: number;
  lng: number;
};

export type UpdateStoreRequestBody = {
  name?: string;
  slug?: string;
  description?: string;
  phone?: string;
  imageIds?: number[];
  province?: string;
  district?: string;
  ward?: string;
  street?: string;
  fullAddress?: string;
  lat?: number;
  lng?: number;
};

export type InfoLocal = {
  province: string;
  district: string;
  ward: string;
  street: string;
  fullAdrress?: string;
};

export class StoreAPI {
  // eslint-disable-next-line no-useless-constructor
  constructor(private apiInstance: AxiosInstance) {}

  async getAllStore(): Promise<APIResponse<StoreResponse[]>> {
    const resp = await this.apiInstance.get<APIResponse<StoreResponse[]>>(
      '/api/stores',
    );

    return resp.data;
  }

  async getStoreById(id: number): Promise<APIResponse<StoreResponse>> {
    const resp = await this.apiInstance.get<APIResponse<StoreResponse>>(
      `/api/stores/${id}`,
    );

    return resp.data;
  }

  async addStore(
    storeData: AddStoreRequestBody,
  ): Promise<APIResponse<StoreResponse>> {
    const resp = await this.apiInstance.post<APIResponse<StoreResponse>>(
      '/api/stores',
      storeData,
    );
    return resp.data;
  }

  async updateStore(
    id: number,
    storeData: UpdateStoreRequestBody,
    imageIds: number[],
  ): Promise<APIResponse<StoreResponse>> {
    const resp = await this.apiInstance.put<APIResponse<StoreResponse>>(
      `/api/stores/${id}`,
      { storeData, imageIds },
    );
    return resp.data;
  }

  async deleteStore(id: number): Promise<APIResponse<StoreResponse>> {
    const resp = await this.apiInstance.delete<APIResponse<StoreResponse>>(
      `/api/stores/${id}`,
      { params: id },
    );
    return resp.data;
  }
}
