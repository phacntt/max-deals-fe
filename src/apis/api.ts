import axios, { AxiosError } from 'axios';
// import camelcaseKeys from 'camelcase-keys';
// import snakecaseKeys from 'snakecase-keys';

export interface APIResponseError {
  statusCode: number;
  message: string;
}

export interface APIResponse<T> {
  data: T;
  total?: number;
  offset?: number;
  limit?: number;
}

export type APIError = AxiosError;

const api = axios.create({
  timeout: 30 * 1000,
  withCredentials: true,
  headers: { accept: 'application/json' },
});

// api.interceptors.request.use(req => {
//   if (req.params) {
//     req.params = snakecaseKeys(req.params, { deep: true });
//   }

//   if (
//     req.data &&
//     req.headers &&
//     req.headers['Content-Type'] !== 'multipart/form-data'
//   ) {
//     req.data = snakecaseKeys(req.data, { deep: true });
//   }

//   return req;
// });

// api.interceptors.response.use(res => {
//   if (res.data) {
//     res.data = camelcaseKeys(res.data, { deep: true });
//   }

//   return res;
// });

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function isAxiosError(err: any): err is AxiosError {
  return 'response' in err || 'code' in err;
}

export const getAPIErrorMessage = (error: AxiosError | Error) => {
  if (isAxiosError(error)) {
    return error.response
      ? error.response.data.message || error.response.statusText
      : error.code;
  }

  return error.message;
};

export default api;
