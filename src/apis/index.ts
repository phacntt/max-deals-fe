import { AccountAPI } from './account.api';
import api from './api';
import { BookAPI } from './book.api';
import { CategoryAPI } from './category.api';
import { DealAPI } from './deal.api';
import { FileAPI } from './file.api';
import { ProvinceAPI } from './province.api';
import { StoreAPI } from './store.api';
import { UserAPI } from './user.api';

export const accountAPI = new AccountAPI(api);
export const dealAPI = new DealAPI(api);
export const categoryAPI = new CategoryAPI(api);
export const storeAPI = new StoreAPI(api);
export const fileAPI = new FileAPI(api);
export const provinceAPI = new ProvinceAPI(api);
export const userAPI = new UserAPI(api);
export const bookAPI = new BookAPI(api);
