import { AxiosInstance } from 'axios';
import { Book } from 'types/Book';
import { APIResponse } from './api';

type BookResponse = Book;
// interface CategoryResponse extend Category {} Nếu thêm field Category

export interface QueryGetBook {
  userId?: number;
  dealId?: number;
  status?: string;
  codeVerify?: string;
  storeId?: number;
}

export type AddBookRequestBody = {
  dateBook?: string;
  dateExpiryBook?: string;
  codeVerify: string;
  status: string;
  userId: number;
  dealId: number;
};

export type UpdateBookRequestBody = {
  dateBook?: string;
  dateExpiry?: string;
  codeVerify?: string;
  status: string;
  userId?: number;
  dealId?: number;
};

export class BookAPI {
  // eslint-disable-next-line no-useless-constructor
  constructor(private apiInstance: AxiosInstance) {}

  async getAllBook(query?: QueryGetBook): Promise<APIResponse<BookResponse[]>> {
    const resp = await this.apiInstance.get<APIResponse<BookResponse[]>>(
      '/api/books',
      {
        params: query,
      },
    );

    return resp.data;
  }

  async getBookById(id: number): Promise<APIResponse<BookResponse>> {
    const resp = await this.apiInstance.get<APIResponse<BookResponse>>(
      `/api/books/${id}`,
      {
        params: id,
      },
    );

    return resp.data;
  }

  async addBook(
    bookData: AddBookRequestBody,
  ): Promise<APIResponse<BookResponse>> {
    const resp = await this.apiInstance.post<APIResponse<BookResponse>>(
      '/api/books',
      bookData,
    );
    return resp.data;
  }

  async updateBook(
    id: number,
    bookData: UpdateBookRequestBody,
  ): Promise<APIResponse<BookResponse>> {
    const resp = await this.apiInstance.put<APIResponse<BookResponse>>(
      `/api/books/${id}`,

      bookData,
    );
    return resp.data;
  }

  async deleteBook(id: number): Promise<APIResponse<BookResponse>> {
    const resp = await this.apiInstance.delete<APIResponse<BookResponse>>(
      `/api/books/${id}`,
      {
        params: id,
      },
    );
    return resp.data;
  }
}
