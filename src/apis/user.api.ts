import { AxiosInstance } from 'axios';

import { User } from 'types/User';
import { APIResponse } from './api';

export type RoleAccount = 'super_admin' | 'admin' | 'user';

export interface QueryGetUser {
  role?: RoleAccount;
  storeId?: number;
}

export type AddUserRequestBody = {
  username: string;
  password: string;
  email: string;
  phone: string;
  name: string;
};

export type UpdateUserRequestBody = {
  username?: string;
  password?: string;
  email?: string;
  phone?: string;
  name?: string;
  role?: string;
  storeId?: number | null;
};

type UsersResponse = User[];

type UserResponse = User;

export class UserAPI {
  // eslint-disable-next-line no-useless-constructor
  constructor(private apiInstance: AxiosInstance) {}

  // eslint-disable-next-line class-methods-use-this

  async getUsers(query?: QueryGetUser): Promise<APIResponse<UsersResponse>> {
    const resp = await this.apiInstance.get<APIResponse<UsersResponse>>(
      '/api/users',
      {
        params: query,
      },
    );
    return resp.data;
  }

  async getUserById(id: number): Promise<APIResponse<UserResponse>> {
    const resp = await this.apiInstance.get<APIResponse<UserResponse>>(
      `/api/users/${id}`,
    );
    return resp.data;
  }

  async register(data: AddUserRequestBody): Promise<APIResponse<UserResponse>> {
    const resp = await this.apiInstance.post<APIResponse<UserResponse>>(
      '/api/users',
      data,
    );
    return resp.data;
  }

  async updateUser(
    id: number,
    data: UpdateUserRequestBody,
  ): Promise<APIResponse<UserResponse>> {
    const resp = await this.apiInstance.put<APIResponse<UserResponse>>(
      `/api/users/${id}`,
      data,
    );
    return resp.data;
  }

  async deleteUser(id: number): Promise<APIResponse<UserResponse>> {
    const resp = await this.apiInstance.delete<APIResponse<UserResponse>>(
      `/api/users/${id}`,
    );
    return resp.data;
  }
}
