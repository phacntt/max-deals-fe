import React, { FC, useEffect } from 'react';
import { useAppDispatch } from 'redux/hooks';
import { fetchMe } from 'redux/slices/current-user.slice';
import AppRoutes from 'routes';
import './App.css';

const MainApp: FC = () => {
  const dispath = useAppDispatch();
  useEffect(() => {
    dispath(fetchMe());
  }, [dispath]);

  return <AppRoutes />;
};

export default MainApp;
