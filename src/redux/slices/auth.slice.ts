import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { stat } from 'fs';
import { Navigate, useNavigate } from 'react-router-dom';
import { RootState } from 'redux/store';
import { AuthState, BasicAuthPayload } from 'redux/types/auth';
import { authService } from 'services';
import { fetchMe } from './current-user.slice';

const initialState: AuthState = {
  sessionStatus: 'validating',
  isAuthenticated: false,
};

export const loginAsync = createAsyncThunk(
  'auth/login',
  async ({ username, password }: BasicAuthPayload, thunkAPI) => {
    const success = await authService.loginWithPassword(username, password);
    if (!success) {
      throw new Error('invalid credentials');
    }

    thunkAPI.dispatch(fetchMe());
    // The value we return becomes the `fulfilled` action payload
    return success ? authService.getAccount() : null;
  },
);

export const logoutAsync = createAsyncThunk('auth/logout', async () => {
  await authService.logout();
});

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    changeEmail(state, action) {
      if (state.account) {
        state.account.email = action.payload;
      }
    },
  },
  extraReducers: builder => {
    builder
      .addCase(loginAsync.pending, state => {
        state.sessionStatus = 'validating';
        state.isAuthenticated = false;
      })
      .addCase(loginAsync.fulfilled, (state, action) => {
        state.sessionStatus = 'valid';
        state.isAuthenticated = true;
        state.account = action.payload ? action.payload : undefined;
      })
      .addCase(loginAsync.rejected, state => {
        state.sessionStatus = 'invalid';
        state.isAuthenticated = false;
        state.account = undefined;
      })
      .addCase(logoutAsync.fulfilled, state => {
        state.sessionStatus = 'invalid';
        state.isAuthenticated = false;
      })
      .addCase(fetchMe.fulfilled, state => {
        state.isAuthenticated = true;
        state.sessionStatus = 'valid';
      });
  },
});

export const selectAuth = (state: RootState) => state.auth;

export const { actions } = authSlice;

export default authSlice.reducer;
