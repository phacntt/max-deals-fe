import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { RootState } from 'redux/store';
import { accountService } from 'services';
import { Account } from 'services/account/account';

type CurrentUserState = Account;

const initialState: CurrentUserState = {} as Account;

export const fetchMe = createAsyncThunk('currentUser/fetchMe', async () => {
  return accountService.getMe();
});

const currentUserSlice = createSlice({
  name: 'currentUser',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(fetchMe.fulfilled, (state, action) => {
      return action.payload ? action.payload : undefined;
    });
  },
});

export const selectCurrentUser = (state: RootState) => state.currentUser;

export default currentUserSlice.reducer;
