import { Account } from 'services/auth/auth';

type SessionStatus = 'validating' | 'valid' | 'invalid';

export interface AuthState {
  isAuthenticated: boolean;
  account?: Account;
  sessionStatus: SessionStatus;
}

export type BasicAuthPayload = {
  username: string;
  password: string;
};
